// Compare.cpp : Defines the entry point for the console application.
//

#include <string>
#include "stdafx.h"
#include <iostream>
#include <ctime>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>

using namespace std;


void mcNemar(bool & dif, double & acc1, double & acc2, string cad1, string cad2); 


int _tmain(int argc, _TCHAR* argv[])
{
	string l = " $\\leftarrow$ ";
	string r = " $\\rightarrow$ ";
	
	vector<string> languages;
	//languages.push_back("ENG");
	//languages.push_back("ESP");
	//languages.push_back("CAT");
	languages.push_back("JAP");
	languages.push_back("ESP");
	languages.push_back("CAT");

	languages.push_back("CHI");
	languages.push_back("GER");
	languages.push_back("ENG");
	languages.push_back("CZE");


	
	vector<string> names;
	//names.push_back("Binary");
	//names.push_back("Ternary");
	//names.push_back("P.M.");
	//names.push_back("Hamming");

	names.push_back("B");
	names.push_back("T");
	names.push_back("PM");
	names.push_back("H");
	
	string prefix1="";
	vector<string> sufix1;
	sufix1.push_back("B_del0.5_k1_BIN.txt");
	sufix1.push_back("B_del0.5_k1_TER.txt");
	sufix1.push_back("B_del0.5_k1_PMA.txt");
	sufix1.push_back("B_del0.5_k1_HAM.txt");

	string prefix2="";
	vector<string> sufix2;
	sufix2.push_back("B_del0.5_k1_SBI.txt");
	sufix2.push_back("B_del0.5_k1_STE.txt");
	sufix2.push_back("B_del0.5_k1_SPM.txt");
	sufix2.push_back("B_del0.5_k1_SHAM.txt");

	string prefix3="";	
	vector<string> sufix3;
	sufix3.push_back("N_del0.5_k1_BIN.txt");
	sufix3.push_back("N_del0.5_k1_TER.txt");
	sufix3.push_back("N_del0.5_k1_PMA.txt");
	sufix3.push_back("N_del0.5_k1_HAM.txt");

		for (int suf = 0; suf<sufix1.size();suf++){
			cout <<  prefix1+languages[0]+sufix1[suf] << " " << prefix2+languages[0]+sufix2[suf] << " " << prefix3+languages[0]+sufix3[suf] << endl;
		}

	for(int i=0;i<languages.size();i++){ //For each language
		cout << "	\\hline" << endl;
		cout << languages[i] << endl;
		for (int suf = 0; suf<sufix1.size();suf++){
			string cad1 = prefix1+languages[i]+sufix1[suf];
			string cad2 = prefix2+languages[i]+sufix2[suf];
			string cad3 = prefix3+languages[i]+sufix3[suf];
			bool dif12,dif13,dif23 , aux;


			double acc1, acc2, acc3;
			mcNemar( dif12, acc1, acc2,cad1,cad2);
			mcNemar( dif13, acc1, acc3,cad1,cad3);
			mcNemar( dif23, acc2, acc3,cad2,cad3);

			cout << " & "<< names[suf];
						
			
			
			cout  << "& ";
			aux= acc1>acc2 & acc1> acc3& dif12 & dif13;
			if (aux) cout << "\\textbf{ ";
				cout << setprecision(4) <<    acc1 << "\\%";
			if (aux) cout << " }";
			cout << "& ";
			if (dif12 & acc1>acc2)
				cout << l ;
			if (dif12 & acc2>acc1)
				cout << r;
			if (!dif12) cout << " $=$ ";


			cout  << "& ";
			aux= acc2>acc1 & acc2> acc3& dif12 & dif23;
			if (aux) cout << "\\textbf{ ";
				cout << setprecision(4) <<    acc2 << "\\%";
			if (aux) cout << " }";
			cout << "& ";
			if (dif23 & acc2>acc3)
				cout << l ;
			if (dif23 & acc3>acc2)
				cout << r;
			if (!dif23) cout << " $=$ ";		

			cout  << "& ";
			

			aux= acc3>acc1 & acc3> acc2& dif13 & dif23;
			if (aux) cout << "\\textbf{ ";
				cout << setprecision(4) <<    acc3 << "\\%";
			if (aux) cout << " }";
			cout << "& ";
			if (dif13 & acc1>acc3)
				cout << r ;
			if (dif13 & acc3>acc1)
				cout << l;
			if (!dif13) cout << " $=$ ";	

			cout << "\\\\" << endl;
		
		}




	}



	string cad1, cad2, val1, val2, val, pred1, pred2;
	int line = 1;
	int adv1, adv2;

	double accuracy1=0;
	double accuracy2=0;
	double total = 0;
	
	double chi;

	return 0;
}

void mcNemar(bool & dif, double & acc1, double & acc2, string cad1, string cad2){

	acc1 = NULL;
	acc2 = NULL;

		string  val1, val2, val, pred1, pred2;
	int line = 1;
	int adv1, adv2;

	double accuracy1=0;
	double accuracy2=0;
	double total = 0;
	
	double chi;

	adv1 = adv2 = 0;
	ifstream f1;
	ifstream f2;
	f1.open(cad1.c_str());
	f2.open(cad2.c_str());


	if (!f1.is_open() || !f2.is_open()){
		cout << "problem opening the files" << endl;
		return;
	}

	getline(f1,cad1);
	getline(f2,cad2);

	while (f1.good()){
		line++;
		for (int i=0; i< 2;i++){
			f1 >> cad1;
			f2 >> cad2;
		}
		f1 >> val1;
		f2 >> val2;

		for (int i=0; i< 3;i++){
			f1 >> cad1;
			f2 >> cad2;
		}
		
		f1 >> pred1;
		f2 >> pred2;

		getline(f1,cad1);
		getline(f2,cad2);
		if (val1!=val2)
		{
			cout << "error: files are not comparable, line" << line << endl;
			return;
		}
		val = val1;

		if(val==pred1) accuracy1++;
		if(val==pred2) accuracy2++;

		if (val==pred1 && val!=pred2)
			adv1++;

		if (val==pred2 && val!=pred1)
			adv2++;
		total++;
	}



	if (f2.good()) cout << "warning, f2 is good, something must be wrong!!"; 

	if (adv1+adv2< 25){
		cout << "adv1+adv2< 25 , test not valid"<< endl;
	//	return;
	}
	if (adv1+adv2 == 0) cout<< "identical experiments" << endl;

	chi = adv1-adv2;
	if (chi<0) chi=-chi;
	chi -= 0.5;
	chi = chi*chi;
	chi = chi/(adv1+adv2);
	
	acc1= 100*accuracy1/total;
	acc2= 100*accuracy2/total;
	
	if (chi>10.83 && adv1+adv2> 25 ) 
		dif= true;
	else dif = false;

	return ;


}