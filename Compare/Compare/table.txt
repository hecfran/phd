%CHIB_del1_k1_BIN. CHIB_del0.5_k1_BIN.
%CHIB_del1_k1_TER. CHIB_del0.5_k1_TER.
%CHIB_del1_k1_PMA. CHIB_del0.5_k1_PMA.
%CHIB_del1_k1_HAM. CHIB_del0.5_k1_HAM.
%CHIB_del1_k1_HAM. CHIB_del1_k1_HAM.
\begin{table}[H]
\footnotesize 
\scriptsize
\centering
\singlespacing
\begin{tabular}{|c c |c c | c |}
\hline
\multicolumn{2}{|c|}{accuracy}& Bdel1k1BIN & Bdel05k1BIN& difference \\
 \hline \hline 
\multirow{5}{*}{\begin{sideways}Chinese \end{sideways}} 
 & B * & 81.03\textbf{\%!}& 80.68\%& -0.35\%\\
 & T * & 82.54\%& 82.83\textbf{\%!}& 0.2995\%\\
 & PM ** & \textbf{79.57\%!}& 76.55\%& -3.027\%\\
 & H ** & \textbf{83.55\%!}& 80.76\%& -2.786\%\\
 % adv1+adv2< 25 , test not valid
 % identical experiments
 & H = & 83.55\%& 83.55\textbf{\%!}& 0\%\\
 \hline \hline 
\multirow{5}{*}{\begin{sideways}German \end{sideways}} 
 & B * & 67.04\textbf{\%!}& 65.08\%& -1.955\%\\
 & T * & 68.25\textbf{\%!}& 66.39\%& -1.862\%\\
 & PM * & 87.9\textbf{\%!}& 85.38\%& -2.514\%\\
 & H = & 80.73\textbf{\%!}& 79.98\%& -0.7449\%\\
 % adv1+adv2< 25 , test not valid
 % identical experiments
 & H = & 80.73\%& 80.73\textbf{\%!}& 0\%\\
 \hline \hline 
\multirow{5}{*}{\begin{sideways}o-German \end{sideways}} 
 & B * & 68.17\textbf{\%!}& 66\%& -2.178\%\\
 & T = & 68.51\textbf{\%!}& 67.59\%& -0.9213\%\\
 & PM ** & \textbf{72.53\%!}& 62.06\%& -10.47\%\\
 & H * & 72.36\textbf{\%!}& 69.43\%& -2.931\%\\
 % adv1+adv2< 25 , test not valid
 % identical experiments
 & H = & 72.36\%& 72.36\textbf{\%!}& 0\%\\
 \hline \hline 
\multirow{5}{*}{\begin{sideways}English \end{sideways}} 
 & B ** & \textbf{67.08\%!}& 66.08\%& -1.005\%\\
 & T * & 68.71\textbf{\%!}& 68.2\%& -0.511\%\\
 & PM ** & \textbf{75.31\%!}& 70.59\%& -4.724\%\\
 & H ** & \textbf{76.71\%!}& 75.21\%& -1.494\%\\
 % adv1+adv2< 25 , test not valid
 % identical experiments
 & H = & 76.71\%& 76.71\textbf{\%!}& 0\%\\
 \hline \hline 
\multirow{5}{*}{\begin{sideways}o-English \end{sideways}} 
 & B = & 60.1\textbf{\%!}& 59.86\%& -0.2448\%\\
 & T = & 64.69\textbf{\%!}& 64.06\%& -0.6294\%\\
 & PM ** & \textbf{65.21\%!}& 55.52\%& -9.685\%\\
 & H ** & \textbf{65.56\%!}& 61.05\%& -4.51\%\\
 % adv1+adv2< 25 , test not valid
 % identical experiments
 & H = & 65.56\%& 65.56\textbf{\%!}& 0\%\\
 \hline \hline 
\multirow{5}{*}{\begin{sideways}Catalan \end{sideways}} 
 & B ** & 64.16\%& \textbf{ 65.08\%!}& 0.9134\%\\
 & T * & 64.28\%& 64.82\textbf{\%!}& 0.541\%\\
 & PM ** & 74.81\%& \textbf{ 77.17\%!}& 2.359\%\\
 & H = & 74.48\textbf{\%!}& 74.22\%& -0.2572\%\\
 % adv1+adv2< 25 , test not valid
 % identical experiments
 & H = & 74.48\%& 74.48\textbf{\%!}& 0\%\\
 \hline \hline 
\multirow{5}{*}{\begin{sideways}Spanish \end{sideways}} 
 & B = & 63.98\%& 64.28\textbf{\%!}& 0.296\%\\
 & T * & 64.07\%& 64.71\textbf{\%!}& 0.6427\%\\
 & PM ** & 73.1\%& \textbf{ 75.39\%!}& 2.292\%\\
 & H = & 73.65\textbf{\%!}& 73.21\%& -0.4397\%\\
 % adv1+adv2< 25 , test not valid
 % identical experiments
 & H = & 73.65\%& 73.65\textbf{\%!}& 0\%\\
 \hline \hline 
\multirow{5}{*}{\begin{sideways}Czech \end{sideways}} 
 & B ** & \textbf{64.82\%!}& 62.81\%& -2.012\%\\
 & T = & 64.86\textbf{\%!}& 64.77\%& -0.09433\%\\
 & PM ** & \textbf{65.37\%!}& 56.77\%& -8.602\%\\
 & H ** & \textbf{72.63\%!}& 63.7\%& -8.931\%\\
 % adv1+adv2< 25 , test not valid
 % identical experiments
 & H = & 72.63\%& 72.63\textbf{\%!}& 0\%\\
 \hline \hline 
\multirow{5}{*}{\begin{sideways}o-Czech \end{sideways}} 
 & B ** & \textbf{62.26\%!}& 59.99\%& -2.269\%\\
 & T * & 63.33\textbf{\%!}& 62.71\%& -0.6195\%\\
 & PM ** & \textbf{64.21\%!}& 55.67\%& -8.536\%\\
 & H ** & \textbf{71.74\%!}& 61.6\%& -10.13\%\\
 % adv1+adv2< 25 , test not valid
 % identical experiments
 & H = & 71.74\%& 71.74\textbf{\%!}& 0\%\\
 \hline \hline 
\multirow{5}{*}{\begin{sideways}Japanesse \end{sideways}} 
 & B = & 53.7\textbf{\%!}& 52.74\%& -0.9592\%\\
 & T = & 53.79\textbf{\%!}& 53.58\%& -0.2069\%\\
 & PM ** & \textbf{53.41\%!}& 48.41\%& -5.003\%\\
 & H = & 70.02\%& 70.49\textbf{\%!}& 0.4702\%\\
 % adv1+adv2< 25 , test not valid
 % identical experiments
 & H = & 70.02\%& 70.02\textbf{\%!}& 0\%\\
 \hline \hline 
\multirow{6}{*}{\begin{sideways}total \end{sideways}}
 & B & *6, **3 & *1, **1 & -0.9763\%  \\
 & T & *3, **0 & *3, **0 & -0.3361\%  \\
 & PM & *8, **7 & *2, **2 & -4.791\%  \\
 & H & *6, **5 & *0, **0 & -3.176\%  \\
 & H & *0, **0 & *0, **0 & 0\%  \\
& all & *23, **15 & *6 ,**3& -1.856\%\\
\hline
\end{tabular} 
\caption{CHIBdel1k1BINvsCHIBdel05k1BIN}
\label{CHIBdel1k1BINvsCHIBdel05k1BIN}
\end{table}
