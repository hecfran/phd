// Compare.cpp : Defines the entry point for the console application.
//

#include <string>
#include "stdafx.h"
#include <iostream>
#include <ctime>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>


using namespace std;
string tableTag =  "";
string resPath = "D://res//";
ofstream tex;
ofstream csv;

	string beginSideways =	"";//"\\begin{sideways}";
	string endSideways = "" ; //"\\end{sideways}";

//ofstream matlab;
// ofstream texkaccuracy;
// ofstream stderror; 
void mcNemar(bool & dif, double & acc1, double & acc2, string cad1, string cad2); 
void mcNemar2(bool & dif001, bool & dif05,  double & acc1, double & acc2, string cad1, string cad2); 

string MatlabClean(string a){
	string b;

	for (int i = 0; i<a.length(); i++)
		if ((a[i]>='a' && a[i]<='z') ||  (a[i]>='A' && a[i]<='Z')||  (a[i]>='0' && a[i]<='9'))
			b+=a[i];
	return b;
}


string convertInt(int number)
{
  if (number == 0)
        return "0";
    string temp="";
    string returnvalue="";
    while (number>0)
    {
        temp+=number%10+48;
        number/=10;
    }
    for (int i=0;i<temp.length();i++)
        returnvalue+=temp[temp.length()-i-1];
    return returnvalue;
}

int _tmain(int argc, _TCHAR* argv[]) 
{

	string fileTag = "";
	string prefix1="";//dev
	vector<string> sufix1;
	string lev1 = "";//deleteDepRel_
	string del1 = "1";
	string BN1 = "B_";//TK_
	string kvalue1 = "k1_";
	
//	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"BIN"); //lev_B_TK_del1_k1_SBI"
	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"HAM");
//	sufix1.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"HAM");
//	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"HAM");
//	sufix1.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"TER");
//	sufix1.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"HAM");

	//sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"HAM"); //lev_B_TK_del1_k1_SBI"
//	sufix1.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"SHAM"); //lev_B_TK_del1_k1_SBI"
// */ //
	
	
//	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"SHAPE");
/*	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"BIN"); //lev_B_TK_del1_k1_SBI"
	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"BIN"); //lev_B_TK_del1_k1_SBI"
	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"BIN"); //lev_B_TK_del1_k1_SBI"

	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"TER"); //lev_B_TK_del1_k1_SBI"
	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"TER"); //lev_B_TK_del1_k1_SBI"

	sufix1.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"TER"); //lev_B_TK_del1_k1_SBI"

//	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"BIN"); //lev_B_TK_del1_k1_SBI"
//	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"BIN"); //lev_B_TK_del1_k1_SBI"
//	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"BIN"); //lev_B_TK_del1_k1_SBI"
//	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"TER");
//	sufix1.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"TER");
//	sufix1.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"TER");

//	*/



	string prefix2="";
	vector<string> sufix2;
	string lev2 = "";//"lev_"; "lev_deleteDepRel_"
	string del2 = "1";
	string BN2 = "B_";//TK_     singleArg_
	string kvalue2 = "k1_"; // k1_N1_AllAtOnce_
	
//	sufix2.push_back(lev2+BN2+"del"+del2+"_"+kvalue2+"SHAPE"); //lev_B_TK_del1_k1_SBI"
	
//	sufix2.push_back(lev2+BN2+"del"+del2+"_"+kvalue2+"BIN"); //lev_B_TK_del1_k1_SBI"
//	sufix2.push_back(lev2+BN2+"del"+del2+"_"+kvalue2+"HAM"); //lev_B_TK_del1_k1_SBI"
	sufix2.push_back(lev2+BN2+"del"+del2+"_predMatchCost1_"+kvalue2+"HAM");
//	sufix2.push_back(lev2+BN2+"del"+del2+"_"+kvalue2+"HAM");
	/*
	sufix2.push_back(lev2+BN2+"del"+del2+"_predMatchCost1_"+kvalue2+"TER");
	sufix2.push_back(lev2+BN2+"del"+del2+"_"+kvalue2+"HAM");

	sufix2.push_back(lev2+BN2+"del"+del2+"_"+kvalue2+"HAM");

//	sufix2.push_back(lev2+BN1+"del"+del2+"_predMatchCost1_"+kvalue2+"TER");
//	sufix2.push_back(lev2+BN1+"del"+del2+"_predMatchCost1_"+kvalue2+"HAM");
	// */
	
//	sufix2.push_back(lev2+BN2+"del"+del2+"_"+kvalue2+"TER"); //lev_B_TK_del1_k1_SBI"
//	sufix2.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"TER");
//	sufix2.push_back(lev2+BN2+"del"+del2+"_"+kvalue2+"HAM");
//	sufix2.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"TER");
//	sufix2.push_back(lev2+BN2+"del"+del2+"_"+kvalue2+"HAM");
//	sufix2.push_back(lev2+BN2+"del"+del2+"_"+kvalue2+"HAM");
/*	
	sufix2.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"BIN");
	sufix2.push_back(lev1+BN1+"del"+del1+"_"+kvalue1+"HAM");
	sufix2.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"TER");
	sufix2.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"HAM");
	sufix2.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"TER");
	sufix2.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"HAM");
	sufix2.push_back(lev1+BN1+"del"+del1+"_predMatchCost1_"+kvalue1+"HAM");
	

//	*/

	vector<string> names;

	names.push_back("H-FH");
//	 names.push_back("S");
//	names.push_back("B");
//	names.push_back("T");
//	names.push_back("FT");
//	names.push_back("H");
//	names.push_back("FH");
	// */
			/*
	names.push_back("B-T");
	names.push_back("B-FT");
	names.push_back("B-H");
	names.push_back("T-FT");
	names.push_back("T-H");
	names.push_back("FT-H");
	// */
	/*
	names.push_back("S-B");
	names.push_back("T-H");
	names.push_back("T-FT");
	names.push_back("T-FH");
	names.push_back("H-FT");
	names.push_back("H-FH");
	names.push_back("FT-FH");
	*/




	//system("dir"); 
	vector<string> languages;
	vector<string> languagesName;
	vector<string> limit;
	bool diff = true;


	/*
	languages.push_back("CHI");// 
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"Chinese "+endSideways+"} ");
	limit.push_back(".");


	languages.push_back("GER");//
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"German "+endSideways+"} ");
	limit.push_back(".");
	/*
	languages.push_back("OGER");//
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"o-German "+endSideways+"} ");
	limit.push_back(".");
	//*/

	
	languages.push_back("ENG");
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"English "+endSideways+"} ");	
	//limit.push_back(".");
	limit.push_back("limSent20000.");

	languages.push_back("ENG");
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"English "+endSideways+"} ");	
	limit.push_back(".");
	//limit.push_back("limSent20000.");

	/*
	languages.push_back("OENG");
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"o-English "+endSideways+"} ");	
	limit.push_back("limSent20000.");
	// */
	
	/*
	languages.push_back("CAT");//
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"Catalan "+endSideways+"} ");
	limit.push_back(".");

	
	languages.push_back("ESP");//
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"Spanish "+endSideways+"} ");
	limit.push_back(".");

	*/
	languages.push_back("CZE");//
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"Czech "+endSideways+"} ");
//	limit.push_back(".");
	limit.push_back("limSent10000.");

	languages.push_back("CZE");//
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"Czech "+endSideways+"} ");
	limit.push_back(".");

	/*
	languages.push_back("OCZE");//
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"o-Czech "+endSideways+"} ");
	limit.push_back("limSent10000.");

	// */
	/*
	languages.push_back("JAP");//	
	languagesName.push_back("\\multirow{"+ convertInt(sufix1.size()) + "}{*}{"+beginSideways+"Japanese "+endSideways+"} ");
	limit.push_back(".");
	

	*/


	vector<vector<int> > sumas;
	vector<vector<int> > sumas05;
	vector <double> diferencias;
	sumas.resize(sufix1.size());
	sumas05.resize(sufix1.size());
	for (int i=0;i<sufix1.size();i++) { sumas[i].resize(2); sumas05[i].resize(2);}
	diferencias.resize(sufix1.size());
	

	
	string TableName = "D://tables//"+MatlabClean(fileTag+tableTag+prefix1+sufix1[0] + "vs" +  prefix2+sufix2[0]) +".tex";
	string TableNamecsv = "D://tables//"+fileTag+tableTag+prefix1+sufix1[0] + "vs" +  prefix2+sufix2[0] +".csv";
	tex.open(TableName.c_str());
	csv.open(TableNamecsv.c_str());

	csv << "accuracy\tcol1\t2col2"<< endl;
		for (int suf = 0; suf<sufix1.size();suf++){
			tex << "%"  << prefix1+languages[0]+sufix1[suf] + limit[0] << " " << prefix2+languages[0]+sufix2[suf] + limit[0] <<  endl;
		}

		tex << "\\begin{table}[p]"<< endl <<
				"\\footnotesize "<< endl <<
				"\\scriptsize"<< endl <<
				"\\centering"<< endl <<
				"\\singlespacing" << endl;
		tex<<	"\\caption{" +MatlabClean( prefix1+languages[0]+sufix1[0] + limit[0] + " vs " + prefix2+languages[0]+sufix2[0] + limit[0]) +"}"<< endl ;
			if (diff)
				tex<<	"\\begin{tabular}{|c c |c c | c |}" << endl;
			else
				tex<<	"\\begin{tabular}{|c c |c c |}" << endl;

			
			tex <<
				"\\hline" << endl<<
				"\\multicolumn{2}{|c|}{accuracy}& "<<
				 MatlabClean( sufix1[0] ) + " & " +
				 MatlabClean( sufix2[0] ) ;
			if (diff)
				tex << "& difference";
			tex << 	" \\\\" << endl; 

	for(int i=0;i<languages.size();i++){ //For each language
		cout << languages[i] << endl;
		tex << " \\hline \\hline "<< endl << languagesName[i] << endl;
		for (int suf = 0; suf<sufix1.size();suf++){
			string cad1 = prefix1+languages[i]+sufix1[suf]+limit[i];
			string cad2 = prefix2+languages[i]+sufix2[suf]+limit[i];
			bool diff001, diff05;
			double acc1, acc2;

			mcNemar2( diff001, diff05, acc1, acc2,cad1,cad2);


	
			csv <<  names[suf] << "\t" << acc1 << "\t"<< acc2 << endl;
			tex << " & "<< names[suf];
			if (diff001 == true)	tex << " ** ";
			else if (diff05 == true) tex << " * ";
			else	tex << " = ";


			diferencias[suf]+=acc2-acc1;

			if (diff05==false && diff001 ==true)
				cout << "error"<< endl;

			if(diff05 == true)
				if (acc1>acc2)
					sumas05[suf][0]++;
				else 
					sumas05[suf][1]++;

			if (diff001 == false)
			{if (acc1 > acc2)
				if (!diff)
					tex  << "& " << setprecision(4) <<    acc1 << "\\textbf{\\%!}"  << "& " << setprecision(4)<< acc2 <<  "\\%" << " \\\\" << endl;
				else
					tex  << "& " << setprecision(4) <<    acc1 << "\\textbf{\\%!}"  << "& " << setprecision(4)<< acc2 <<  "\\%" << "& " << acc2-acc1 <<"\\%\\\\" << endl;
			else
				if (!diff)
					tex  << "& " << setprecision(4) <<    acc1 << "\\%"  << "& " << setprecision(4)<< acc2 <<  "\\textbf{\\%!}" << " \\\\" << endl;
				else 
					tex  << "& " << setprecision(4) <<    acc1 << "\\%"  << "& " << setprecision(4)<< acc2 <<  "\\textbf{\\%!}" << "& " << acc2-acc1 <<"\\%\\\\" << endl;
			}
			else if (acc1 > acc2){
				if (!diff)
					tex  << "& " <<  "\\textbf{" << setprecision(4)  << acc1 << "\\%!}"  << "& " << setprecision(4)<< acc2 <<  "\\%" << " \\\\" << endl;
				else
					tex  << "& " <<  "\\textbf{" << setprecision(4)  << acc1 << "\\%!}"  << "& " << setprecision(4)<< acc2 <<  "\\%" <<  "& " << acc2-acc1 <<"\\%\\\\" << endl;
				sumas[suf][0]++;
			//	diferencias[suf]+=acc2-acc1;
			}
			else{
				if (!diff)
					tex  << "& " << setprecision(4) <<  acc1 << "\\%"  << "& \\textbf{ " << setprecision(4)<< acc2 <<  "\\%!}" << " \\\\" << endl;
				else 
					tex  << "& " << setprecision(4) <<  acc1 << "\\%"  << "& \\textbf{ " << setprecision(4)<< acc2 <<  "\\%!}"  << "& " << acc2-acc1 <<"\\%\\\\" << endl;
				sumas[suf][1]++;
			//	diferencias[suf]+=acc2-acc1;
			}
			//%Binary 		check=	& 58.12\%	&59.12\%
			//tex << endl; 
		} 
	}
	tex << " \\hline \\hline "<< endl << "\\multirow{"+ convertInt(sufix1.size()+1) + "}{*}{"+beginSideways+"total "+endSideways+"}" << endl;
	for (int suf = 0; suf<sufix1.size();suf++){
		tex << " & "<< names[suf];
	//	if (sumas[suf][0]==0)
	//		tex  << " & " <<   "-";
	//	else if (sumas[suf][0]<4)
			tex  << " & *" <<  sumas05[suf][0] << ", **" <<  sumas[suf][0];
	//	else
	//		tex <<  " & \\textbf{" << sumas[suf][0] << "}";

//		if (sumas[suf][1]==0)
//			tex << " & -";
//		else if (sumas[suf][1]<4)
			tex  << " & *" << sumas05[suf][1] << ", **" <<   sumas[suf][1];
//		else
//			tex <<  " & \\textbf{" << sumas[suf][1] << "}";
		if (diff) tex << " & "<< diferencias[suf]/languages.size() << "\\% ";
		tex  << " \\\\" << endl; 	
	} 

	double auxiliar050 = 0;
	for (int i= 0;i<sufix1.size();i++)
		auxiliar050+=sumas05[i][0];
	
	double auxiliar0 = 0;
	for (int i= 0;i<sufix1.size();i++)
		auxiliar0+=sumas[i][0];

	double auxiliar051 = 0;
	for (int i= 0;i<sufix1.size();i++)
		auxiliar051+=sumas05[i][1];

	double auxiliar1 = 0;
	for (int i= 0;i<sufix1.size();i++)
		auxiliar1+=sumas[i][1];

	
	double auxiliardiff = 0;
	for (int i= 0;i<sufix1.size();i++)
		auxiliardiff+=diferencias[i];



	tex << "& all & *" <<  auxiliar050 <<", **" << auxiliar0 << " & *"  <<auxiliar051 << " ,**" <<  auxiliar1;
	if (diff)
		tex << "& "  << (auxiliardiff)/(sufix1.size()*languages.size())<< "\\%" ;
	tex << "\\\\"<< endl;

	tex <<"\\hline"<< endl<<
			"\\end{tabular} "<< endl;
	
	tex <<		"\\label{" +MatlabClean( prefix1+languages[0]+sufix1[0] + limit[0] + " vs " + prefix2+languages[0]+sufix2[0] + limit[0]) +"}" << endl <<
			"\\end{table}" << endl;

	tex.close();
	csv.close();


	string cad1, cad2, val1, val2, val, pred1, pred2;
	int line = 1;
	int adv1, adv2;

	double accuracy1=0;
	double accuracy2=0;
	double total = 0;
	
	double chi;

	return 0;
}


bool fexists(string filename)
{
	ifstream ifile((resPath+filename).c_str());
  return ifile;
}




void mcNemar(bool & dif, double & acc1, double & acc2, string cad1, string cad2){
	//tex << cad1 << " " << cad2 << endl;
	dif = false;
	acc1 = -1;
	acc2 = -1;

		string  val1, val2, val, pred1, pred2;
	int line = 1;
	int adv1, adv2;

	double accuracy1=0;
	double accuracy2=0;
	double total = 0;
	
	double chi;

	adv1 = adv2 = 0;
	ifstream f1;
	ifstream f2;


	if	(!fexists(cad1+"fin"))
		tex << "% " << cad1 << "fin do not exist. The experiment is not finished"<< endl;
	if	(!fexists(cad2+"fin"))
		tex << "% " << cad2 << "fin do not exist. The experiment is not finished"<< endl;

	f1.open((resPath+cad1+"txt").c_str());
	f2.open((resPath+cad2+"txt").c_str());


	if (!f1.is_open() || !f2.is_open()){
		cout << "problem opening the files" << endl;
		return;
	}

	getline(f1,cad1);
	getline(f2,cad2);

	while (f1.good()){
		line++;
		for (int i=0; i< 2;i++){
			f1 >> cad1;
			f2 >> cad2;
		}
		f1 >> val1;
		f2 >> val2;

		for (int i=0; i< 3;i++){
			f1 >> cad1;
			f2 >> cad2;
		}
		
		f1 >> pred1;
		f2 >> pred2;


	
		
		getline(f1,cad1+"txt");
		getline(f2,cad2+"txt");


		if (val1!=val2)
		{
			tex << "%error: files are not comparable, line" << line << endl;
			tex << "%"<< cad1 << " " << cad2 << endl;
			return;
		}
		val = val1;

		if(val==pred1) accuracy1++;
		if(val==pred2) accuracy2++;

		if (val==pred1 && val!=pred2)
			adv1++;

		if (val==pred2 && val!=pred1)
			adv2++;
		total++;
	}



	if (f2.good()) tex << "%warning, f2 is good, something must be wrong!!"<< endl; 

	if (adv1+adv2< 25){
		tex << " % adv1+adv2< 25 , test not valid"<< endl;
	//	return;
	}
	if (adv1+adv2 == 0) tex<< " % identical experiments" << endl;




	chi = adv1-adv2;
	if (chi<0) chi=-chi;
	chi -= 0.5;
	chi = chi*chi;
	chi = chi/(adv1+adv2);
	
	acc1= 100*accuracy1/total;
	acc2= 100*accuracy2/total;
	
	if (chi>10.83 && adv1+adv2> 25 ) 
		dif= true;
	else dif = false;

	return ;


}




void mcNemar2(bool & dif001, bool &dif05, double & acc1, double & acc2, string cad1, string cad2){
	//tex << cad1 << " " << cad2 << endl;
	dif001 = false;
	dif05 = false;
	acc1 = -1;
	acc2 = -1;

	string file1 = cad1;
	string file2 = cad2;


	string  val1, val2, val, pred1, pred2;
	int line = 1;
	int adv1, adv2;

	double accuracy1=0;
	double accuracy2=0;
	double total = 0;
	
	double chi;

	adv1 = adv2 = 0;
	ifstream f1;
	ifstream f2;


	if	(!fexists(cad1+"fin"))
		tex << "% " << cad1 << "fin do not exist. The experiment is not finished"<< endl;
	if	(!fexists(cad2+"fin"))
		tex << "% " << cad2 << "fin do not exist. The experiment is not finished"<< endl;

	f1.open((resPath+cad1+"txt").c_str());
	f2.open((resPath+cad2+"txt").c_str());


	if (!f1.is_open())
		cout << "problem opening: " << file1 << endl;

	if (!f2.is_open())
		cout << "problem opening: " << file2 << endl;


	if (!f1.is_open() || !f2.is_open()){
		cout << "problem opening the files" << endl;
		return;
	}

	getline(f1,cad1);
	getline(f2,cad2);

	while (f1.good()){
		line++;
		for (int i=0; i< 2;i++){
			f1 >> cad1;
			f2 >> cad2;
		}
		f1 >> val1;
		f2 >> val2;

		for (int i=0; i< 3;i++){
			f1 >> cad1;
			f2 >> cad2;
		}
		
		f1 >> pred1;
		f2 >> pred2;


	
		
		getline(f1,cad1+"txt");
		getline(f2,cad2+"txt");


		if (val1!=val2)
		{
			double basura;
 			tex << "%error: files are not comparable, line" << line << " files: "<< file1 << " " << file2 << endl;
			mcNemar2(dif001, dif05, acc1, basura, file1, file1);
			mcNemar2(dif001, dif05, acc2, basura, file2, file2);
			return;
			break;
		}
		val = val1;

		if(val==pred1) accuracy1++;
		if(val==pred2) accuracy2++;

		if (val==pred1 && val!=pred2)
			adv1++;

		if (val==pred2 && val!=pred1)
			adv2++;
		total++;
	}



	if (f2.good()) tex << "%warning, f2 is good, something must be wrong!!"<< endl; 

	if (adv1+adv2< 25){
		tex << " % adv1+adv2< 25 , test not valid"<< endl;
		tex << " % "<< file1 << " <is almost equal to> "<< file2 << endl; 
	//	return;
	}
	if (adv1+adv2 == 0) tex<< " % identical experiments" << endl;




	chi = adv1-adv2;
	if (chi<0) chi=-chi;
	chi -= 0.5;
	chi = chi*chi;
	chi = chi/(adv1+adv2);
	
	acc1= 100*accuracy1/total;
	acc2= 100*accuracy2/total;
	
	if (chi>10.828 && adv1+adv2> 25 ) 
		dif001= true;
	else dif001 = false;

	if (chi>3.841 && adv1+adv2> 25 ) 
		dif05= true;
	else dif05 = false;
	return ;


}