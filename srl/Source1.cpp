	void DepTreeCorpus2::identifyAndLabelMeWhith(DepTreeCorpus & training){///????????????????????????????????????????????????????????????????????????????????
//	ofstream salida;
	int correct=0;
	int total=0;
	DepTree2 * currentSentence;


	for(int sentence = 0; sentence<(int)DTcorpora.size() ;sentence++)// for each sentence.
	{
		cout << "complete: " << (sentence/((double)DTcorpora.size())*100)<< "%"<<   "   \trelabeling and Identifing subtree n:" << sentence << " from line: " <<  DTcorpora[sentence]->line_number << endl;
		currentSentence = (DepTree2*) DTcorpora[sentence]; 

		for (int predicate = 0; predicate< (int) currentSentence->POpred.size();predicate++){ //for each predicate on the sentence we need to find its arguments.

			int CnodeNumber = currentSentence->POpred[predicate];
			((DepTreeNode*)currentSentence->PO_nodes[CnodeNumber])->pegamento=true;
			
			///measure all distances
			vector<TreeMeasure*> lista;
			for (int subtree = 0 ; subtree<(int)training.samples.size(); subtree++){
				((DepTreeNode*)training.samples[subtree]->PO_nodes[training.samples[subtree]->predicateNode])->pegamento=true;

				Alignment *q;
				TreeDistance *p = new TreeDistance(currentSentence,training.samples[subtree]);
				lista.push_back(p);
				//p->getAlignament(&q); q->dot_show();

				((DepTreeNode*)training.samples[subtree]->PO_nodes[training.samples[subtree]->predicateNode])->pegamento=false;
			}
			// short
			 sort (lista.begin(), lista.end(), *compare);

			// find arguments // in this case use only the first one

			Alignment *p= NULL;
			lista[0]->getAlignament(&p);
			DepSubTree * subt = NULL;
			subt = (DepSubTree *) lista[0]->getCoDomainTree();

			vector<pair<int, SemanticRelation *> > empty;// = new vector<pair<int,SemanticRelation *> >();
			vector<vector<pair<int, SemanticRelation *> > > *  auxi;

			//currentSentence->NEW_POapreds = currentSentence->POapreds;
		//	for (unsigned int i = 0; i< currentSentence->POapreds.size(); i++)//cleaning new positions.
		//		currentSentence->NEW_POapreds.push_back(empty);
			auxi = 	&currentSentence->NEW_POapreds;

			auxi ->resize(currentSentence->POapreds.size());

			//vector<vector<pair<int, SemanticRelation *> > > NEW_POapreds;
			for (int argNum = 0; argNum<subt->POApred.size(); argNum++){
				// for each argument in the sample sub tree that maches a node in the sentence, mark it as an argument.

				int position; 
				position = subt->POApred[argNum].first;
				int sentencePosition;
				sentencePosition = p->inverseF(position);
				pair<int, SemanticRelation *> s;
				s = subt->POApred[argNum];
				s.first = sentencePosition;
				if (s.first>0){
					//currentSentence->NEW_POapreds[argNum].push_back(s);
					currentSentence->NEW_POapreds[predicate].push_back(s);
				}

			}



			((DepTreeNode*)currentSentence->PO_nodes[CnodeNumber])->pegamento=false;//des preparar. 




//		if(params()->cacheSize>0)
//			addToCache(query.samples[position],0);//ADDING TO CACHE

			//cleaning memory
			for(unsigned int i = 0; i<lista.size();i++){
				delete lista[i];
			}

		}

	}
//	salida.close();
	params()->accuracy=((double)correct)/((double)total);
//	cout << "accuracy " <<params()->accuracy << endl;
	//PARAMETERS.inform << "accuracy = " <<PARAMETERS.accuracy  << endl;
}
