//#include "StdAfx.h"
#include "stdafx.h"
#include "TreeDistance.h"
#include <algorithm>



/**
* abs function
*/
double habs(double num){
	if (num>0)
		return num;
	else return -num;
}


/**
* Construct a tree distance object.
* @param domine, domain tree.
* @param codomine, codomine tree.
*/
TreeDistance::TreeDistance(AbsTree *domine, AbsTree *codomine):DomainTree(domine),CoDomainTree(codomine)
{
	distance=-1.111;//to debug;
	firstAlignment =NULL;
}


TreeDistance::~TreeDistance(void)
{
	if(firstAlignment!=NULL)
		delete firstAlignment;

}

/**
* Creates an algignment object
*/
Alignment::Alignment(AbsTree * A, AbsTree * B){


Image = new int[A->size()];
CoImage = new int[B->size()];
DomainTree   =A;
CoDomainTree =B;
for (unsigned int i = 0; i<A->size(); i++)
	Image[i]=-1;
for (unsigned int i = 0; i<B->size(); i++)
	CoImage[i]=-1;
}


/**
* @return true if both alignments are equal
*/
bool Alignment::isEqual(Alignment* N){
	for (int i = 1; i<(int)DomainTree->size();i++)
		if(Image[i]!=N->Image[i]) return false;
	for (int i = 1; i<(int)CoDomainTree->size();i++)
		if(CoImage[i]!=N->CoImage[i]) return false;
	return true;
}





Alignment::~Alignment(){
	delete [] Image;
	delete [] CoImage; 
}

/**
* Matches nodes X in the domain tree to node Y in the co-domain tree and vice-versa
*/
void Alignment::set( int X, int Y){
	assert(X>=0);	
	assert(Y>=0);
	assert(Image[X]==-1);
	assert(CoImage[Y]==-1);
	Image[X]=Y;
	CoImage[Y]=X;
}


/**
* Matches nodes X in the domain tree to node Y in the co-domain tree and vice-versa only if they are not already matching other nodes.
*/
bool Alignment::secureSet(int X, int Y){
	if (Image[X]!=-1)
		return false;
	if (CoImage[Y]!=-1)
		return false;
	set(X,Y);
	return true;
}


/**
* Matches nodes X in the domain tree to node Y in the co-domain tree and vice-versa
*/
int Alignment::function(int PO){
	return Image[PO];
}

/** returns the domain tree of the alignment */
AbsTree * Alignment::getDomainTree(){
	return DomainTree;
}

/** returns the co-domain tree of the alignment */
AbsTree * Alignment::getCoDomainTree(){
	return CoDomainTree;
}

/** returns the domain tree */
AbsTree * TreeDistance::getDomainTree(){
	return DomainTree;
}

/** returns the co-domain tree */
AbsTree * TreeDistance::getCoDomainTree(){
	return CoDomainTree;
}

/** for a given position node of the co-domain it returns the position of the matched node in the domain tree */
int Alignment::inverseF(int PO){
	return CoImage[PO];
}
/*
void Alignment::shell_show(){
	cout << "Image: ";
	for (int i = 0; i<(int)DomainTree->size();i++)
		if (Image[i]!=-1)
			cout << i << "-->" << Image[i]<< " ";
	cout << endl << "CoImage: ";
	for (int i = 0; i<(int)CoDomainTree->size();i++)
		if (CoImage[i]!=-1)
			cout << CoImage[i]<< "<-- "<< i << " ";
			cout << endl;
}*/

/**
* shows the content of the alignment in the shell
*/
void Alignment::shell_show(){
	cout << "Image: ";
	for (int i = 0; i<(int)DomainTree->size();i++){
//		if(i==6)
//			cout << "here"<< endl;
		if (Image[i]!=-1){
			cout << i << "-->" << Image[i]<< "[" << DomainTree->PO_nodes[i]->map2cost(CoDomainTree->getNode(Image[i])) <<"] ";
		}
		else{
			cout << "delete("<< i<<")[" << DomainTree->PO_nodes[i]->deleteCost() << "] ";
		}
	}
	cout << endl << "CoImage: ";
	for (int i = 0; i<(int)CoDomainTree->size();i++)
		if (CoImage[i]!=-1){
			cout << CoImage[i]<< "<-- "<< i << "[" << DomainTree->PO_nodes[CoImage[i]]->map2cost(CoDomainTree->getNode(i)) <<"] ";
		}
		else{
			cout << "inserted("<< i<<")[" << CoDomainTree->PO_nodes[i]->createCost() << "] ";
		}
			cout << endl;
}


/**
* @returns the distance of the alignment
* the distance is re-calculated from the alignment
*/
double Alignment::recalculateDistance(){// ?????
	double total=0;
	for (int i = 0; i<(int)DomainTree->size();i++)
	{
		if (Image[i]==-1)//start in 0 or in 1???????
			total+= DomainTree->PO_nodes[i]->deleteCost();
		else {
			total+= DomainTree->PO_nodes[i]->map2cost(CoDomainTree->PO_nodes[Image[i]]);
		}
	}
	for (int i = 0; i<(int)CoDomainTree->size();i++)
	{
		if (CoImage[i]==-1)//start in 0 or in 1???????
			total+=CoDomainTree->PO_nodes[i]->createCost();
	}

	return total;

}

/**
* draws the alginment in a file in dot format
* @param f is the file in which it should be written.
*/
void Alignment::dot_show(ofstream& f) {
	assert(DomainTree);
	assert(CoDomainTree);
	int second = 1000;
	string shape;
	
	
	DepTree* Dtree = dynamic_cast<DepTree*>(DomainTree);

	if(Dtree) cout << "es un DEPTREE"<<endl;

	f << "compound=true;" << endl;

	f << "subgraph DomainTree{ " << endl;
	 // vector<unsigned int> sem_parents;
	  for(int x = 0; x < (int)DomainTree->PO_nodes.size(); x++) {//8-11-2010
		  string form = ((depTreeNodeInfo * )DomainTree->PO_nodes[x]->info)->getFormString();

		  if(Dtree) //es un deptree
		  {
			 
			  if(  ( (DepTreeNode *) DomainTree->PO_nodes[x] )->isPred())
				shape = ",shape=box";
			else
				shape="";
			  if(Image[x]==-1)
			  {
			 	  f << x << " [label=\"" << DomainTree->PO_nodes[x]->POindex <<form << "\",color=red"<< shape <<"];"<< endl;
			  }
			  else
			  {
				  f << x << " [label=\"" << DomainTree->PO_nodes[x]->POindex <<form << "\"" << shape <<"];"<< endl;
			  }

		  }
		  else
			    if(  ( (DepTreeNode *) DomainTree->PO_nodes[x] )->isPred())
				shape = ",shape=box";
		  if(Image[x]==-1)
			  f << x << " [label=\"" << DomainTree->PO_nodes[x]->POindex <<form << "\",color=red" << shape << "];"<< endl;
		  else   f << x << " [label=\"" << DomainTree->PO_nodes[x]->POindex <<form << "\""<< shape <<"];"<< endl;
	  }

	  for(int x = 0; x < (int)DomainTree->PO_nodes.size(); x++) { // 8-11-2010
		//  string deprel = ((depTreeNodeInfo * )DomainTree->PO_nodes[x]->info)->getDepRelation();
		 if(DomainTree->PO_nodes[x]->parent!=NULL){
			f <<  DomainTree->PO_nodes[x]->parent->POindex << " -> " << x <<  " [label=\"" << /*deprel <<*/ "\",color=blue];"<< endl;
		 }
	  }
	  /*
	  subgraphcluster1{
node[style=filled];
b0->b1->b2->b3;
label="process#2";
color=blue
} process #2
tart*/
	  f << "}" << endl ; // endsubgraph
	f << "subgraph CoDomainTree{  " << endl; // << "node[style=filled]; "<< endl << "label= \" CoDomain line=\""<<endl;
	  
	  for(int x = 0; x < (int)CoDomainTree->PO_nodes.size(); x++) {// 8-11-2010
		  string form = ((depTreeNodeInfo * )CoDomainTree->PO_nodes[x]->info)->getFormString();

		   
			if(  ( (DepTreeNode *) CoDomainTree->PO_nodes[x] )->isPred())
				shape = ",shape=box";
			else
				shape="";

		  if (CoImage[x]==-1)
			  f << x+second << " [label=\"" << CoDomainTree->PO_nodes[x]->POindex <<form << "\",color=red"<< shape <<"];"<< endl;
		  else  f << x+second << " [label=\"" << CoDomainTree->PO_nodes[x]->POindex <<form << "\"" << shape <<"];"<< endl;
	  }

	  for(int x = 0; x < (int)CoDomainTree->PO_nodes.size(); x++) {// 8-11-2010
		//  string deprel = ((depTreeNodeInfo * )DomainTree->PO_nodes[x]->info)->getDepRelation();
		 if(CoDomainTree->PO_nodes[x]->parent!=NULL){
			f <<  CoDomainTree->PO_nodes[x]->parent->POindex+ second << " -> " << x +second<<  " [label=\"" << /*deprel <<*/ "\",color=blue];"<< endl;
		 }
	  }
	  
	  f << "}" << endl;//"color=blue"<< endl <<" }" << endl ; // endsubgraph
	  

	  //for (int x= 0; x<(int)DomainTree->size();x++){// 8-11-2010
	  for (int x= 0; x<(int)DomainTree->PO_nodes.size();x++){// 8-11-2010
		  if (Image[x]!=-1){
			  if(	  0 == DomainTree->PO_nodes[x]->map2cost(CoDomainTree->PO_nodes[Image[x]])){
				f << x << " -> " << Image[x]+second << " [label=\"" <<  "\",constraint=false,color=green,style=bold];"<< endl;
			  }else 
				f << x << " -> " << Image[x]+second << " [label=\"" <<  "\",constraint=false,color=red,style=bold];"<< endl;
		  }
	  }
}
	
/**
* Draws and display itself using dot.
*/
void Alignment::dot_show(){
 ofstream f;
 string titulo = "alignment";
  // makes filename /tmp/bloggs_parse.dot
  // prints dot prologue
  string personal_parse = "hecfran";
//  string personal_parse(getenv("LOGNAME")); // NOT WORKING OVER WINDOWS
  //personal_parse = "/tmp/" + personal_parse + "_dep_tree";
  personal_parse = "./" + personal_parse + "_dep_tree";
  string dot_file;
  //c/out << personal_parse << endl;
  dot_file = personal_parse + ".dot";
  f.open(dot_file.c_str());
  f << "digraph "<< titulo <<" {\n";
  f << "center=true;";
  f << "ordering=out;\n";
  // f << "size=\"10,10\";\n"; // use this to try to fix size
  // prints the tree internals in dot language
  dot_show(f);
  // print dot epilogue
  f << "};";
  f.close();
  // make /tmp/bloggs_parse.ps
  string dot_command;
  dot_command = "dot -Tps " + dot_file + " -o " + personal_parse + ".ps";
  cout << dot_command << endl;
   system(dot_command.c_str());
   // show the postscript file
   string ghost_command;
   ghost_command = "gv " + personal_parse + ".ps";
   cout << ghost_command << endl;
   system(ghost_command.c_str());
   system("hecfran_dep_tree.ps");
   system("pause");
}



/**
* returns the aligment by changing the pointer al to the alignment object. (it avoids constructing copies of the alignment) 
*/
double TreeDistance::getAlignament( Alignment ** al){
	if (firstAlignment==NULL)
		build1AlginamentTables();
	///TODO ALIGNAMENT
	*al = firstAlignment;
	return distance;
}


/**
* @returns the distance
*/
double TreeDistance::getDistance(){
	if (distance == -1.111) //return distance;
		buildSimpleTables();
	
	if (params()->noralize){
		double denominator = ((DepSubTree*)DomainTree)->costDelete()+((DepSubTree*)CoDomainTree)->costInsert();
		return distance/denominator+(DomainTree->weight+CoDomainTree->weight);

	}
	return distance+(DomainTree->weight+CoDomainTree->weight);
}



/**
* @retun the node with Post order index corresponds to PO.
*/
AbsTreeNode * AbsTree::getNode(int PO){
	assert(PO>=0);
	assert(PO<(int)PO_nodes.size());
	return PO_nodes[PO];
}

/**
* @return true if X and Y match each other. 
*/
bool Alignment::match(int X, int Y){
	assert(X>=0);
	return (Image[X]==Y);
}


/**
* build the distance and alignment object.
*/
void TreeDistance::build1AlginamentTables()
{
	vector<vector<vector<SimpleTableNode> > > * forestTables;// all forest tables.
	vector<vector<SimpleTableNode> > * treeTable;

	forestTables = new vector<vector<vector<SimpleTableNode> > >;// all forest tables.
	treeTable = new vector<vector<SimpleTableNode> >;

	
	vector<int> * keyRootsA, *keyRootsB, *mostLeftA, *mostLeftB;

	keyRootsA = DomainTree->getKeyRoots();
	keyRootsB = CoDomainTree->getKeyRoots();
	mostLeftA = DomainTree->getMostLeft();
	mostLeftB = CoDomainTree->getMostLeft();	
	forestTables->resize((keyRootsA->size()+1) * (keyRootsB->size()+1));
	
	treeTable->resize(mostLeftA->size());

	for(unsigned int i =0; i<treeTable->size(); i++)
		(treeTable->at(i)).resize(mostLeftB->size());

	int counter = 0;
	for (unsigned int keyA = 0; keyA < keyRootsA->size(); keyA++)
		for (unsigned int keyB = 0; keyB < keyRootsB->size(); keyB++) {
			build1AlignamentTablesTreeDist((*keyRootsA)[keyA], (*keyRootsB)[keyB], counter,mostLeftA, mostLeftB, forestTables, treeTable ); // build tables one by one.
			counter++;
		}
/*		//
		cout<< "treeTable:" << endl;
		for(int i = 0; i < (int)treeTable->size();i++){
			for(int j = 0; j < (int)treeTable->at(i).size(); j++){
				cout << treeTable->at(i).at(j).value <<" ";
			}
			cout << endl;
		}// */
	int x = treeTable->size();
	int y = treeTable->at(x-1).size();
	distance = treeTable->at(x-1).at(y-1).value;

	firstAlignment= new Alignment(DomainTree , CoDomainTree);
	build1aligment(&treeTable->at(x-1).at(y-1));

	delete treeTable;
	delete forestTables;
	
}


/**
* build the distance and alignment object.
* recursive function
*/
void TreeDistance::build1aligment(SimpleTableNode* node){
	if (node->matchA!=-1)	firstAlignment->set(node->matchA,node->matchB);
	if (node->forestFrom)	build1aligment(node->forestFrom);
	if (node->treeFrom) 	build1aligment(node->treeFrom);
}


/**
* create the tree distance object using a lighter algorithm which do not allow to create the alignment. 
*/
void TreeDistance::buildSimpleTables(){
	vector<vector<vector<double> > > * forestTables;// all forest tables.
	vector<vector<double> > * treeTable;

	forestTables = new vector<vector<vector<double> > >;// all forest tables.
	treeTable = new vector<vector<double> >;

	
	vector<int> * keyRootsA, *keyRootsB, *mostLeftA, *mostLeftB;

	keyRootsA = DomainTree->getKeyRoots();
	keyRootsB = CoDomainTree->getKeyRoots();
	mostLeftA = DomainTree->getMostLeft();
	mostLeftB = CoDomainTree->getMostLeft();	
	forestTables->resize((keyRootsA->size()+1) * (keyRootsB->size()+1));
	
	treeTable->resize(mostLeftA->size());

	for(unsigned int i =0; i<treeTable->size(); i++)
		(treeTable->at(i)).resize(mostLeftB->size());

	int counter = 0;
	for (unsigned int keyA = 0; keyA < keyRootsA->size(); keyA++)
		for (unsigned int keyB = 0; keyB < keyRootsB->size(); keyB++) {
			buildSimpleTablesTreeDist((*keyRootsA)[keyA], (*keyRootsB)[keyB], counter,mostLeftA, mostLeftB, forestTables, treeTable ); // build tables one by one.
			counter++;
		}
		int x = treeTable->size();
		int y = treeTable->at(x-1).size();
		distance = treeTable->at(x-1).at(y-1);
		
	/*	cout<< "treeTable:" << endl;
		for(int i = 0; i <(int) treeTable->size();i++){
			for(int j = 0; j < (int)treeTable->at(i).size(); j++){
					cout << treeTable->at(i).at(j) <<" ";
			}
			cout << endl;
		}
		*/
		delete treeTable;
		delete forestTables;

	
}


/**
* This recursive function is part of the tree edit distance implementation
*/
void TreeDistance::build1AlignamentTablesTreeDist(int pos1, int pos2, int counter, vector<int> * mostLeftA, vector<int> * mostLeftB, vector<vector<vector<SimpleTableNode> > > * forestTables, vector< vector < SimpleTableNode > > * treeTable){
//vector<vector<double> > * fdist;
	AbsTree * treeA = DomainTree; 
	AbsTree * treeB = CoDomainTree;
	unsigned int boundA = pos1 - (*mostLeftA)[pos1] + 2;
	unsigned int boundB = pos2 - (*mostLeftB)[pos2] + 2;
	
	(*forestTables)[counter].resize(boundA);
	for (unsigned int i = 0; i < boundA; i++)
		(*forestTables)[counter][i].resize(boundB);

	//for(unsigned int i = 0; i<boundA; i++) // TO REMOVE
	//	for (unsigned int j = 0; j<boundB; j++)
	//		(*forestTables)[counter][i][j]= (double)0/(double)0; // 0/0 means NAN, it is just to be sure that we do not use nan number in the computation


	(*forestTables)[counter][0][0].value = 0; // row and column 0 have to be inicializated.
	for (unsigned int i = 1; i < (*forestTables)[counter].size(); i++) {
		AbsTreeNode * nodeA = this->DomainTree->getNode(i-1);// jhljhkjlhjklhkjlhkljhjkhjhjlkh

		(*forestTables)[counter][i][0].value = (double)(*forestTables)[counter][i - 1][0].value
		+ nodeA->createCost();
		(*forestTables)[counter][i][0].forestFrom= &(*forestTables)[counter][i - 1][0];// DEBE QUE SER VALIDO ELIMINAR ESTA LINEA
				//+ AtomicCost->cost(nodeA, NULL); // inicializated with the cost of creating the node.
		// 2/8/09
		//  (*forestTables)[counter][i][0].op1from = &(*forestTables)[counter][i - 1][0]; // and inicializated with where the values are piked from.
	}
	
	for (unsigned int i = 1; i < (double)(*forestTables)[counter][0].size(); i++) {
		double auxiliar = CoDomainTree->getNode(i-1)->deleteCost();// AtomicCost->cost(NULL, treeB->getPostNode(i));
		double auxiliar2 = (double)(*forestTables)[counter][0][i - 1].value;
		(*forestTables)[counter][0][i].value = auxiliar2 +  auxiliar;
		(*forestTables)[counter][0][i].forestFrom= &(*forestTables)[counter][0][i-1];// DEBE QUE SER VALIDO ELIMINAR ESTA LINEA
		// 2/8/09
		//   (*forestTables)[counter][0][i].op2from = &(*forestTables)[counter][0][i - 1];
	}
	/////////////////////////////
		double op1, op2, op3, op;  // ther are 3 options, op have the cheap of them.
	unsigned int k, i; // a
	unsigned int l, j; // b
	unsigned int m, n;

	for (k = (*mostLeftA)[pos1], i = 1; k <= (unsigned int) pos1; k++, i++) {
		for (l = (*mostLeftB)[pos2], j = 1; l <= (unsigned int) pos2; l++, j++) {
			if ((*mostLeftA)[pos1] == (*mostLeftA)[k] && (*mostLeftB)[pos2]
					== (*mostLeftB)[l]) {
				// two trees;
				AbsTreeNode * a;
				AbsTreeNode * b;
				a = treeA->getNode(k-1);//getPostNode(k);
				b = treeB->getNode(l-1);//getPostNode(l);
				op1 = (*forestTables)[counter][i - 1][j].value + b->createCost();//AtomicCost->cost(NULL,b);
				op2 = (*forestTables)[counter][i][j - 1].value + a->deleteCost();//AtomicCost->cost(a, NULL);
				op3 = (*forestTables)[counter][i - 1][j - 1].value + a->map2cost(b);//AtomicCost->cost(a, b);// node match.

				op = op1;
				if (op2 < op)
					op = op2;
				if (op3 < op)
					op = op3;

				if(op==op3)
				{
					(*forestTables)[counter][i][j].forestFrom=&(*forestTables)[counter][i - 1][j - 1];
					(*forestTables)[counter][i][j].matchA=k-1;
					(*forestTables)[counter][i][j].matchB=l-1;
				}
				else if(op==op2) (*forestTables)[counter][i][j].forestFrom=&(*forestTables)[counter][i][j - 1];
				else {(*forestTables)[counter][i][j].forestFrom=&(*forestTables)[counter][i - 1][j];
				assert(op==op1);}



				//hh added:
				// if op1 is the cheapes the previous cost was taken from foresTables[counter][i-1][j].
				// the same with op2.

	//			if (op==op1) (*forestTables)[counter][i][j].op1from = &(*forestTables)[counter][i - 1][j];
	//			if (op==op2) (*forestTables)[counter][i][j].op2from = &(*forestTables)[counter][i][j - 1];
	//			if (op==op3) {
	//				(*forestTables)[counter][i][j].op3from1 = &(*forestTables)[counter][i - 1][j - 1];
	//				(*forestTables)[counter][i][j].matchA = k;
	//				(*forestTables)[counter][i][j].matchB = l;
	//			}
				//save results
				(*forestTables)[counter][i][j].value = op;
				//(*treeTable)[k][l].copy( (*forestTables)[counter][i][j] );
				//cout << (*treeTable).size() << endl;
				//cout << (*treeTable)[k].size() << endl;

				(*treeTable)[k][l].copy((*forestTables)[counter][i][j]);
				// copia bien??????

			} else {
				// forest.
				m = (*mostLeftA)[k] - (*mostLeftA)[pos1]; // measuring the forest;
				n = (*mostLeftB)[l] - (*mostLeftB)[pos2]; // measuring the forest;

				op1 = (*forestTables)[counter][i - 1][j].value + treeB->getNode(l-1)->deleteCost();
				//+ AtomicCost->cost(NULL,treeB->getPostNode(l));
				op2 = (*forestTables)[counter][i][j - 1].value +  treeA->getNode(k-1)->createCost();
					//AtomicCost->cost(	treeA->getPostNode(k), NULL);
				op3 = (*forestTables)[counter][m][n].value + //treeA->getNode(k-1)->map2cost(treeB->getNode(l-1));
					  (*treeTable)[k][l].value;
				//	c/out << op1 <<" " << op2 << " " << op3 << endl;

				op = op1;
				if (op2 < op)
					op = op2;
				if (op3 < op)
					op = op3;

				//save results;
					(*forestTables)[counter][i][j].value = op;
					if (op3==op) {
						(*forestTables)[counter][i][j].forestFrom= &(*forestTables)[counter][m][n];
						(*forestTables)[counter][i][j].treeFrom= &(*treeTable)[k][l];
					}
					else if (op==op2) (*forestTables)[counter][i][j].forestFrom= & (*forestTables)[counter][i][j - 1];
					else (*forestTables)[counter][i][j].forestFrom = &(*forestTables)[counter][i - 1][j];
						
				//hh added:
			//	if (op==op1) (*forestTables)[counter][i][j].op1from = &(*forestTables)[counter][i - 1][j];
			//	if (op==op2) (*forestTables)[counter][i][j].op2from = &(*forestTables)[counter][i][j - 1];
			//	if (op==op3) {
			//	assert(i != m || j != n);/*{
			/*			cout <<"pos1:" << pos1 <<" pos2: " << pos2 << endl;
						cout << "i: " << i << " j: " << j << endl;
						cout << "error fatal " << endl;
						throw "error fatal";
					}*/
				// in option 3 with forest the values come from the fores table and the tree table.
			//		(*forestTables)[counter][i][j].op3from1 = &(*forestTables)[counter][m][n];
			//		(*forestTables)[counter][i][j].op3from2 = &(*treeTable)[k][l];
				}


			}

		}

	}


/**
* This recursive function is part of the tree edit distance implementation
*/
void TreeDistance::buildSimpleTablesTreeDist(int pos1, int pos2, int counter, vector< int > * mostLeftA, vector< int > * mostLeftB, vector< vector< vector< double > > > * forestTables, vector< vector< double > > * treeTable){
//vector<vector<double> > * fdist;
	AbsTree * treeA = DomainTree; 
	AbsTree * treeB = CoDomainTree;
	unsigned int boundA = pos1 - (*mostLeftA)[pos1] + 2;
	unsigned int boundB = pos2 - (*mostLeftB)[pos2] + 2;
	
	(*forestTables)[counter].resize(boundA);
	for (unsigned int i = 0; i < boundA; i++)
		(*forestTables)[counter][i].resize(boundB);

	//for(unsigned int i = 0; i<boundA; i++) // TO REMOVE
	//	for (unsigned int j = 0; j<boundB; j++)
	//		(*forestTables)[counter][i][j]= (double)0/(double)0; // 0/0 means NAN, it is just to be sure that we do not use nan number in the computation


	(*forestTables)[counter][0][0] = 0; // row and column 0 have to be inicializated.
	for (unsigned int i = 1; i < (*forestTables)[counter].size(); i++) {
		AbsTreeNode * nodeA = this->DomainTree->getNode(i-1);// jhljhkjlhjklhkjlhkljhjkhjhjlkh

		(*forestTables)[counter][i][0] = (double)(*forestTables)[counter][i - 1][0]
		+ nodeA->createCost();
				//+ AtomicCost->cost(nodeA, NULL); // inicializated with the cost of creating the node.
		// 2/8/09
		//  (*forestTables)[counter][i][0].op1from = &(*forestTables)[counter][i - 1][0]; // and inicializated with where the values are piked from.
	}
	
	for (unsigned int i = 1; i < (double)(*forestTables)[counter][0].size(); i++) {
		double auxiliar = CoDomainTree->getNode(i-1)->deleteCost();// AtomicCost->cost(NULL, treeB->getPostNode(i));
		double auxiliar2 = (double)(*forestTables)[counter][0][i - 1];
		(*forestTables)[counter][0][i] = auxiliar2 +  auxiliar;
		// 2/8/09
		//   (*forestTables)[counter][0][i].op2from = &(*forestTables)[counter][0][i - 1];
	}
	/////////////////////////////
		double op1, op2, op3, op;  // ther are 3 options, op have the cheap of them.
	unsigned int k, i; // a
	unsigned int l, j; // b
	unsigned int m, n;

	for (k = (*mostLeftA)[pos1], i = 1; k <= (unsigned int) pos1; k++, i++) {
		for (l = (*mostLeftB)[pos2], j = 1; l <= (unsigned int) pos2; l++, j++) {
			if ((*mostLeftA)[pos1] == (*mostLeftA)[k] && (*mostLeftB)[pos2]
					== (*mostLeftB)[l]) {
				// two trees;
				AbsTreeNode * a;
				AbsTreeNode * b;
				a = treeA->getNode(k-1);//getPostNode(k);
				b = treeB->getNode(l-1);//getPostNode(l);
				op1 = (*forestTables)[counter][i - 1][j] + b->createCost();//AtomicCost->cost(NULL,b);
				op2 = (*forestTables)[counter][i][j - 1] + a->deleteCost();//AtomicCost->cost(a, NULL);
				op3 = (*forestTables)[counter][i - 1][j - 1] + a->map2cost(b);//AtomicCost->cost(a, b);// node match.

				op = op1;
				if (op2 < op)
					op = op2;
				if (op3 < op)
					op = op3;



				//hh added:
				// if op1 is the cheapes the previous cost was taken from foresTables[counter][i-1][j].
				// the same with op2.

	//			if (op==op1) (*forestTables)[counter][i][j].op1from = &(*forestTables)[counter][i - 1][j];
	//			if (op==op2) (*forestTables)[counter][i][j].op2from = &(*forestTables)[counter][i][j - 1];
	//			if (op==op3) {
	//				(*forestTables)[counter][i][j].op3from1 = &(*forestTables)[counter][i - 1][j - 1];
	//				(*forestTables)[counter][i][j].matchA = k;
	//				(*forestTables)[counter][i][j].matchB = l;
	//			}
				//save results
				(*forestTables)[counter][i][j] = op;
				//(*treeTable)[k][l].copy( (*forestTables)[counter][i][j] );
				//cout << (*treeTable).size() << endl;
				//cout << (*treeTable)[k].size() << endl;

				(*treeTable)[k][l] = (*forestTables)[counter][i][j];
				

			} else {
				// forest.
				m = (*mostLeftA)[k] - (*mostLeftA)[pos1]; // measuring the forest;
				n = (*mostLeftB)[l] - (*mostLeftB)[pos2]; // measuring the forest;

				op1 = (*forestTables)[counter][i - 1][j] + treeB->getNode(l-1)->deleteCost();
				//+ AtomicCost->cost(NULL,treeB->getPostNode(l));
				op2 = (*forestTables)[counter][i][j - 1] +  treeA->getNode(k-1)->createCost();
					//AtomicCost->cost(	treeA->getPostNode(k), NULL);
				op3 = (*forestTables)[counter][m][n] + //treeA->getNode(k-1)->map2cost(treeB->getNode(l-1));
					  (*treeTable)[k][l];
					//c/out << op1 <<" " << op2 << " " << op3 << endl;

				op = op1;
				if (op2 < op)
					op = op2;
				if (op3 < op)
					op = op3;

				//save results;
					(*forestTables)[counter][i][j] = op;
				//hh added:
			//	if (op==op1) (*forestTables)[counter][i][j].op1from = &(*forestTables)[counter][i - 1][j];
			//	if (op==op2) (*forestTables)[counter][i][j].op2from = &(*forestTables)[counter][i][j - 1];
			//	if (op==op3) {
			//	assert(i != m || j != n);/*{
			/*			cout <<"pos1:" << pos1 <<" pos2: " << pos2 << endl;
						cout << "i: " << i << " j: " << j << endl;
						cout << "error fatal " << endl;
						throw "error fatal";
					}*/
				// in option 3 with forest the values come from the fores table and the tree table.
			//		(*forestTables)[counter][i][j].op3from1 = &(*forestTables)[counter][m][n];
			//		(*forestTables)[counter][i][j].op3from2 = &(*treeTable)[k][l];
				}


			}

		}
	}

/**
* Iniciate the values of a cell of a table.
*/
SimpleTableNode::SimpleTableNode(){
	value = -1;
	forestFrom = NULL;
	treeFrom = NULL;
	matchA = -1;
	matchB = -1;
}

/**
* copy the values from one cell to another.
*/
void SimpleTableNode::copy(SimpleTableNode & k){
	value = k.value;
	forestFrom = k.forestFrom;
	treeFrom = k.treeFrom;
	matchA = k.matchA;
	matchB = k.matchB;
	
}

//}

/**
* Similary to tree distance object, creates a tree kernel object.
*/
TreeKernel2::TreeKernel2(AbsTree *domine, AbsTree *codomine):DomainTree(domine),CoDomainTree(codomine)
{
	distance=-1;//to debug;
	firstAlignment =NULL;
}

TreeKernel2::~TreeKernel2(void)
{
	if(firstAlignment!=NULL)
		delete firstAlignment;
}

/**
* @return the kernel score 
*/
double TreeKernel2::getDistance(){
	if (distance == -1) //return distance;
		this->buildTables();//24-oct-2011
	return distance;
//	return getAlignament(&firstAlignment);//distance;
}

/**
* @param al returns the alginment by modifiying the parameter al.
*/
double TreeKernel2::getAlignament( Alignment ** al){
	if (firstAlignment==NULL)
		buildTables();
	///TODO ALIGNAMENT
	*al = firstAlignment;
	return distance+DomainTree->weight+CoDomainTree->weight;

}

/**
* Build the tables necesary to calculate the tree kernerl values.
*/
void TreeKernel2::buildTables(){
	int rows = this->DomainTree->PO_nodes.size();
	int cols = this->CoDomainTree->PO_nodes.size();
	vector<vector< double > > C_values;

	C_values.resize(cols);
	for( int i =0; i<cols; i++)
		C_values.at(i).resize(rows);

	for (int i=1; i<cols;i++)
		for (int j=1 ; j<rows ; j++)
 			C_values[i][j] = getCvalue(i,j,C_values);
	firstAlignment = makeAlignment(C_values);
}



/**
* Internal function to calculate the tree kernel value.
*/
bool TreeKernel2::increaseCounter(vector<int> & counter, int max){
	for (int i = counter.size()-1; i>=0 && i<max; i++){ //CHECK CODE!
		if (counter[i]<max-1)
		{
			counter[i]++;
			for (int j = i+1; j<(int)counter.size();j++)
				counter[j]=counter[j-1]+1;
			return true;
		}
		max--;
	}
	return false;
}

/**
* @return the doman tree
*/
AbsTree * TreeKernel2::getDomainTree(){
	return DomainTree;
}

/**
* @return the co-doman tree
*/
AbsTree * TreeKernel2::getCoDomainTree(){
	return CoDomainTree;
}

/**
* internal function to calculate the tree kernel value.
*/
double TreeKernel2::multiplySubChild(AbsTreeNode * nodeA, AbsTreeNode *nodeB, vector< vector < double > > & C_values, 	vector< vector < int > > & positionsA, vector< vector < int > > & positionsB){
	double cuenta = 0; 
	for (int i = 0; i<(int)positionsA.size();i++)
		for (int j=0; j<(int)positionsB.size();j++){
			double subcuenta = 1;

			for (int k = 0; k<(int)positionsA[i].size(); k++){
				int PO_childA = nodeA->children[positionsA[i][k]]->POindex;
				int PO_childB = nodeB->children[positionsB[j][k]]->POindex;
				double C_value= C_values[PO_childA][PO_childB];
				subcuenta = subcuenta*C_value;
			}
			cuenta += subcuenta;
		}
	return cuenta;
}

/**
* @retun the C value for two given nodes. it is used to contruct the C values table.
*/
double TreeKernel2::getCvalue(int domainNode, int coDomainNode, vector< vector < double > > & C_values){
	
	cout << domainNode << " " << coDomainNode << endl;
	AbsTreeNode * nodeA = DomainTree->PO_nodes.at(domainNode);
	AbsTreeNode * nodeB = CoDomainTree->PO_nodes.at(coDomainNode);

	double counter = 1;


	for (int i = 0 ; i < (int)nodeA->children.size(); i++){
		int posChildA = nodeA->children[i]->POindex;
		for (int j = 0; j< (int) nodeB->children.size();j++)
		{
			int  posChildB = nodeB->children[j]->POindex;
			counter += C_values[posChildA][posChildB];
		}

	}



	return -nodeA->map2cost(nodeB)*counter;












	/*


	int childrenA = nodeA->children.size();
	int childrenB = nodeB->children.size();
	int maxChildren = childrenA;
	if (childrenB<childrenA) maxChildren = childrenB;

	vector<vector<int> > positionsA, positionsB;
	
	double cuenta = 1;
	for (int ventana= 1; ventana<maxChildren; ventana++){
			
		vector<int> counterA;
		for (int i = 0; i<ventana;i++)
			counterA.push_back(i);

		do{
			positionsA.push_back(counterA);
		}
		while( this->increaseCounter(counterA,childrenA));
		
		vector<int> counterB;
		for (int i = 0; i<ventana;i++)
			counterB.push_back(i);

		do{
			positionsB.push_back(counterB);
		}
		while( this->increaseCounter(counterB,childrenB));

		cuenta +=multiplySubChild(nodeA,nodeB,  C_values,  positionsA,  positionsB);
		
	}
	cuenta = cuenta * nodeA->map2cost(nodeB)*(-1.0);  

	return cuenta;*/
}

/**
* This function is used to sort the potential matches inside the tree kernel and produce an alignment.
*/
bool mySortFuncion( pair<double,pair < int, int > > & a, pair< double , pair < int , int > > & b){

	double valA, valB;
	valA = a.first;
	valB = b.first;
	if (valA!=valB)
		return (valA<valB);

	int xA, xB;
	xA = a.second.first;
	xB = b.second.first;
	if (xA!=xB)
		return (xA<xB);
	return (a.second.second<b.second.second);
}

/**
* This function is used to sort the potential matches inside the tree kernel and produce an alignment.
*/
void mySortAlg(	vector<pair<double,pair<int,int> > > & positions){
	vector<pair<double,pair<int,int> > > aux = positions;
	
	positions.clear();
	make_heap(aux.begin(),aux.end(),*mySortFuncion);	 

	while(aux.size()>0){
		positions.push_back(aux.front());
		pop_heap(aux.begin(),aux.end(),*mySortFuncion);
			 //pop_heap(v.begin(),v.end(),*compare2);
		aux.pop_back();
	}


	return;
}

/**
* This function is used to sort the potential matches inside the tree kernel and produce an alignment.
*/
Alignment * TreeKernel2::makeAlignment(vector<vector<double> > & C_values){

	firstAlignment= new Alignment(DomainTree , CoDomainTree);

	vector<pair<double,pair<int,int> > > positions;
	pair<int,int> coordenate;
	pair<double,pair<int,int> > point;

	for (int i = 1; i<(int)C_values.size(); i++)
		for (int j = 1; j<(int)C_values[0].size();j++){
			coordenate.first=i;
			coordenate.second=j;
			point.first=-C_values[i][j];//negative kernel function.
			point.second=coordenate;
			positions.push_back(point);
		}

		
	//make_heap(positions.begin(),positions.end(),*mySortFuncion);	 
	//std::sort( positions.begin(), positions.end(), mySortFuncion);
	mySortAlg(positions);
	

	for (int i = 0; i<(int)positions.size();i++){
		coordenate = positions[i].second;
		firstAlignment->secureSet(coordenate.first, coordenate.second);
	}

	//firstAlignment->dot_show();
	return firstAlignment;


}

/**
* sets the tree distance to a new distance d
*/
void TreeDistance::setDistance(double d){
	distance = d;
}

/**
* sets the tree distance to a new distance d
*/
void TreeKernel2::setDistance(double d){
	this->distance=d;
}

/**
* sets the position in the vector to i
*/
void TreeKernel2::setVectorPossition(int i){
	positionVector = i;
}

/**
* @retun the position vector variable
*/
int TreeKernel2::getVectorPossition(){
	return positionVector;
}

/**
* sets the position vector variable to i
*/
void TreeDistance::setVectorPossition(int i){
	positionVector = i;
}

/**
* @return the position vector variable
*/
int TreeDistance::getVectorPossition(){
	return positionVector;
}




//MARTIN 26/06/2012

/**
* @return a string representing how to draw a node of a tree in dot.
* @author martin 
*/
string make_dot_node(string style, AtomicMeasureType cost_setting, string colour, int x, depTreeNodeInfo *theinfo, DepTreeNode *thenode, DepSubTree *thetree, ClassGlovalParameters & symbolTable) {
  
  stringstream dot_label_str;

  if(style == "record") {
    //TODO highlighting of pred
    dot_label_str << "[shape=record,label=";
    dot_label_str << "\"" << x << " | {";
    dot_label_str << theinfo->getFormString() << " | ";
    if(cost_setting == BINARY) {
      dot_label_str << theinfo->getPOS() << "-" << theinfo->getDepRelation();
    }
    else if(cost_setting == TERNARY) {
      dot_label_str << theinfo->getPOS() << " | ";
      dot_label_str << theinfo->getDepRelation();
    }
    else if(cost_setting == HAMMING) {
      dot_label_str << theinfo->getPOS() << " | ";
      dot_label_str << theinfo->getDepRelation() << " | ";
      dot_label_str << theinfo->getFormString(); 
    }
    else if(cost_setting == PREDICATE_MATCH) {
      dot_label_str << theinfo->getPOS() << " | ";
      dot_label_str << theinfo->getDepRelation() ;
      if(thenode->isPred()) {
		  dot_label_str <<  " | " << symbolTable.getString(thenode->predicate);
      }
    }

    // scan thru to see if this is an argument and if so include role label
    for(int i = 0; i < thetree->POApred.size(); i++) {
      if(thetree->POApred[i].first == x) {
	dot_label_str <<  " | " << thetree->POApred[i].second->getString();
      }
    }
    // if(thenode->isArg()) { dot_label_str << "isarg"; }
    // else { dot_label_str << "notarg"; }

    dot_label_str << "}\"";
  }
  else if(style == "ellipse") { //TODO catch up with record
    dot_label_str << "[shape=ellipse,label=";
    dot_label_str << "\"" << x << theinfo->getFormString() << "\\n ";
    dot_label_str << theinfo->getPOS() << "\\n ";
    dot_label_str << theinfo->getDepRelation();
    dot_label_str << "\"";
  }
  else { 
    dot_label_str << "[label=\"" << x << theinfo->getFormString();
    dot_label_str << "\"";
  }

  dot_label_str << ",color=" << colour;
  dot_label_str << "];";
  return dot_label_str.str();

}







/**
*Creates a tree kernel3 object (distance algorithm)
*/
TreeKernel3::TreeKernel3(AbsTree *domine, AbsTree *codomine){
	DomainTree = domine;
	CoDomainTree = codomine;
	distance=-1;
	firstAlignment=NULL;
	positionVector=-1;
}
void TreeKernel3::buildTables(){
	vector<vector<double> > table;
	table.resize(DomainTree->PO_nodes.size());
	for (int d = 0; d< DomainTree->PO_nodes.size();d++){
		table[d].resize(CoDomainTree->PO_nodes.size());
//		for (int c = 0; c< CoDomainTree->PO_nodes.size();c++){	}
	}
	distance = 0;
	for (int d = 0; d< DomainTree->PO_nodes.size();d++){
		for (int c = 0; c< CoDomainTree->PO_nodes.size();c++){
			table[d][c]=getCvalue(d,c,table);
			distance+= habs(table[d][c]);
		}
	}
	distance = -distance;
	firstAlignment= makeAlignment(table);

}

/**
* Get a C valu for two given nodes, it is used to build the table of C values.
*
*/
double TreeKernel3::getCvalue(int d, int c, vector< vector< double > > & table){
double total = 1;
AbsTreeNode * nodeD = DomainTree->PO_nodes.at(d);
AbsTreeNode * nodeC = CoDomainTree->PO_nodes.at(c);

for (int posD = 0; posD< nodeD->children.size();posD++)
	for (int posC = 0; posC< nodeC->children.size();posC++)
		total += habs(table[posD][posC]);

total = total*nodeD->map2cost(nodeC);
return habs(total);
}


/**
* Creates a 1-to-1 alginment out of the C valu table.
*/
Alignment * TreeKernel3::makeAlignment(vector < vector< double > > & C_values){

	firstAlignment= new Alignment(DomainTree , CoDomainTree);

	vector<pair<double,pair<int,int> > > positions;
	pair<int,int> coordenate;
	pair<double,pair<int,int> > point;

	for (int d = 0; d<(int)C_values.size();d++)
		for (int c = 0; c<(int)C_values[0].size();c++){
			coordenate.first=d;
			coordenate.second=c;
			point.first=C_values[d][c];//negative kernel function.
			point.second=coordenate;
			positions.push_back(point);
		}

	mySortAlg(positions);
	

	for (int i = 0; i<(int)positions.size();i++){
		coordenate = positions[i].second;
	//	firstAlignment->secureSet(coordenate.first, coordenate.second);
		firstAlignment->secureSet(coordenate.second, coordenate.first);

	}
	return firstAlignment;
}


/**
* @return the domain tree
*/
AbsTree * TreeKernel3::getDomainTree(){
	return DomainTree;}

/**
* @return the co-domain tree
*/
AbsTree * TreeKernel3::getCoDomainTree(){
	return CoDomainTree;
}


TreeKernel3::~TreeKernel3(void){
	if (firstAlignment!=NULL)
		delete firstAlignment;
}

/**
* @return the kernel score
*/
double TreeKernel3::getDistance(){
	if (distance==-1&& firstAlignment==NULL){
		buildTables();
	}
	return distance;
}

/**
* @return the kernel score
* sets the variable a to point the alignment
*/
double TreeKernel3::getAlignament(Alignment * * a){
	if (firstAlignment==NULL){
		buildTables();
	}
	*a = firstAlignment;
	return distance;
}


/**
* the the distance to d.
*/
void TreeKernel3::setDistance(double d){
	distance = d;
}

/**
* set the variable positionVector to i.
*/
void TreeKernel3::setVectorPossition(int i){
		positionVector = i;
}

/**
* @return the variable positionVector.
*/
int TreeKernel3::getVectorPossition(){
	
	return positionVector;
}



//martin 26/06/2012
/**
* @author martin
* display the alginment in dot format
* @param style to determine how much to show in a node
* @param cost_setting which atomic cost settings were used
* @param f file in which should be written.
*/
void Alignment::dot_show_new(string style, AtomicMeasureType cost_setting, ofstream& f) {
  assert(DomainTree);
  assert(CoDomainTree);
  int second = 1000;
  string shape;
	
  f << "compound=true;" << endl;

  // make subtgraph for Domain tree 
  f << "subgraph DomainTree{ " << endl;

  // vector<unsigned int> sem_parents;
  // make the nodes
  for(int x = 0; x < (int)DomainTree->PO_nodes.size(); x++) {//emms:20-6-2012
    depTreeNodeInfo *theinfo = ((depTreeNodeInfo *)DomainTree->PO_nodes[x]->info);
    DepTreeNode *thenode = ((DepTreeNode *)(DomainTree->PO_nodes[x]));

    DepSubTree *thetree = ((DepSubTree*)(DomainTree));

    f << x << " ";
    if(Image[x]==-1) { 
      f << make_dot_node(style, cost_setting,"red", x, theinfo, thenode, thetree,*params());
      f << endl;
    }
    else {
      f << make_dot_node(style, cost_setting,"black", x, theinfo, thenode, thetree,*params());
      f << endl;
    }

    // debugging    
 //    cout << "for " << theinfo->getFormString() << " predicate via depTreeNodeInfo: " << symbolTable.getString(theinfo->PRED) << endl;

 //    //    if(((DepSubTree *)DomainTree)->is_pred(x)) { 
 //    if(thenode->isPred()) {
 //     cout << "is predicate" << endl;
 //     int pred_code = 0;
 //     //pred_code = ((DepTreeNode *)(DomainTree->PO_nodes[x]))->predicate;
 //     pred_code = thenode->predicate;
 //     cout << "value of predicate is: " <<  pred_code << "decodes as: " <<  symbolTable.getString(pred_code) << endl; 
 // }
 //    else { cout << "is not predicate" << endl; }

 

    // if(style == "TernRec") {
    // //TODO highlighting of pred
    //  f << x << "[shape=record,label=";
    //  dot_label_str << "\"" << x << " | {";

    //  dot_label_str << theinfo->getFormString() << " | ";
    //  dot_label_str << theinfo->getPOS() << " | ";
    //  dot_label_str << theinfo->getDepRelation();
    //  dot_label_str << "}\"";
    //  f << dot_label_str.str();
    // }
    // else if(style == "TernEllipse") {
    // f << x << "[label=";
    // dot_label_str << "\"" << x << theinfo->getFormString() << "\\n ";
    // dot_label_str << theinfo->getPOS() << "\\n ";
    // dot_label_str << theinfo->getDepRelation();
    // dot_label_str << "\"";
    // }
    // else {}



    // f << "];" << endl;

  }

  // join up the nodes
  for(int x = 0; x < (int)DomainTree->PO_nodes.size(); x++) { // emms
    //  string deprel = ((depTreeNodeInfo * )DomainTree->PO_nodes[x]->info)->getDepRelation();
    if(DomainTree->PO_nodes[x]->parent!=NULL){
      f <<  DomainTree->PO_nodes[x]->parent->POindex << " -> " << x <<  " [label=\"" << /*deprel <<*/ "\"];"<< endl;
    }
  }
  f << "}" << endl ; // endsubgraph

  // make subgraph for CoDomainTree
  f << "subgraph CoDomainTree{  " << endl; 
	  
  for(int x = 0; x < (int)CoDomainTree->PO_nodes.size(); x++) {// emms
    depTreeNodeInfo *theinfo = ((depTreeNodeInfo *)CoDomainTree->PO_nodes[x]->info);
    DepTreeNode *thenode = ((DepTreeNode *)(CoDomainTree->PO_nodes[x]));
    
    DepSubTree *thetree = ((DepSubTree*)(CoDomainTree));

    f << x+second << " ";
    if(CoImage[x]==-1) { 
      f << make_dot_node(style, cost_setting, "red", x, theinfo, thenode, thetree,*params());
      f << endl;
    }
    else {
      f << make_dot_node(style, cost_setting, "black", x, theinfo, thenode, thetree,*params());

      f << endl;
    }


    // debugging    
 //    cout << "for " << theinfo->getFormString() << " predicate info: " << symbolTable.getString(theinfo->PRED) << endl;
 //    if(((DepSubTree *)CoDomainTree)->is_pred(x)) { 
 //     cout << "is predicate" << endl;
 //     int pred_code = 0;
 //     pred_code = ((DepTreeNode *)(CoDomainTree->PO_nodes[x]))->predicate;
 //     cout << "value of predicate is: " <<  pred_code << "decodes as: " <<  symbolTable.getString(pred_code) << endl; 
 // }
 //    else { cout << "is not predicate" << endl; }

    // stringstream dot_label_str;
    // dot_label_str << "\"" << x << " | {";
    // depTreeNodeInfo *theinfo = ((depTreeNodeInfo *)CoDomainTree->PO_nodes[x]->info);
    // dot_label_str << theinfo->getFormString() << " | ";
    // dot_label_str << theinfo->getPOS() << " | ";
    // dot_label_str << theinfo->getDepRelation();
    // dot_label_str << "}\"";
    
    // f << dot_label_str.str(); 

    // if(CoImage[x]==-1) { f << ",color=red"; }

    // f << "];" << endl;


 //    string form = ((depTreeNodeInfo * )CoDomainTree->PO_nodes[x]->info)->getFormString();
 //    if(  ( (DepTreeNode *) CoDomainTree->PO_nodes[x] )->isPred())
 //      shape = ",shape=box";
 //    else
 //      shape="";

 //    if (CoImage[x]==-1)
 //      f << x+second << " [label=\"" << CoDomainTree->PO_nodes[x]->POindex <<form
 // << "\",color=red"<< shape <<"];"<< endl;
 //    else  f << x+second << " [label=\"" << CoDomainTree->PO_nodes[x]->POindex <<form << "\"" << shape <<"];"<< endl;

  }

  for(int x = 0; x < (int)CoDomainTree->PO_nodes.size(); x++) {// emms
    //  string deprel = ((depTreeNodeInfo * )DomainTree->PO_nodes[x]->info)->getDepRelation();
    if(CoDomainTree->PO_nodes[x]->parent!=NULL){
      f <<  CoDomainTree->PO_nodes[x]->parent->POindex+ second << " -> " << x +second<<  " [label=\"" << /*deprel <<*/ "\"];"<< endl;
    }
  }
	  
  f << "}" << endl;//"color=blue"<< endl <<" }" << endl ; // endsubgraph
	  

  // draw arrows linking graphs
  for (int x= 0; x<(int)DomainTree->PO_nodes.size();x++){// emms

    if (Image[x]!=-1){
      if(0 == DomainTree->PO_nodes[x]->map2cost(CoDomainTree->PO_nodes[Image[x]])){
	f << x << " -> " << Image[x]+second << " [label=\"" <<  "\",constraint=false,color=green,style=bold];"<< endl;
      } else 
	f << x << " -> " << Image[x]+second << " [label=\"" <<  "\",constraint=false,color=red,style=bold];"<< endl;
    }
  }

}
	
// style to determine how much to show in a node


/**
* Display alignment in dot style.
* @param style to determine how much to show in a node
* @cost_setting cost_setting which atomic cost settings were used
*/
void Alignment::dot_show_new(string style, AtomicMeasureType cost_setting){
 ofstream f;
 string titulo = "alignment";
  // makes filename /tmp/bloggs_parse.dot
  // prints dot prologue
  string personal_parse = "hecfran";
  //  string personal_parse(getenv("LOGNAME")); // NOT WORKING OVER WINDOWS
  //personal_parse = "/tmp/" + personal_parse + "_dep_tree";
  personal_parse = "./" + personal_parse + "_dep_tree";
  string dot_file;
  //c/out << personal_parse << endl;
  dot_file = personal_parse + ".dot";
  f.open(dot_file.c_str());
  f << "digraph "<< titulo <<" {\n";
  f << "center=true;";
  f << "ordering=out;\n";
  // f << "size=\"10,10\";\n"; // use this to try to fix size
  // prints the tree internals in dot language
  dot_show_new(style,cost_setting,f);
  // print dot epilogue
  f << "};";
  f.close();
  // make /tmp/bloggs_parse.ps
  string dot_command;
  dot_command = "dot -Tps " + dot_file + " -o " + personal_parse + ".ps";
  cout << dot_command << endl;
   system(dot_command.c_str());
   // show the postscript file
   string ghost_command;
   ghost_command = "gv " + personal_parse + ".ps";
   cout << ghost_command << endl;
   system(ghost_command.c_str());
   system("hecfran_dep_tree.ps");
   system("pause");

}

