#include "stdafx.h"
//#include "StdAfx.h"
#include "DepTreeCorpus.h"
#include "TreeDistance.h"

#include <sstream>
#include <string>


#define MINDIFF  0.00001
double str2num (string cad){
	
double i;
bool success;
istringstream myStream(cad);

if (myStream>>i)
success = true;
else
success = false;

return i;
}


//bool compare (TreeDistance* i, TreeDistance* j) { 
//	return (i->getDistance() > j->getDistance());
//}
bool icompare (TreeMeasure* i, TreeMeasure* j) { 
	return (i->getDistance() < j->getDistance());//??????
}

bool compare (TreeMeasure* i, TreeMeasure* j) { 
	return (i->getDistance() > j->getDistance());//??????
}

//bool compare2 (TreeKernel* i, TreeKernel* j) { 
//	return (i->getDistance() > j->getDistance());
//}



string num2str(int num){
	string a ="";
	if (num == 0) return "0";
	
	while (num>0){
		a = (char)('0'+num%10)+a;
		num = num/10;
	}
	return a;
}


bool DepTreeCorpus::deltaTest(){
	DepTree * a;
	DepTree * b;
	a = this->DTcorpora[0];
	b = this->DTcorpora[10];
	Alignment *al1, *al2;
	double FirstDelta = params()->delta;

	params()->delta = -10;
	TreeDistance td(a,b);
	td.getAlignament(&al1);
	params()->delta = 12;
	TreeDistance td2(a,b);
	td2.getAlignament(&al2);



	params()->delta=FirstDelta;
	return	al1->isEqual(al2);
	
	
	
	//	DepTree a = this.
}

bool get_next_stanza(ifstream  &f,vector<string> &latest_stanza, int & counter) {
  latest_stanza.clear();

  bool in_stanza = false;
  string line = "";
  while(!(in_stanza) && f) {
    counter++; //hh 11/6/2009

    getline(f,line);
    //if(line != "") {
	if(line.size()>10){
      in_stanza = true;
      latest_stanza.push_back(line);
    } 
    else{
                cout << "warning: white line in file, line: " << counter << endl;
        }
  }
  if(!in_stanza) { return false; }

  else {
    bool finished_stanza = false;
    while((!finished_stanza) && f) {
        counter++; //hh 11/6/2009

      getline(f,line);
      //if(line == "") { 
	  if(line.size()<11){
		  finished_stanza = true; }
      else {
        latest_stanza.push_back(line);
      }
    }
    return finished_stanza;

  }

}
/*
void DepTreeCorpus::writeTableOfDistances(const char * file){
	
   ofstream myfile;
   myfile.open(file);

	for (int i = 0; i<(int)DTcorpora.size();i++){
		
		for (int j = 0; j<(int)DTcorpora.size();j++){
			TreeDistance *p = new TreeDistance(DTcorpora[i],DTcorpora[j]);

			myfile << p->getDistance() <<"\t";

		}
		myfile << endl;

	}
	myfile.close();
}
*/


int DepTreeCorpus::countAmountArguments(){
	int counter=0;
	for(int i=0;i<(int)this->samples.size();i++){
		counter+= samples[i]->POApred.size();
	}
	return counter;
}

void DepTreeCorpus::countIntermediateArguments(){
	int subTreeSize = samples.size();
	int dangersubtrees = 0;
//	for (int x=0; x<this->DTcorpora.size();x++){
//		this->DTcorpora[x]->showDangerSubTrees();
//	}
	cout << "list of sub-trees with intermediate node arguments: ";
	for (int x = 0; x<subTreeSize;x++)
		if (samples[x]->intermediateArguments()>0){
			dangersubtrees++;
			cout << samples[x]->line_number << ", ";

	//		samples[x]->dot_show();
		}
	cout << endl<<endl;
		
	




	cout << "there are" << samples.size() << " sub-trees" << endl;
	cout << dangersubtrees << " have argumens in internal nodes";

}

vector<string> split(string cad, char separator){
	vector<string> v;
	string aux=""; 
	for (int i = 0; i<(int)cad.size(); i++){
		if (cad[i]==separator)
		{
			v.push_back(aux);
			aux = "";
		}
		else aux+= cad[i];
	}
	v.push_back(aux);
	return v;
}




vector<AbsTreeNode*> DepTreeCorpus::ProcessPOSLine(string cad, int counter){
	vector<AbsTreeNode*> result;
	vector<string> tokens;
	tokens = split(cad,' ');

	string line = "0	R_	R_	R_	R_	R_	_	_	0	0	_	_	_";
	//1	The	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_	_
	result.push_back(new DepTreeNode(line,counter));

	for (int i = 0; i<(int)tokens.size() ; i++){

		vector<string> node = split(tokens[i],'/');//lemma/POS
		line = num2str(i+1) + " " +node[0] + "	" + node[0] + "	" + node[0] + "	" + node[1] + "	" + node[1]+ "	_	_	1	1	_	_	_	_	";
		result.push_back(new DepTreeNode(line,counter));
	}
	return result;
}


void DepTreeCorpus::ProcessDependency( vector<AbsTreeNode*> & v, string & cad){

//det(time-4, the-2)

	vector<string> t = split(cad,'(');
	string dependency = t[0];
	t = split(t[1],',');
	string n1 = t[0];
	string n2 = t[1];
	t = split(n1,'-');
	n1 = t[1];
	t = split(n2,'-');
	n2 = t[2];

	double pos1 = str2num(n1);
	double pos2 = str2num(n2);
//POR ACABAR :(

}


vector<vector<string> > ProcessPOSL(string cad, int counter){
	vector<vector <string> > result;
	vector<string> tokens;
	tokens = split(cad,' ');

	vector<string> line;
	line.push_back("1	R_	R_	R_	R_	R_	_	_	0	0	_	_	_	_");
	result.push_back(line);
	//1	The	the	the	DT	DT	_	_	4	4	NMOD	NMOD	_	_	_	_
	//result.push_back(new DepTreeNode(line,counter));

	for (int i = 0; i<(int)tokens.size() ; i++){
		line.clear();

		//ID	FORM	LEMMA	PLEMMA	POS	PPOS	FEAT	PFEAT	HEAD	PHEAD	DEPREL	PDEPREL	FILLPRED	PRED	APRED1
		vector<string> node = split(tokens[i],'/');//lemma/POS

		line.push_back(num2str(i+2)); //ID 0
		line.push_back(node[0]);   //FORM	1
		line.push_back(node[0]);   //LEMA	2
		line.push_back(node[0]);   //PLEMA	3
		line.push_back(node[1]);   //POS	4
		line.push_back(node[1]);   //PPOS	5
		line.push_back("_");	//FEAT		6
		line.push_back("_");	//PFEAT		7
		line.push_back("1");	//HEAD		8
		line.push_back("1");	//PHEAD		9
		line.push_back("root");	//DEPREL	10
		line.push_back("root");	//PDEPREL	11
		line.push_back("_");	//FILLPRED	12
		line.push_back("_");	//PRED		13
//		line = num2str(i+1) + " " +node[0] + "	" + node[0] + "	" + node[0] + "	" + node[1] + "	" + node[1]+ "	_	_	1	1	_	_	_	_	";
		//result.push_back(new DepTreeNode(line,counter));
		result.push_back(line);
		
	}
	return result;
}


void AddDependencyL(vector<vector <string> > & v,string cad){
	
//det(time-4, the-2)
	if (cad=="") return;
	vector<string> t = split(cad,'(');
	string dependency = t[0];
	t = split(t[1],',');
	string n1 = t[0];
	string n2 = t[1];
	t = split(n1,'-');
	n1 = t.back();
	t = split(n2,'-');
	n2 = t.back();
	t = split(n2,')');
	n2 = t[0];

	int pos1 = (int)str2num(n1);
	int pos2 = (int)str2num(n2);
//	v[pos2].at(8)=n1; //12 Feb 2011
//	v[pos2].at(9)=n1;
	v[pos2].at(8)=num2str(pos1+1);
	v[pos2].at(9)=num2str(pos1+1);
	v[pos2].at(10)=dependency;
	v[pos2].at(11)=dependency;

	return;


}


void TranslateFormat(const char * inputFile, char * outputFile){

  ifstream two_trees;
  int line_counter = 0;
 // int old_line_counter;
  string cad;

   ofstream myfile;
   myfile.open(outputFile);



  two_trees.open(inputFile);
  if(!two_trees) {
    cout << "failed to open\n";
    throw "failed to open";
  }
    while(getline(two_trees,cad)){
//  while(two_trees >> cad){
	  line_counter++;
	 // ProcessPOSL(cad,line_counter);
	  vector<vector<string> > v = ProcessPOSL(cad, line_counter);
	  
	  //two_trees >> cad ; //read white line;
	  getline(two_trees,cad);
	  line_counter++;
	  do{
		  getline(two_trees,cad);
		  // two_trees >> cad ;
		  line_counter++;
		  AddDependencyL(v,cad);
		  //add dependency link

	  }while(cad!="");


	  for (int i = 0; i<(int)v.size();i++){
		  for (int j = 0; j<(int)v[i].size();j++){
			  myfile << v[i][j];
			  if (j+1< (int) v[i].size()) myfile << "\t";
		  }
		  myfile << endl;
	  }
	  myfile<<endl;
	  v.clear();

  }

	 


  myfile.close();



}


DepTreeCorpus::DepTreeCorpus(const char * file_name, int format){
if (format == 1)
	cout << "loading standford-dependency parse";
	
  fileName = file_name; //recicled code.
  ifstream two_trees;
  int line_counter = 0;
//  int old_line_counter;
  string cad;

  two_trees.open(file_name);
  if(!two_trees) {
    cout << "failed to open\n";
    throw "failed to open";
  }
  
  while(two_trees >> cad){
	  line_counter++;
	  ProcessPOSLine(cad,line_counter);
	  vector<AbsTreeNode*> v = ProcessPOSLine(cad, line_counter);
	  
	  two_trees >> cad ; //read white line;
	  line_counter++;
	  do{
		  two_trees >> cad ;
		  line_counter++;
		//  AddDependencyL(v,cad);
		  //add dependency link

	  }while(cad!="");

  }
}


DepTreeCorpus::DepTreeCorpus(const char * file_name){
	
	
  fileName = file_name; //recicled code.
  ifstream two_trees;
  int line_counter = 1;
  int old_line_counter;
  int sentenceNumber = 0;

  two_trees.open(file_name);
  if(!two_trees) {
    cout << "failed to open\n";
	string cad(file_name);
	cout << cad<< endl;
    throw "failed to open";
  }
  vector<string> latest_stanza;

  bool no_last = true;
  int errors = 0;


  while (no_last){


    old_line_counter = line_counter;
    no_last = get_next_stanza(two_trees,latest_stanza, line_counter);
	sentenceNumber++;
	//cout << line_counter << endl;
    try{
    	DepTree * p;
    	p= new DepTree2(latest_stanza,old_line_counter);//5-april-2011
		//((AbsDepTree*)p)->dot_show();
    	DTcorpora.push_back(p);
		//cout << "processing line: " << old_line_counter << endl;
    }
    catch (const char * exception)
    {errors++;
	cout << "sentence:" << sentenceNumber << endl;
	cout << exception << endl;
	} //do nothing.
	if (sentenceNumber==params()->maxSentenses)
		break;

  }
  if (errors >1) params()->inform << "warning : " << errors << "sentences deleted from " << file_name << endl;

  /////////DTcorpora[0]->buildDepSubTrees();
 // return;
  for (int x = 0; x<(int)DTcorpora.size();x++){
	  DTcorpora[x]->buildDepSubTrees();
	  DTcorpora[x]->ready2TreeDistance();
	  for(int y = 0; y<(int)DTcorpora[x]->DepSubTrees.size();y++){
		  ///18-oct-2011

		  samples.push_back(DTcorpora[x]->DepSubTrees[y]);
		  DTcorpora[x]->DepSubTrees[y]->ready2TreeDistance();
	  }
  }
 // return;





  for (int x = 0; x<(int)samples.size();x++)
  {
	  depTreeNodeInfo * p;
	  int predicate =  samples[x]->predicateNode;
	  p = (depTreeNodeInfo * )samples[x]->PO_nodes[predicate]->info;
	  switch (p->getPOS()[0]){
	  case 'V':
	  	samplesVerbs.push_back(samples[x]);
		break;
	  case 'N':
	  	samplesNouns.push_back(samples[x]);
		//samples[x]->dot_show();
	  	break;
	  default:
		  samplesOthers.push_back(samples[x]);
		//  cout << "error, sub-tree " << samples[x]->line_number << " is not recognizes as verb or as noun" << endl; 
	  }

  }


  //cout << "verbs: "<< samplesVerbs.size() << endl;
  //cout << "nouns: "<< samplesNouns.size() << endl;
  //cout << "others: "<< samplesOthers.size() << endl;


  //10-feb-2012
  if (params()->splitDataSet){
	  	//vector<DepTree*>    DTcorpora;
		//vector<DepSubTree*> samples;
	  samples.clear();
	  if (params()->keepVebs)
		  samples = samplesVerbs;
	  
	  if (params()->keepNoums)
		  samples.insert(samples.end(), samplesNouns.begin(), samplesNouns.end());
		  //samples = samplesNouns;
  }

  //???? seems not working
  /*
  for (int x = 0; x<(int)DTcorpora.size();x++){
	//  DTcorpora[x]->buildDepSubTrees();
	  for(int y = 0; y<(int)DTcorpora[x]->POapreds.size();y++){
		  for (int z = 0; z<(int)DTcorpora[x]->POapreds[y].size();z++){
			  DepRelTree * p = new DepRelTree(DTcorpora[x], y,z);
			  relTrees.push_back(p);
			  //DepRelTree::DepRelTree(DepTree* sentence ,int predPosition, int argPosition)
			 
			//  p->ready2TreeDistance();
		  }
	  }
  
  }

  */

//  cacheIni = samples.size();
 // cachePointer = cacheIni;
  //cacheFin = cacheIni+params()->cacheSize;
   

}

/*
void weka(string  in, string out ){
		

ofstream salida(out.c_str());
salida << "@RELATION SemRole\n";
salida << "\n";
salida << "@ATTRIBUTE Predicate STRING\n";
salida << "@ATTRIBUTE Argument STRING\n";
salida << "@ATTRIBUTE PredicateRelation STRING\n";
salida << "@ATTRIBUTE ArgumentRelation STRING\n";
salida << "@ATTRIBUTE PredicatePOS STRING\n";
salida << "@ATTRIBUTE ArgumentPOS STRING\n";
salida << "@ATTRIBUTE PredicateLemma STRING\n";
salida << "@ATTRIBUTE ArgumentLemma STRING\n";
salida << "@ATTRIBUTE PredicateHead STRING\n";
salida << "@ATTRIBUTE ArgumentHead STRING\n";
salida << "@ATTRIBUTE ArgumentPosition STRING\n";
salida << "@ATTRIBUTE SemanticRelation STRING\n";
salida << "\n";
salida << "@DATA";
salida << "\n";

//		 relTrees[i]->
//	 






  ifstream two_trees;
  int line_counter = 1;
  int old_line_counter;
  int sentenceNumber = 0;

  two_trees.open(in.c_str());
  if(!two_trees) {
    cout << "failed to open\n";
    throw "failed to open";
  }
  vector<string> latest_stanza;

  bool no_last = true;
  int errors = 0;
  while (no_last){


    old_line_counter = line_counter;
    no_last = get_next_stanza(two_trees,latest_stanza, line_counter);
	sentenceNumber++;
	cout << line_counter << endl;
    try{
    	DepTree * p;
    	p= new DepTree2(latest_stanza,old_line_counter);//5-april-2011
		//((AbsDepTree*)p)->dot_show();
    	//DTcorpora.push_back(p);
		//cout << "processing line: " << old_line_counter << endl;


		 for(int y = 0; y<(int)p->POapreds.size();y++){
		  for (int z = 0; z<(int)p->POapreds[y].size();z++){
			  DepRelTree * pp  = new DepRelTree(p, y,z);
			  pp->saveWEKA(salida);
			  delete pp;
		  }
		 }
		 delete p;
    }
    catch (const char * exception)
    {errors++;
	cout << "sentence:" << sentenceNumber << endl;
	cout << exception << endl;
	} //do nothing.

  }
  if (errors >1) params()->inform << "warning : " << errors << "sentences deleted from " << in << endl;

 salida.close();
}

void DepTreeCorpus::addToCache(DepSubTree* tree, double weight){
	if (cacheFin==cacheIni) return; //there is no cache.
	if ((int)samples.size()<cacheFin)
		samples.push_back(tree);
	else{
		samples[cachePointer]= tree;
		cachePointer++;
		if (cachePointer==cacheFin)
			cachePointer=cacheIni;
	}

	tree->weight= weight;
	return;
}
*/

void DepTreeCorpus::describeData(){
	cout << "file name: \t"<< this->fileName<< endl;
	cout << "sentences: \t"<< this->DTcorpora.size()<< endl;
	cout << "sub-trees: \t"<< this->samples.size()<< endl;
	cout << "Verbs sub-trees: \t" << this->samplesVerbs.size() << endl;
	cout << "Nouns sub-trees: \t" << this->samplesNouns.size() << endl;
	cout << "other sub-trees: \t" << samplesOthers.size() << endl;
	//cout << "labels:    "<< this->relTrees.size()<<endl;
	cout << "total arguments: \t" << this->countAmountArguments()<< endl;

	cout << "semantic structures entropy with    Predicate= " << getSemanticStructuresEntropy(true)<< endl;
	cout << "semantic structures entropy without Predicate= " << getSemanticStructuresEntropy(false)<< endl;


	////AVERAGE SUB-TREE SIZE!.

	double subtreesize = 0;
	for (int i=0;i<samples.size();i++)
		subtreesize+=samples[i]->getNumberOfNodes();
	cout << "sub-tree size: \t"<< subtreesize/samples.size() << endl;



	double branching_factor=0;
	double removedsamples=0;
	for (int i = 0; i<(int)samples.size();i++){
		if (samples[i]->PO_nodes.size()>1)
			branching_factor += ((AbsTree*)samples[i])->no_leaf_branch_factor();
		else removedsamples++;
	}
	branching_factor = branching_factor/(samples.size()-removedsamples);
	cout << "branching factor: \t"<< branching_factor << endl;
	
	branching_factor=0;
	removedsamples=0;
	for (int i = 0; i<(int)samplesVerbs.size();i++){
		if (samplesVerbs[i]->PO_nodes.size()>1)
			branching_factor += ((AbsTree*)samplesVerbs[i])->no_leaf_branch_factor();
		else removedsamples++;
	}
	branching_factor = branching_factor/(samplesVerbs.size()-removedsamples);
	cout << "branching factor Verbs: \t"<< branching_factor << endl;
	
	branching_factor=0;
	removedsamples=0;
	for (int i = 0; i<(int)samplesNouns.size();i++){
		if (samplesNouns[i]->PO_nodes.size()>1)
			branching_factor += ((AbsTree*)samplesNouns[i])->no_leaf_branch_factor();
		else removedsamples++;
	}
	branching_factor = branching_factor/(samplesNouns.size()-removedsamples);
	cout << "branching factor Nouns: \t"<< branching_factor << endl;
	
	branching_factor=0;
	removedsamples=0;
	for (int i = 0; i<(int)samplesOthers.size();i++){
		if (samplesOthers[i]->PO_nodes.size()>1)
			branching_factor += ((AbsTree*)samplesOthers[i])->no_leaf_branch_factor();
		else removedsamples++;
	}
	branching_factor = branching_factor/(samplesOthers.size()-removedsamples);
	cout << "branching factor Others: \t"<< branching_factor << endl;
	


	



	int maxsize = 100;
	vector<int> sizesVerbs(maxsize);
	vector<int> sizesNouns(maxsize);
	vector<int> sizesOthers(maxsize);
	vector<int> sizes(maxsize);

	vector<int> rootPredicate(maxsize);
	vector<int> rootArgument(maxsize);
	vector<int> leavePredicate(maxsize);
	
	/////////////
	int ArgumentsInTreesRootPredicate = 0;
	int totalArguments=0;
	for (int i = 0; i<this->samples.size();i++)
	{
		if (samples[i]->rootIsPredicate())
			ArgumentsInTreesRootPredicate+= samples[i]->POApred.size();
		totalArguments += samples[i]->POApred.size();
		
	}
	cout << "percentage of arguments in predicate rooted sub-tree: \t"<< (((double)ArgumentsInTreesRootPredicate)/((double)totalArguments))  << endl;
	/////////////










	for (int i = 0; i<maxsize;i++) {
		sizesVerbs[i]=0;
		sizesNouns[i]=0;
		sizes[i]=0;
		rootPredicate[i]=0;
		rootArgument[i]=0;
		leavePredicate[i]=0;
	}
	for (int i = 0; i<(int)samplesVerbs.size();i++){
		int newsize = this->samplesVerbs[i]->PO_nodes.size();
		if (newsize<maxsize) sizesVerbs[newsize]++;
	}
	
	for (int i = 0; i<(int)samplesNouns.size();i++){
		int newsize = this->samplesNouns[i]->PO_nodes.size();
		if (newsize<maxsize) sizesNouns[newsize]++;
	}
	
	for (int i = 0; i<(int)samplesOthers.size();i++){
		int newsize = this->samplesOthers[i]->PO_nodes.size();
		if (newsize<maxsize) sizesOthers[newsize]++;
	}

	for (int i = 0; i<maxsize;i++) {
		sizes[i]= sizesVerbs[i]+sizesNouns[i]+sizesOthers[i];
	}

	//--------

	for (int i = 0; i<(int)samples.size();i++){
			int newsize = this->samples[i]->PO_nodes.size();
			if (newsize>=maxsize) continue;
			if (samples[i]->rootIsPredicate())
				rootPredicate[newsize]++;
			if (samples[i]->rootIsArgument())
				rootArgument[newsize]++;
			if (samples[i]->leavePredicate())
				leavePredicate[newsize]++;
	}
	//--------


	double averageSize = 0;
	for (int i = 0; i<(int)sizes.size();i++)
		averageSize += i*sizes[i];
	averageSize= (averageSize/samples.size());
	cout << "average size of sub-tree: \t"<< averageSize << endl;
	
	
	averageSize = 0;
	for (int i = 0; i<(int)sizesVerbs.size();i++)
		averageSize += i*sizesVerbs[i];
	averageSize= (averageSize/samplesVerbs.size());
	cout << "average size of Verb sub-tree: \t"<< averageSize << endl;

	
	averageSize = 0;
	for (int i = 0; i<(int)sizesNouns.size();i++)
		averageSize += i*sizesNouns[i];
	averageSize= (averageSize/samplesNouns.size());
	cout << "average size of Noun sub-tree: \t"<< averageSize << endl;
	
	
	averageSize = 0;
	for (int i = 0; i<(int)sizesOthers.size();i++)
		averageSize += i*sizesOthers[i];
	averageSize= (averageSize/samplesOthers.size());
	cout << "average size of Other sub-tree: \t"<< averageSize << endl;



	
	//-----------------------------------------------------------------------------------------------




	





	averageSize = 0;
	for (int i = 0; i<(int)sizes.size();i++)
		averageSize += rootPredicate[i];
	averageSize= (averageSize/samples.size());
	cout << "Percentage of subtrees predicate rooted: \t"<< 100.00*averageSize << endl;

	averageSize = 0;
	for (int i = 0; i<(int)sizes.size();i++)
		averageSize += rootArgument[i];
	averageSize= (averageSize/samples.size());
	cout << "Percentage of subtrees argument rooted: \t"<< 100.00*averageSize << endl;

	averageSize = 0;
	for (int i = 0; i<(int)sizes.size();i++)
		averageSize += leavePredicate[i];
	averageSize= (averageSize/samples.size());
	cout << "Percentage of subtrees predicate leaved: \t"<< 100.00*averageSize << endl;




	
//	while(maxsize > 0  && sizes[maxsize-1]==0)
//		maxsize--;
	cout << "sub-tree size:\t totalTrees \t totalTreesVerbs \t totalTreesNouns \t totalOtherTrees \t"<<
		"percentageRootPredicate \t percentageRootArgument \t leavePredicate"<< endl ;
	for (int i = 0; i<maxsize;i++)
		if (sizes[i]!=0)
		//	cout << "size:\t"<< i << "\t& " << sizes[i] << "\t& "<< sizesVerbs[i] <<" \t& " << sizesNouns[i]  << " \t& " << sizesOthers[i] << "\t& "
		//	<< (100.00*(double)rootPredicate[i])/sizes[i] << "\% &\t" << (100.00*(double)rootArgument[i])/sizes[i] << "\% &\t"  << (100.00*(double)leavePredicate[i])/sizes[i] << "\%"  
		//	<< endl;
			cout << "size:\t"<< i << "\t& " << sizes[i] <<  "\t& "
			<< (100.00*(double)rootPredicate[i])/sizes[i] << "\% &\t" << (100.00*(double)rootArgument[i])/sizes[i] << "\% &\t"  << (100.00*(double)leavePredicate[i])/sizes[i] << "\% \\\\"  
			<< endl;
	






	//------------------------------------------
	vector<pair<int,int> > labels;
	double totalLabels;
	totalLabels = getSemRel(labels);
	cout << "Semantic Labels:" << endl;
	for (int i = 0; i<(int)labels.size();i++){
		cout << params()->getString(labels[i].first)<< "&\t"<< labels[i].second << "&\t" << scientific <<  100.0*labels[i].second/totalLabels << "\\\% \\\\"<<endl;
	}
	labels.clear();
	totalLabels = getSintacticRel(labels);
	cout << "Dependency Relation Labels on the sub-trees:" << endl;
	for (int i = 0; i<(int)labels.size();i++){
		cout << params()->getString(labels[i].first)<< "&\t"<< labels[i].second <<"&\t" << scientific << 100.0*labels[i].second/totalLabels << "\\\% \\\\"<< endl;
	}


	//-----------------------------------------------------------------------
	 
}


bool compareLabel(const pair < int, int  > & i, const pair < int, int  > & j) 
{
	return (i.second > j.second);
}

/**
*	24-oct-2010
*	It puts in semRel a vector of all possible labels and
*	in conter a vector of how many times that label apears. 
*   hector
*/											//semantic relation, counter;
int DepTreeCorpus::getSemRel(vector< pair < int, int  > > & semCounter){
	int counter=0;
	for (int sam = 0; sam<(int)this->samples.size();sam++){
		DepSubTree* s = samples[sam];
		for (int arg = 0; arg<(int)s->POApred.size();arg++){
			int argumento = s->POApred[arg].second->getRelation();
			//search and add
			int position;
			counter++;
			for ( position = 0; position<(int)semCounter.size();position++)
				if (semCounter[position].first==argumento) 
					break;
			if (position==semCounter.size()){
				pair<int,int> a;
				a.first= argumento;
				a.second=0;
				semCounter.push_back(a);
			}
			
			semCounter[position].second++;

		}
	} //end counting semantic relations.
	std::sort(semCounter.begin(), semCounter.end(), compareLabel);
	return counter;
}


/**
*	24-oct-2010
*	It puts in semRel a vector of all possible labels and
*	in conter a vector of how many times that label apears. 
*   hector
*/		
int DepTreeCorpus::getSintacticRel(vector< pair < int, int  > > & depRelCounter){
	int counter=0;
	for (int sam = 0; sam<(int)this->samples.size();sam++){
		DepSubTree* s = samples[sam];
		for (int nod = 0; nod<(int)s->PO_nodes.size();nod++){
			AbsTreeNode *n =  s->PO_nodes[nod];
			depTreeNodeInfo* info = (depTreeNodeInfo*) n->info;
			counter++;
			int label = info->DEPREL;
			int position;
			for ( position = 0; position<(int)depRelCounter.size();position++)
				if (depRelCounter[position].first==label) 
					break;
			if (position==depRelCounter.size()){
				pair<int,int> a;
				a.first= label;
				a.second=0;
				depRelCounter.push_back(a);
			}
			depRelCounter[position].second++;
		}	
	} //end counting semantic relations.
	std::sort(depRelCounter.begin(), depRelCounter.end(), compareLabel);
	return counter;
}

/*
void DepTreeCorpus::saveRelTrees(string FileName){
		 ofstream salida(FileName.c_str());
   salida << "@RELATION SemRole\n";
salida << "\n";
salida << "@ATTRIBUTE Predicate STRING\n";
salida << "@ATTRIBUTE Argument STRING\n";
salida << "@ATTRIBUTE PredicateRelation STRING\n";
salida << "@ATTRIBUTE ArgumentRelation STRING\n";
salida << "@ATTRIBUTE PredicatePOS STRING\n";
salida << "@ATTRIBUTE ArgumentPOS STRING\n";
salida << "@ATTRIBUTE PredicateLemma STRING\n";
salida << "@ATTRIBUTE ArgumentLemma STRING\n";
salida << "@ATTRIBUTE PredicateHead STRING\n";
salida << "@ATTRIBUTE ArgumentHead STRING\n";
salida << "@ATTRIBUTE ArgumentPosition STRING\n";
salida << "@ATTRIBUTE SemanticRelation STRING\n";
salida << "\n";
salida << "@DATA";
salida << "\n";
	 for (int i = 0; i<(int)relTrees.size();i++)
		 relTrees[i]->saveWEKA(salida);
	 salida.close();
}
*/

void DepTreeCorpus::saveMatrix(string FileName){
	 ofstream salida(FileName.c_str());
	 int sizeMatrix = DTcorpora.size();
	 vector<TreeDistance*> v;
	 
	 for (int i = 0; i<(int)DTcorpora.size();i++){
		 for (int j = 0; j<(int)DTcorpora.size();j++){
			 TreeDistance *d = new TreeDistance(DTcorpora[i],DTcorpora[j]);
			 v.push_back(d);
			 d->getDistance();
		 }
		 sort (v.begin(), v.end(), *icompare);

		 salida << DTcorpora[i]->line_number;
		 int counter=0;
		 for (int j = 0; j<(int)v.size();j++){
			 salida << " "<<v[j]->getCoDomainTree()->line_number<<":"<< v[j]->getDistance();

//			 Alignment *al;
			/* if (i==0 && v[j]->getCoDomainTree()->line_number==5445){
				 v[j]->getAlignament(&al);
				 al->dot_show();
				 al->shell_show();
				 system("pause");
			 }*/
			 delete v[j];
			 counter++;
		 }
		 if (counter != sizeMatrix){
			cout<< "error in the code";
			return;
		 }
		 salida << endl;
		 v.clear();
		 cout << 100*(double)i/(double)DTcorpora.size() << " % percentage done" << endl;
		 
	 }
	 cout << "matrix size: " << sizeMatrix << endl; 
	 salida.close();
}

void DepTreeCorpus::save(string FileName){
	 ofstream salida(FileName.c_str());
	 for (int i = 0; i<(int)DTcorpora.size();i++)
		 DTcorpora[i]->save(salida);
	 salida.close();
}

void DepTreeCorpus::largeScaleTestSubTrees(){
	vector< vector < double > > table;
	double distance1;
	double distance2;
	Alignment * al = NULL;
	TreeDistance *p;
	table.resize(this->samples.size());
	for (unsigned int i =0 ; i< samples.size() ;  i++){
		table.at(i).resize(samples.size());
		cout << "sentence " << i << endl;
		for (unsigned int j = 0; j<samples.size(); j++){
			//((AbsDepTree*)samples[i])->dot_show();
			cout << "size:" << ((AbsDepTree*)samples[i])->PO_nodes.size() << endl;
			p = new TreeDistance(samples[i],samples[j]);
			distance1 = p->getDistance();
			distance2 = p->getAlignament(&al);
			al->dot_show();
			if (distance1!=distance2)
				cout << "panic: distance1 != distance2" << endl;
			table.at(i).at(j)=distance1;


			delete p;
		}
	}
	
	for (unsigned int i =0 ; i< samples.size() ;  i++){
		
		for (unsigned int j = 0; j<samples.size(); j++){
			cout << table[i][j] << " ";
		}
		cout << endl;
	}
	cout << "hay " << samples.size() << " sub arboles" << endl;
}





void DepTreeCorpus::LargeScaleTest(){
//	return;

	vector< vector < double > > table;
	double distance1;
	double distance2;
	Alignment ** al = NULL;
	TreeDistance *p;
	table.resize(this->DTcorpora.size());
	for (unsigned int i =0 ; i< DTcorpora.size() ;  i++){
		table.at(i).resize(DTcorpora.size());
		cout << "sentence " << i << endl;
		for (unsigned int j = 0; j<DTcorpora.size(); j++){
			p = new TreeDistance(DTcorpora[i],DTcorpora[j]);
			distance1 = p->getDistance();
			distance2 = distance1;//p->getAlignament(al);
			if (distance1!=distance2)
				cout << "panic: distance1 != distance2" << endl;
			table.at(i).at(j)=distance1;
			delete p;
		}
	}
	cout << "sentences: "<< DTcorpora.size() << endl;
	for (unsigned int i =0 ; i< DTcorpora.size() ;  i++){
		
		for (unsigned int j = 0; j<DTcorpora.size(); j++){
			cout << table[i][j] << " ";
		}
		cout << endl;
}
}


/*
void DepTreeCorpus::LargeScaleTestKernel(){
//	return;

	vector< vector < double > > table;
	double distance1;
	double distance2;
	Alignment ** al = NULL;
	Alignment *al2;
	al = & al2;
	TreeKernel *p;
	table.resize(this->DTcorpora.size());
	for (unsigned int i =0 ; i< DTcorpora.size() ;  i++){
		table.at(i).resize(DTcorpora.size());
		cout << "sentence " << i << endl;
		for (unsigned int j = 0; j<DTcorpora.size(); j++){
			p = new TreeKernel(DTcorpora[i],DTcorpora[j]);
			distance1 = p->getDistance();
			distance2 = p->getAlignament(al);
			if (distance1!=distance2)
				cout << "panic: distance1 != distance2" << endl;
			table.at(i).at(j)=distance1;
			delete p;
		}
	}
	cout << "sentences: "<< DTcorpora.size() << endl;
	for (unsigned int i =0 ; i< DTcorpora.size() ;  i++){
		
		for (unsigned int j = 0; j<DTcorpora.size(); j++){
			cout << table[i][j] << " ";
		}
		cout << endl;
}
}
*/


bool lineHave11items(string line){

istringstream buffer(line);
string value;
int i = 0;
while(buffer)
{
	buffer >> value;
	i++;
}
return (i>=11);
	
}

int getLines(string file){
	// returns how many lines a log file has. 
	// if file doent exit creates the file
 	int counter = 0;
	string auxfile = "aux"+file;
	ifstream ifile(file.c_str());
	
	if (!ifile){
		ifile.close();

		ofstream	outputFile;
		outputFile.open(file.c_str());
		outputFile.close();
		return 0;
	}
	else 	ifile.close();


	// if the file exist
	//copy the file if the lines are ok.
  string line;
  ifstream inputfile (file.c_str());
  ofstream outputfile (auxfile.c_str());
 // if (inputfile.is_open())
 // {
    while ( inputfile.good() )
    {
      getline (inputfile,line);
	  if (lineHave11items(line)){
		 outputfile << line << endl;
		 counter++;
	  }
	  else break;
    }
    inputfile.close();
	outputfile.close();
  //}

//  system("pause");
  // copy back the file;
  string line2;

  if (counter >10) counter -=2;

  ifstream auxiliar(auxfile.c_str());
  ofstream original(file.c_str());
  if (auxiliar.is_open())
  {
  //  while ( auxiliar.good() )
	for (int i = 0; i<counter;i++)
    {
      getline(auxiliar,line2);
	  //if (lineHave11items(line2)){
	  if (line2.length()>6){
		  original << line2 << endl;
		 // counter++;
	  }
	//  }
	  else break;
    }
    auxiliar.close();
	original.close();
  }
  else
	  cout << "auxiliar no abre!";


	
  //system("pause");
  remove(auxfile.c_str());
  return counter;
}
/*
void DepTreeCorpus::reLabelBrackPoints(DepTreeCorpus & query){

	ofstream salida;
	int correct=0;
	int total=0;
	string cad=  params()->stringTAG + ".txt";

	int counter = getLines(cad);
	cout << "total lines: "<< counter << endl;



	salida.open(cad.c_str(),ios::app);

	cout << "relabelling process"<< endl;
	if (counter == 0)
	salida << "targetLine\ttargetPredicate\told_label\tTargetNNodes\tsourceLine\tsourcePredicate\tnew_label\tSourceNNodes\tdistance\tsamples_at_same_distance\tNtreesCheked" << endl;
	else counter--;
	////////
	for(int position = 0; position<(int)query.samples.size() ;position++)
	{
		cout << ". complete: " << (position/((double)query.samples.size())*100)<< "%"<<   "   \trelabeling subtree n:" << position << " from line: " <<  query.samples[position]->line_number << endl;
	//	if (params()->K_for_KNN!=1)
			 KNN3relabel(query.samples[position],salida,correct, total, counter);
	//	else reLabel(query.samples[position],salida,correct, total); //30-09-2011
		salida.flush();
//		if(params()->cacheSize>0)
//			addToCache(query.samples[position],0);//ADDING TO CACHE
	
	}
	salida.close();
	params()->accuracy=((double)correct)/((double)total);
	cout << "accuracy " <<params()->accuracy << endl;

}*/



/*
void DepTreeCorpus::reLabel(DepTreeCorpus & query){
	//deprecated do not use this function
	ofstream salida;
	int correct=0;
	int total=0;
	string cad=  params()->stringTAG + ".txt";
	salida.open(cad.c_str());

	cout << "relabelling process"<< endl;
	salida << "targetLine\ttargetPredicate\told_label\tTargetNNodes\tsourceLine\tsourcePredicate\tnew_label\tSourceNNodes\tdistance\tsamples_at_same_distance\tNtreesCheked" << endl;

	////////
	for(int position = 0; position<(int)query.samples.size() ;position++)
	{
		int counter = 0;
		cout << ". complete: " << (position/((double)query.samples.size())*100)<< "%"<<   "   \trelabeling subtree n:" << position << " from line: " <<  query.samples[position]->line_number << endl;
		if (params()->K_for_KNN!=1)
			 KNN3relabel(query.samples[position],salida,correct, total,counter);
		else reLabel(query.samples[position],salida,correct, total); //30-09-2011
//		if(params()->cacheSize>0)
//			addToCache(query.samples[position],0);//ADDING TO CACHE
	}
	salida.close();
	params()->accuracy=((double)correct)/((double)total);
	cout << "accuracy " <<params()->accuracy << endl;
	//PARAMETERS.inform << "accuracy = " <<PARAMETERS.accuracy  << endl;
}
*/
/**
* 6-jan-2012
* this function shows the alignment between two taken treees;
*/
  void DepTreeCorpus::showMap(DepTreeCorpus & query, int source, int target){

//	ofstream salida;
	int correct=0;
	int total=0;
	string cad=  params()->stringTAG + ".txt";
//	salida.open(cad.c_str());

	
	
	DepSubTree * s = this->samples[source];
	DepSubTree * t = query.samples[target];
	
	cout <<" source: "<< source <<" target: " << target<< endl;

	TreeDistance * td = new TreeDistance(s,t);

	cout << "source " << endl;
	s->dot_show();
	s->show_info_nodes();


	cout << "target " << endl;
	t->dot_show();
	t->show_info_nodes();

	cout << "map"<< endl;
	Alignment * a;
	double distance = td->getAlignament(&a);
	cout << "distance " << distance << endl;
	a->dot_show();
	a->shell_show();


}


/**
* 
* relables data set using a file of pre procesed ditances.
* @param query sub-tree to re-label.
* @param file indicates where is the file of distances.
*/
void DepTreeCorpus::reLabelUsingTable(DepTreeCorpus & query, string file){
	ofstream salida;
	int correct=0;
	int total=0;
	string cad=  "results/"+ params()->stringTAG + ".txt";
	salida.open(cad.c_str());
	ifstream in;
	in.open(file.c_str());


	cout << "relabelling process"<< endl;
	salida << "targetLine\ttargetPredicate\told_label\tTargetNNodes\tsourceLine\tsourcePredicate\tnew_label\t"<<
		"SourceNNodes\tdistance\tsamples_at_same_distance\tNtreesCheked\tpanelSize\taristocrats\tSupportingVotes\t" <<
		"AmountEquivalenceClass\tIsTargetPredicatRooted\t" << 
		params()->codeVersion << endl;


	ifstream ficheroDone;
	string cad2=  params()->stringTagTrees + ".tre.done";
	ficheroDone.open(cad2.c_str());
	if (!ficheroDone)
	{
		cout <<"distance file doen't exist";
		exit(0);

	}
	ficheroDone.close();

	for(int position = 0; position<(int)query.samples.size() ;position++)
	{
		int counter = 0;
		in >> counter;
		cout << "* using table: complete: " << (position/((double)query.samples.size())*100)<< "%"<<   "   \trelabeling subtree n:" << position << " from line: " <<  query.samples[position]->line_number << endl;
		if (params()->allAtOnce)
			KNN4AllAtOnceRelabel(query.samples[position],salida,correct, total,in);
		else
			KNN4relabel(query.samples[position],salida,correct, total,in);
	}
	salida.close();
	params()->accuracy=((double)correct)/((double)total);
	cout << "accuracy " <<params()->accuracy << endl;
	//PARAMETERS.inform << "accuracy = " <<PARAMETERS.accuracy  << endl;
}

/**
* 17-oct-2011
* relables data set using a file of pre procesed ditances.
*
*/
/*
void DepTreeCorpus::reLabelUsingTableRel(DepTreeCorpus & query, string file){
	ofstream salida;
	int correct=0;
	int total=0;
	string cad=  params()->stringTAG + ".txt";
	salida.open(cad.c_str());
	ifstream in;
	in.open(file.c_str());


	cout << "relabelling process for relation trees"<< endl;
	salida << "targetLine\ttargetPredicate\told_label\tTargetNNodes\tsourceLine\tsourcePredicate\tnew_label\tSourceNNodes\tdistance\tsamples_at_same_distance\tNtreesCheked" << endl;

	////////
	for(int position = 0; position<(int)query.samples.size() ;position++)
	{
		int counter = 0;
		in >> counter;
		cout << "* using table: relation trees complete: " << (position/((double)query.relTrees.size())*100)<< "%"<<   "   \trelabeling subtree n:" << position
			<< " from line: " <<  query.relTrees[position]->line_number << endl;
		
		KNN4relabelRel(query.relTrees[position],salida,correct, total,in);
	}
	salida.close();
	params()->accuracy=((double)correct)/((double)total);
	cout << "accuracy " <<params()->accuracy << endl;
	//PARAMETERS.inform << "accuracy = " <<PARAMETERS.accuracy  << endl;
}
*/


/*
* 17-oct-2011
* saving relation trees distances.
*
*/
/*
void DepTreeCorpus::preProcesFileRel(DepTreeCorpus & query){

	ofstream salida;
	string cad=  params()->stringTagTrees + ".tre";
	salida.open(cad.c_str());

	cout << "saving distance relation-trees"<< endl;
	for(int position = 0; position<(int)query.relTrees.size() ;position++)
	{
		cout << "Saving relation-trees distances complete: " << (position/((double)query.relTrees.size())*100)<< "%"<< endl;
		salida << position;
		saveRelationDistances(query.relTrees[position],salida);
	}
	salida.close();
}
*/

/**
* 
* It saves the tree distances into a file for a query sample.
* @param query the query tree to be labelled. 
*
*/
void DepTreeCorpus::preProcesFile(DepTreeCorpus & query){
//	if (params()->multipleArgumentsTrees==false)
//	{
//		preProcesFileRel(query);
//		return;
//	}

	ofstream salida;
	string cad=  params()->stringTagTrees + ".tre";
	

	int counter = getLines(cad);//25-oct-2011

	salida.open(cad.c_str(), ios::app);
	cout << "saving distance trees"<< endl;
	for(int position = 0; position<(int)query.samples.size() ;position++)
	{
		if (counter ==0)
		{
			cout << " Saving distances complete: " << (position/((double)query.samples.size())*100)<< "%"<< endl;
			salida << position;
			saveDistances(query.samples[position],salida);
		}
		else
			counter--;
	}
	salida.close();
	cad = cad+".done";
	salida.open(cad.c_str());
	salida.close();
}

/**
* 
* saves a the distances for a particular tree (query)
* @param query query sub-tree to be labelled
* @param salida file in which the distances should be save. 
*
*/
void DepTreeCorpus::saveDistances(DepSubTree * query ,ofstream & salida){
	
	vector<TreeMeasure*> v;//create vector of alignments. 
	v.reserve(samples.size());
	TreeMeasure * c;
	for(unsigned int i = 0; i<samples.size();i++){	
		if (params()->SimilarityMachine==Zhang)
			c = new TreeDistance(query,samples[i]);
		else 
			c = new TreeKernel3(query,samples[i]);
		c->setVectorPossition(i);
		v.push_back(c);
	
	}

	std::sort(v.begin(), v.end(), icompare);
	cout << "maxSamplesSave = " << params()->maxSamples;
	for( int i = 0; i<params()->maxSamples && i<v.size();i++){	
		//cout << i << endl;

		TreeMeasure * c=v[i];
		salida << " " << c->getVectorPossition()<<":"<<c->getDistance();
	}
	salida << endl;
	
	for (unsigned int i = 0; i<v.size();i++)
		delete v[i];
	return;
}

/**
* 17-oct-2011
* saving a the distance of a particular tree (query)
*
*/
/*
void DepTreeCorpus::saveRelationDistances(DepRelTree * query ,ofstream & salida){
	
	vector<TreeMeasure*> v;//create vector of alignments. 
	v.reserve(samples.size());

	for(unsigned int i = 0; i<relTrees.size();i++){	
		query->show_info_nodes();
			TreeDistance * c = new TreeDistance(query,relTrees[i]);
			salida << " " << i<<":"<<c->getDistance();
			delete c;
	}
	salida << endl;
	return;
}
*/


/**
* Relabel a query sub-tree using itself as training.
@param query : It is the sub-tree that will be labelled
@param salida : It is the log file where the labelling is recorded.
@param correct: It is a counter of how many predictions were correct
@param total: It is a counter of how many predictions were made
*/
void DepTreeCorpus::reLabel(DepSubTree * query,ofstream &salida, int & correct, int & total){
	
	



	string targetPredicate, sourcePredicate,old_label,new_label;
	int targetLine,targetNNodes,sourceLine,sourceNNodes,samples_at_same_distance,NtreesCheked;
	double distance;
	


//	vector<TreeDistance*> v;
//	vector<TreeDistance*> lista;
	vector<TreeMeasure*> v;
	vector<TreeMeasure*> lista;
	
	lista.reserve(samples.size());
	v.resize(samples.size());
//	TreeDistance * td;
	for(unsigned int i = 0; i<samples.size();i++){

//		switch (params()->SimilarityMachine){
//			case Zhang:
				v[i] = new TreeDistance(query,samples[i]);
			//	v[i]->getDistance();
//				break;
//			case TreeKernel:
//				v[i] = new TreeKernel(query,samples[i]);
//				break;
//		};
	}
	 make_heap(v.begin(),v.end(),*compare);	 
	 //make_heap(v.begin(),v.end(),*compare2);
	 
	 for(int arg = 0; arg<(int)query->POApred.size();arg++){
	 total++;
	targetPredicate=old_label=new_label="?";
	targetLine=targetNNodes=sourceLine=sourceNNodes=samples_at_same_distance=NtreesCheked=-1;
	distance=-1;

	 	 double distanceCHECK = -1;
		 int samedistance=1;
		 targetLine = query->line_number;
		 targetPredicate = ((depTreeNodeInfo*)query->PO_nodes[query->predicateNode]->info)->getFormString();
		 int NumberTreeCheck=0;
		 SemanticRelation * squery;
		 squery = query->POApred[arg].second;
		 old_label= squery->getString();
		 targetNNodes= query->size()+1;
	 	 bool ArgDone = false;
		 if (lista.size()==0){
			 lista.push_back(v.front());
			 pop_heap(v.begin(),v.end(),*compare);
			 //pop_heap(v.begin(),v.end(),*compare2);
			 v.pop_back();
		 }
		 int i = 0;
		 for(i = 0; !ArgDone && i< (int)lista.size();i++){
			 if(i!=0){
				 if(lista[i]->getDistance()==lista[i-1]->getDistance()) samedistance++;
				 else {
					 
					 if (distanceCHECK!=-1){
						 ArgDone = true;
				 	}else samedistance=1;
					
				 }
			 }
			 Alignment *p= NULL;
			 DepSubTree * sam;
			 lista[i]->getAlignament(&p);
			 sam = (DepSubTree*)(lista[i]->getCoDomainTree());

			 if(distanceCHECK==-1 && p->match(query->predicateNode,sam->predicateNode)){
				 for(int samArgPosition = 0;distanceCHECK==-1 && samArgPosition<(int) sam->POApred.size();samArgPosition++)
					 if ( p->match(query->POApred[arg].first,sam->POApred[samArgPosition].first))
					 {
						 //ENCONTRADO;
						
						 ArgDone = true; //mod
						 sourceLine= sam->line_number; 
						 SemanticRelation * ssample;

						 sourcePredicate = ((depTreeNodeInfo*)sam->PO_nodes[sam->predicateNode]->info)->getFormString();
					 
						 ssample = sam->POApred[samArgPosition].second;
						 //cout << "old relation: "<<squery->getString()<< endl;;

						 if(ssample->getRelation()==squery->getRelation())
							 correct++;
						 squery->setPrediction(ssample);

						// cout << "new relation: "<<ssample->getString()<< endl;;
						 new_label= ssample->getString();
						
						 // salida << ((depTreeNodeInfo*)query->PO_nodes[query->predicateNode]->info)->getFormString() << '\t';
						 sourceNNodes = sam->size()+1;
						 distance = lista[i]->getDistance();
						  NumberTreeCheck = i+1;
						  distanceCHECK= lista[i]->getDistance();
						  
						  //samples_at_same_distance\tNtreesCheked" << endl;
					 }
			 }
			 else {
				 // no sirve
			 }

			 //si hace falta se anyade uno mas.
			 if (!ArgDone && i==lista.size()-1 && v.size()>0)
			 {
			 	lista.push_back(v.front());
			 	pop_heap(v.begin(),v.end(),*compare);
			 	//pop_heap(v.begin(),v.end(),*compare2);
			 	
				v.pop_back();
			}
		
			 if(ArgDone)  {
			 	samples_at_same_distance = samedistance;
				NtreesCheked = NumberTreeCheck;

			 }
		 }

		 if (v.size()==0 && i == lista.size()){
				NtreesCheked = lista.size();


			 cout << "no hay ejemplos validos";
				
		 }
		 
				salida << targetLine << '\t'
					<< targetPredicate<< '\t'
					<< old_label << '\t'
					<< targetNNodes << '\t'
					<< sourceLine<< '\t'
					<< sourcePredicate<< '\t'
					<< new_label << '\t'
					<< sourceNNodes << '\t'
					<< distance << '\t'
					<< samples_at_same_distance << '\t'
					<< NtreesCheked  << endl;





	 }
	//vector<TreeDistance*> v;
	//vector<TreeDistance*> lista;

	 for (unsigned int i = 0; i<v.size();i++)
		 delete v[i];
	 for (unsigned int i = 0; i<lista.size();i++)
		 delete lista[i];
}
/*
void DepTreeCorpus::KNN3relabel(DepSubTree * query,ofstream &salida, int & correct, int & total, int & counter){
	/**
	30-09-2011 
	/

	string targetPredicate, sourcePredicate,old_label,new_label;
	int targetLine,targetNNodes,sourceLine,sourceNNodes,samples_at_same_distance,NtreesCheked;
	double distance = 0.0;

	targetLine = targetNNodes = sourceLine = sourceNNodes = samples_at_same_distance =NtreesCheked = -11;
	//    *			*				?				?					?				    *			
	targetPredicate =  sourcePredicate = old_label = new_label = "??";
	//    *				     ?              *             *   
	targetLine = query->line_number;
	targetNNodes= query->size()+1;

	vector<TreeMeasure*> v;//create vector of alignments. 
	v.reserve(samples.size());
	

		//v[i] = new TreeDistance(query,samples[i]);
	
	if (counter > (int)query->POApred.size()){
		counter -= (int)query->POApred.size();
		cout <<"*"<< counter<<"*";
		return;
	}
	
	for(unsigned int i = 0; i<samples.size();i++)	
//		if(params()->distanceType=="TK"){
//			TreeKernel* tkp;//= new TreeKernel( query, samples[i]);			
//			v.push_back( tkp);
			//v.push_back(new TreeKernel( query, samples[i]));
			//TreeKernel(AbsTree *domine, AbsTree *codomine);
//		}
//		else 
			v.push_back(new TreeDistance(query,samples[i]));
			//	TreeDistance(AbsTree *domine, AbsTree *codomine);

	std::sort(v.begin(), v.end(), icompare);
	 for(int arg = 0; arg<(int)query->POApred.size();arg++){//FOR EACH ARGUMENT
	

		 if (counter>0){
			 counter--;
			 continue;
		 }

		total++;
		targetPredicate=old_label=new_label="?";
		KNNpredictor preditor(params()->K_for_KNN);

		targetPredicate = ((depTreeNodeInfo*)query->PO_nodes[query->predicateNode]->info)->getFormString();
		 int i = 0;
//		 for(i = 0; !preditor.isready() && i< (int)v.size();i++){ 5-oct-2011
 		 for(i = 0;  i< (int)v.size();i++){
			 Alignment *p= NULL;
			 DepSubTree * sam;
			 v[i]->getAlignament(&p);
			 sam = (DepSubTree*)(v[i]->getCoDomainTree());
			  
			  SemanticRelation * ssample;
			 
			  bool storePredition = false;
			 if(p->match(query->predicateNode,sam->predicateNode))
				for(int samArgPosition = 0; samArgPosition<(int) sam->POApred.size();samArgPosition++){
					
					 //sourcePredicate = ((depTreeNodeInfo*)sam->PO_nodes[sam->predicateNode]->info)->getFormString();
					 ssample = sam->POApred[samArgPosition].second;

					if (p->match(query->POApred[arg].first,sam->POApred[samArgPosition].first)){//they match
						preditor.addSample(ssample->getRelation(), v[i]->getDistance());
						storePredition=true;
					}
				}
				if (storePredition == false)//5-oct-2011
					preditor.addSample(-1,v[i]->getDistance());
		}
		
//		if (preditor.isready()){
		 {
			 int newLabelint = preditor.getLabel();
			 if (newLabelint == -1)
				 new_label = "?";
			 else
				 new_label = params()->getString(preditor.getLabel());
			distance = 0;//v[i]->getDistance();

			//if(preditor.getLabel()==preditor.getLabel())
			//correct++;


		}
		
		SemanticRelation * squery;
		squery = query->POApred[arg].second;
		old_label= squery->getString();
		
		NtreesCheked = i+1;
		salida << targetLine << '\t'
					<< targetPredicate<< '\t'
					<< old_label << '\t'
					<< targetNNodes << '\t'
					<< "??\t" //<< sourceLine<< '\t'
					<< "??\t" //<< sourcePredicate<< '\t'
					<< new_label << '\t'
					<< "??\t" //<< sourceNNodes << '\t'
					<< distance << '\t'
					<< "??\t" //<< samples_at_same_distance << '\t'
					<< NtreesCheked  << endl;

	}//end arg.
}
*/


/*
void DepTreeCorpus::KNN2reLabel(DepSubTree * query,ofstream &salida, int & correct, int & total){
	/**
	30-09-2011 
	/

	string targetPredicate, sourcePredicate,old_label,new_label;
	int targetLine,targetNNodes,sourceLine,sourceNNodes,samples_at_same_distance,NtreesCheked;
	double distance = 0.0;

	targetLine = targetNNodes = sourceLine = sourceNNodes = samples_at_same_distance =NtreesCheked = -11;
	targetPredicate =  sourcePredicate = old_label = new_label = "??";
	
	vector<TreeMeasure*> v;//create vector of alignments. 
	v.reserve(samples.size());
	for(unsigned int i = 0; i<samples.size();i++)	v.push_back(new TreeDistance(query,samples[i]));
		//v[i] = new TreeDistance(query,samples[i]);
	
	std::sort(v.begin(), v.end(), icompare);//TODO  CHECK IF IT WORKS !!!!!!!!!!!!!!!

	 for(int arg = 0; arg<(int)query->POApred.size();arg++){//FOR EACH ARGUMENT
	
		total++;
		targetPredicate=old_label=new_label="?";
		KNNpredictor preditor(params()->K_for_KNN);


		 int i = 0;
		 for(i = 0; !preditor.isready() && i< (int)v.size();i++){
			 Alignment *p= NULL;
			 DepSubTree * sam;
			 v[i]->getAlignament(&p);
			 sam = (DepSubTree*)(v[i]->getCoDomainTree());
			  SemanticRelation * ssample;
			 
			 if(p->match(query->predicateNode,sam->predicateNode))
				for(int samArgPosition = 0; samArgPosition<(int) sam->POApred.size();samArgPosition++){
					
					 //sourcePredicate = ((depTreeNodeInfo*)sam->PO_nodes[sam->predicateNode]->info)->getFormString();
					 ssample = sam->POApred[samArgPosition].second;

					if (p->match(query->POApred[arg].first,sam->POApred[samArgPosition].first))//they match
						preditor.addSample(ssample->getRelation(), v[i]->getDistance());
				}
		}
		
		if (preditor.isready()){
			new_label = params()->getString(preditor.getLabel());
						
			if(preditor.getLabel()==preditor.getLabel())
			correct++;
		}
		
		SemanticRelation * squery;
		squery = query->POApred[arg].second;
		old_label= squery->getString();
		

		salida << targetLine << '\t'
					<< targetPredicate<< '\t'
					<< old_label << '\t'
					<< targetNNodes << '\t'
					<< sourceLine<< '\t'
					<< sourcePredicate<< '\t'
					<< new_label << '\t'
					<< sourceNNodes << '\t'
					<< distance << '\t'
					<< samples_at_same_distance << '\t'
					<< NtreesCheked  << endl;

	}//end arg.
}
	*/



/*
void DepTreeCorpus::KNNreLabel(DepSubTree * query,ofstream &salida, int & correct, int & total){
	// query : It is the sub-tree that will be labelled
	// salida : It is the log file where the labelling is recorded.
	// correct: It is a counter of how many predictions were correct
	// total: It is a counter of how many predictions were made
	
	string targetPredicate, sourcePredicate,old_label,new_label;
	int targetLine,targetNNodes,sourceLine,sourceNNodes,samples_at_same_distance,NtreesCheked;
	double distance;
	


//	vector<TreeDistance*> v;
//	vector<TreeDistance*> lista;
	vector<TreeMeasure*> v;
	vector<TreeMeasure*> lista;
	
	lista.reserve(samples.size());
	v.resize(samples.size());
//	TreeDistance * td;
	for(unsigned int i = 0; i<samples.size();i++){
		

//		switch (params()->SimilarityMachine){
//			case Zhang:
				v[i] = new TreeDistance(query,samples[i]);
			//	v[i]->getDistance();
//				break;
//			case TreeKernel:
//				v[i] = new TreeKernel(query,samples[i]);
//				break;
//		};
	}
	 make_heap(v.begin(),v.end(),*compare);	 
	 //make_heap(v.begin(),v.end(),*compare2);
	 
	 for(int arg = 0; arg<(int)query->POApred.size();arg++){
	 total++;
	targetPredicate=old_label=new_label="?";
	KNNpredictor preditor(params()->K_for_KNN);

	targetLine=targetNNodes=sourceLine=sourceNNodes=samples_at_same_distance=NtreesCheked=-1;
	distance=-1;

	 	 double distanceCHECK = -1;
		 int samedistance=1;
		 targetLine = query->line_number;
		 targetPredicate = ((depTreeNodeInfo*)query->PO_nodes[query->predicateNode]->info)->getFormString();
		 int NumberTreeCheck=0;
		 SemanticRelation * squery;
		 squery = query->POApred[arg].second;
		 old_label= squery->getString();
		 targetNNodes= query->size()+1;
	 	 bool ArgDone = false;
		 if (lista.size()==0){
			 lista.push_back(v.front());
			 pop_heap(v.begin(),v.end(),*compare);
			 //pop_heap(v.begin(),v.end(),*compare2);
			 v.pop_back();
		 }
		 int i = 0;
		 for(i = 0; !ArgDone && i< (int)lista.size();i++){
			 if(i!=0){
				 if(lista[i]->getDistance()==lista[i-1]->getDistance()) samedistance++;
				 else {
					 
					 if (distanceCHECK!=-1){
						 ArgDone = true;
				 	}else samedistance=1;
					
				 }
			 }
			 Alignment *p= NULL;
			 DepSubTree * sam;
			 lista[i]->getAlignament(&p);
			 sam = (DepSubTree*)(lista[i]->getCoDomainTree());

			 if(distanceCHECK==-1 && p->match(query->predicateNode,sam->predicateNode)){
				 for(int samArgPosition = 0;distanceCHECK==-1 && samArgPosition<(int) sam->POApred.size();samArgPosition++)
					 if ( p->match(query->POApred[arg].first,sam->POApred[samArgPosition].first))
					 {
						 //ENCONTRADO;// quizas no suficionete. 
						
						// ArgDone = true; //mod
						 sourceLine= sam->line_number; 
						 SemanticRelation * ssample;

						 sourcePredicate = ((depTreeNodeInfo*)sam->PO_nodes[sam->predicateNode]->info)->getFormString();
					 
						 ssample = sam->POApred[samArgPosition].second;
						 //cout << "old relation: "<<squery->getString()<< endl;;

						

						// cout << "new relation: "<<ssample->getString()<< endl;;
						 //new_label= ssample->getString();
						 preditor.addSample(ssample->getRelation(), lista[i]->getDistance());
						 
						 // salida << ((depTreeNodeInfo*)query->PO_nodes[query->predicateNode]->info)->getFormString() << '\t';
						 sourceNNodes = sam->size()+1;
						 distance = lista[i]->getDistance();
						  NumberTreeCheck = i+1;
						  distanceCHECK= lista[i]->getDistance();
						  
						  ArgDone = preditor.isready();//30-sep-2011
						  if (ArgDone){
							  new_label = params()->getString(preditor.getLabel());
							
							  if(preditor.getLabel()==squery->getRelation())
								correct++;
							squery->setPrediction(ssample);
								
						  }
						  
						  //samples_at_same_distance\tNtreesCheked" << endl;
					 }
			 }
			 else {
				 // no sirve
			 }

			 //si hace falta se anyade uno mas.
			 if (!ArgDone && i==lista.size()-1 && v.size()>0)
			 {
			 	lista.push_back(v.front());
			 	pop_heap(v.begin(),v.end(),*compare);
			 	//pop_heap(v.begin(),v.end(),*compare2);
			 	
				v.pop_back();
			}
		
			 if(ArgDone)  {
			 	samples_at_same_distance = samedistance;
				NtreesCheked = NumberTreeCheck;

			 }
		 }

		 if (v.size()==0 && i == lista.size()){
				NtreesCheked = lista.size();


			 cout << "no hay ejemplos validos";
				
		 }
		 
				salida << targetLine << '\t'
					<< targetPredicate<< '\t'
					<< old_label << '\t'
					<< targetNNodes << '\t'
					<< sourceLine<< '\t'
					<< sourcePredicate<< '\t'
					<< new_label << '\t'
					<< sourceNNodes << '\t'
					<< distance << '\t'
					<< samples_at_same_distance << '\t'
					<< NtreesCheked  << endl;

	 }
	//vector<TreeDistance*> v;
	//vector<TreeDistance*> lista;

	 for (unsigned int i = 0; i<v.size();i++)
		 delete v[i];
	 for (unsigned int i = 0; i<lista.size();i++)
		 delete lista[i];
}

*/
//DepTreeCorpus::~DepTreeCorpus(void)
//{
//}

/**
* Compares if teh alignments using Distance Ternary and Similarity Ternary are equivalent. 
*/
void DepTreeCorpus::TestEqualAlignments(DepTreeCorpus & q){
	int equalCounter = 0;
	int totalCounter = 0;
	
	int smallS = 0;
	int smallT = 0;
	int smallTotal = 1000;

	for (int sourceTree = 0; sourceTree <(int)this->samples.size(); sourceTree++){
	//for (int targetTree = 0; targetTree <q.samples.size(); targetTree++){

//		if (samples[sourceTree]->getNumberOfNodes()<3)
//			continue;
	for (int targetTree = 0; targetTree <(int)q.samples.size(); targetTree++){

		
//		if (q.samples[targetTree]->getNumberOfNodes()<3)
//			continue;

		params()->AtomicMeasure = TERNARY;
		TreeDistance * p = new TreeDistance(samples[sourceTree],q.samples[targetTree]);
		Alignment * a1;
		Alignment * a2;
		p->getAlignament(&a1);
		params()->AtomicMeasure = STERNARY;
		TreeDistance * p2 = new TreeDistance(samples[sourceTree],q.samples[targetTree]);
		p2->getAlignament(&a2);

		bool equal = true;
		double aux1;
		double aux2;
				params()->AtomicMeasure = TERNARY;
				aux1= a1->recalculateDistance();
				aux2 = a2->recalculateDistance();
		if (aux1>aux2+0.1 && aux2>aux1+01) 
			equal = false;
				params()->AtomicMeasure = STERNARY;
				aux1= a1->recalculateDistance();
				aux2 = a2->recalculateDistance();
		if (aux1>aux2+0.1 && aux2>aux1+01) 
			equal = false;
	


		if (equal)//a1->isEqual(a2)) 
			equalCounter++;
		else{
/*			samples[sourceTree]->show_info_nodes();
			samples[sourceTree]->dot_show();
				cout<< endl;
			q.samples[targetTree]->show_info_nodes();
			q.samples[targetTree]->dot_show();
			cout<< endl;
		a1->shell_show();
		
		//	a1->dot_show();
		cout<< endl;
			a2->shell_show();
	
		//	a2->dot_show();
		*/
			if ( (int) samples[sourceTree]->size() + (int) q.samples[targetTree]->size()  <smallTotal){
				smallS = sourceTree;
				smallT = targetTree;
			}
		}
		totalCounter++;

		delete p;
		delete p2;

	}
	cout << 100*(double)sourceTree/(double)samples.size() << " % done " << endl;
	}

	cout << "equal Alignments: " << equalCounter << endl;
	cout << "total Alignments: " << totalCounter << endl;
	cout << "equal percentage: " << ((double)equalCounter*100)/((double)totalCounter)<< " % "<< endl;

	do{

		samples[smallS]->dot_show();
		q.samples[smallT]->dot_show();

		params()->AtomicMeasure = TERNARY;
			TreeDistance * p = new TreeDistance(samples[smallS],q.samples[smallT]);
		Alignment * a1;
		Alignment * a2;
		p->getAlignament(&a1);
		params()->AtomicMeasure = STERNARY;
		TreeDistance * p2 = new TreeDistance(samples[smallS],q.samples[smallT]);
		p2->getAlignament(&a2);
		
		samples[smallS]->show_info_nodes();
		cout << endl;
		q.samples[smallT]->show_info_nodes();
			a1->shell_show();
			cout << "distance " << p->getDistance() << endl;
			a1->dot_show();
			cout << endl;
			a2->shell_show();
		cout << "similarity " << p2->getDistance() << endl;
			a2->dot_show();
			cout << "distances: " << endl;
				params()->AtomicMeasure = TERNARY;
				cout << a1->recalculateDistance() << endl;
				cout << a2->recalculateDistance() << endl;
			cout << "similaritys: " << endl;
				params()->AtomicMeasure = STERNARY;
				cout << a1->recalculateDistance() << endl;
				cout << a2->recalculateDistance() << endl;
				
	}while(0);

	return;

}




//DepTreeCorpus::DepTreeCorpus(const char * file_name, int format){

DepTreeCorpus2::DepTreeCorpus2(const char * file_name):DepTreeCorpus(file_name){
}

/**
* Evaluates the labeling of itself. 
*/
void DepTreeCorpus2::evaluate(){
	int  TPPosition, FPPosition,FNPosition, TPLabel, FPLabel, FNLabel;
	TPPosition= FPPosition=FNPosition= TPLabel= FPLabel= FNLabel=0;
	double precision, recall, f1;

	if (params()->stringTAG[0]!='R')
		params()->stringTAG= "R"+params()->stringTAG;
	for (int i = 0; i<(int)this->DTcorpora.size();i++)
		((DepTree2*)DTcorpora[i])->evaluate(TPPosition, FPPosition,FNPosition, TPLabel, FPLabel, FNLabel);
	ofstream salida;
	string cad=  params()->stringTAG + ".txt";
	salida.open(cad.c_str());
	salida <<  "TP \t FP \t FN \t Precision \t Recall \t F1" << endl;
	precision = TPPosition/((double) TPPosition+FPPosition);
	recall = TPPosition/((double) TPPosition+FNPosition);
	f1 = 2 * (precision*recall)/(precision+recall);
	salida <<  TPPosition << "\t" << FPPosition << "\t"<< FNPosition << "\t" << precision << "\t" << recall << "\t" << f1 << endl;
	params()->accuracy= f1;
	
	precision = TPLabel/((double) TPLabel+FPLabel);
	recall = TPLabel/((double) TPLabel+FNLabel);
	f1 = 2 * (precision*recall)/(precision+recall);
	
	salida	<< TPLabel    << " " << FPLabel    << " "<< FNLabel    <<  "\t" << precision << "\t" << recall << "\t" << f1 << endl;
	salida.close();
}
	
/*
void putBestInZero( vector<TreeMeasure*> * lista){

	int lower = 0;
	for (int i = 1;i<(int)lista->size();i++){
		if ((*lista)[lower]->getDistance()>(*lista)[i]->getDistance())
			lower = i;
	}

	TreeMeasure * p = (*lista)[0];
	(*lista)[0]=(*lista)[lower];
	(*lista)[lower]=p;
	return;
}*/

/**
* Identifies and labels itself.
*@param training it is the training corpus. 
*/
void DepTreeCorpus2::identifyAndLabelMeWhith(DepTreeCorpus & training){
	int correct=0;
	int total=0;
	DepTree2 * currentSentence;


	for(int sentence = 0; sentence<(int)DTcorpora.size() ;sentence++)// for each sentence. //***********
	{
		cout << "complete: " << (sentence/((double)DTcorpora.size())*100)<< "%"<<   "   \trelabeling and Identifing subtree n:" << sentence << " from line: " <<  DTcorpora[sentence]->line_number << endl;
		currentSentence = (DepTree2*) DTcorpora[sentence]; 

			vector<vector<pair<int, SemanticRelation *> > > *  auxi;
			auxi = 	&currentSentence->NEW_POapreds;
			auxi ->resize(currentSentence->POapreds.size());


		for (int predicate = 0; predicate< (int) currentSentence->POpred.size();predicate++){ //for each predicate on the sentence we need to find its arguments.

			int CnodeNumber = currentSentence->POpred[predicate];
			((DepTreeNode*)currentSentence->PO_nodes[CnodeNumber])->pegamento=true;
			
			///measure all distances
			vector<TreeMeasure*> lista;
			for (int subtree = 0 ; subtree<(int)training.samples.size(); subtree++){
				((DepTreeNode*)training.samples[subtree]->PO_nodes[training.samples[subtree]->predicateNode])->pegamento=true;
				TreeDistance *p = new TreeDistance(currentSentence,training.samples[subtree]);
				p->getDistance();
				lista.push_back(p);
			}
					int lower = 0;
					for (int i = 1;i<(int)lista.size();i++){
						double actual, nuevo;
						actual = lista[lower]->getDistance();
						nuevo = lista[i]->getDistance(); 
						if (actual>nuevo){
							lower = i;
						}
					}
					TreeMeasure * pun = lista[0];
					lista[0]=lista[lower];
					lista[lower]=pun;
	
						Alignment *q;

						int lineSentence = ((DepTreeNode*)currentSentence->PO_nodes[CnodeNumber])->info->fileSystemLine;
						int position = ((DepSubTree*)lista[0]->getCoDomainTree())->predicateNode;
						int lineSample   = ((DepSubTree*)lista[0]->getCoDomainTree())->PO_nodes[position]->info->fileSystemLine;
					
						if (lineSentence == lineSample)
							cout << "ok" << endl;
						else {
							cout << "not ok" << lineSentence << " <> " << lineSample <<  endl;

							for (int i = 1; i< (int)lista.size();i++){
								int lineSample2   = ((DepSubTree*)lista[i]->getCoDomainTree())->PO_nodes[position]->info->fileSystemLine;
								if (lineSentence == lineSample2)
								{
									cout << "distance 0 : " << lista[0]->getDistance();
									cout << "distance " << i << " : " << lista[i]->getDistance();
									((DepTree*)lista[0]->getDomainTree())->show_info_nodes();
									lista[0]->getAlignament(&q); q->dot_show();
									((DepSubTree*)lista[0]->getCoDomainTree())->show_info_nodes();
									lista[i]->getAlignament(&q); q->dot_show();
									((DepSubTree*)lista[i]->getCoDomainTree())->show_info_nodes();

								}
							}

						}


						

			Alignment *p= NULL;
			lista[0]->getAlignament(&p);
			DepSubTree * subt = NULL;
			subt = (DepSubTree *) lista[0]->getCoDomainTree();
			for (int argNum = 0; argNum<(int)subt->POApred.size(); argNum++){
				int position; 
				position = subt->POApred[argNum].first;
				int sentencePosition;
				sentencePosition = p->inverseF(position);
				pair<int, SemanticRelation *> s;
				s = subt->POApred[argNum];
				s.first = sentencePosition;
				if (s.first>0){
					currentSentence->NEW_POapreds[predicate].push_back(s);
				}

			}



			((DepTreeNode*)currentSentence->PO_nodes[CnodeNumber])->pegamento=false;//des preparar. 




			//cleaning memory
			for(unsigned int i = 0; i<lista.size();i++){
				delete lista[i];
			}

		}

	}
	params()->accuracy=((double)correct)/((double)total);
}




 

/**
* Calculate the closest value to the choosen k (k-NN) which fits an equivalence class. 
* @return returns the nearest_cutoff for k = minK
*/
int KNNpredictor::nearest_cutoff(){
	if ((int)samples.size()<=minK) return -1;

	int min,max;

	for (min = minK; min>=1;min--){
		double diff = distances[min]-distances[min-1];
		if (diff<0) diff = diff*-1;
		if (diff>MINDIFF){
			break;
		}
	}
	for (max = minK+1; max<(int)distances.size();max++)
	{
		double diff = distances[max-1]-distances[max];
		if(diff<0) diff= diff*-1;
		if(diff>MINDIFF)
			break;
	}

	if (min == 0)//15/06/2012
		return max;

	if(minK-min > max-minK)
		return max;
	else
		return min;
}


/**
* Construct a object to hold a k-NN panel
* @param k is the desiderable panel size. 
*/
KNNpredictor::KNNpredictor(int k){
	minK = k;
	ready = false;
	prediction=-2;//no prediciton;
	lastCutOff = -1;
	aristocrats = -1;
	supportingVotes = -1;
}

/** @retrun the value of the lastCutOff */
int KNNpredictor::getLastCutOff(){return lastCutOff;}
/** @retrun the amount of valid votes */
int KNNpredictor::getAristocrats(){return aristocrats;}
/** @retrun the amounts of valid votes in favour of the final decision.  */
int KNNpredictor::getSupportingVotes(){return supportingVotes;}

/** @retrun how many equivalence classes were used on the prediction */
int KNNpredictor::getUsedEquivalenceClass(){
	int counter = 0;
	for (int i = 0; i<lastCutOff;i++)
	{
		double diff = distances[i+1]-distances[i];
		if (diff<0) diff=diff*-1;
		if (diff> MINDIFF)
		{
			counter++;
		}

	}
	return counter;
}

/** makes a prediction. 
* @retrun the prediction*/
int KNNpredictor::predict(bool allowNull){
	int k = nearest_cutoff();

	bool venedict = false;
	vector<int> candidates;
	vector<int> counters;

	int maxposition = 0;
	int sampleNumber=0;
	while (!venedict){
		while (sampleNumber<(int)samples.size())
		{
//			bool encontrado = false;
			int label = samples[sampleNumber];

			int k;
			for (k = 0 ; k<(int)candidates.size();k++)
				if (samples[k]==label)
					break;
			if (k==candidates.size())//no encontrado
			{
				candidates.push_back(label);
				counters.push_back(1);
			}
			else{
				if (candidates[label]==-1 && !allowNull){}
				else
					counters[k]++;
			}
			sampleNumber++;

			if (sampleNumber >= k && distances[sampleNumber-1]!=distances[sampleNumber])
				break;
		}

		bool draw = false; 
		for (int i = 1; i<(int)candidates.size();i++){
			
			if (counters[maxposition]<counters[i]){
				maxposition = i;
				draw = false;
			}
			if (counters[maxposition]==counters[i])
				draw = true;
		}
		if (!draw)
			venedict = true; 
	
	}
	return candidates[maxposition];

}






/** @retrun the rediction. */
int KNNpredictor::getLabel(){
	return prediction;
}

/** @retrun true if the prediction is ready. */
bool KNNpredictor::isready(){
	return ready;}

/** turn the parameter ready to false. */
void KNNpredictor::turnUnReady(){
	ready = false;
}

/** turns the parameter ready to true*/
void KNNpredictor::turnReady(){
	ready = true;
}

/** Adds a sample to the k-NN panel
* @param label the new vote to be added.
* @param distance the distance to the query.
*/
void KNNpredictor::addSample(int label, double distance){
	samples.push_back(label);
	distances.push_back(distance);
	if ((int)samples.size()>= minK){
		if (samples.size()>=2){
			double diff = distances.back()-distances[distances.size()-2];
			if(diff<0) diff = diff*-1;
			if(diff> MINDIFF){
				ready = calculate();
			}
		}
	}
}

/**
* @return a new panel size which fit the next equivalence class.
* @param cut previous panel size.
*/
int KNNpredictor::extend(int cut){
	cut++;

	while (cut<(int)samples.size()-1){
		double diff= distances[cut]-distances[cut+1];
		if(diff <0) diff = diff*-1;
		if (diff>MINDIFF){
			cut++;
			return cut;
		}
		cut++;
	}
	return -1;// need to extend;
}


/**
* @return prediction using a fix panel size.
* @param cut especifies a panel size.
*/
int KNNpredictor::getLabelUsing(int cut){
	vector<int> candidates;
	vector<int> counters;
	aristocrats = 0;

	if (cut == 0 ) return -2;
	for (int l = 0; l<cut; l++){
		bool encontrado = false;
		int label = samples[l];

		int k;
		for (k = 0 ; k<(int)candidates.size();k++)
			if (candidates[k]==label)
				break;
		
		if (k==candidates.size())//no encontrado
		{
			candidates.push_back(label);
			counters.push_back(1);
			if (candidates[k]==-1 && !params()->allowNullPrediciton)
				counters[k]--;
			

		}
		else{
			counters[k]++;
			if (candidates[k]==-1 && !params()->allowNullPrediciton)
				counters[k]--;
		}		
	}

	int maxposition = 0;

	bool draw = false;
	for (int i = 1; i<(int)candidates.size();i++){
		if (counters[maxposition]==counters[i]){
			draw = true;
		}
		if (counters[maxposition]<counters[i]){
			maxposition = i;
			draw = false;
		}
	}
	//count aristocrats;
	for (int i = 0; i<(int)candidates.size();i++)
		if (candidates[i]!=-1)
			aristocrats += counters[i];
	
	supportingVotes = counters[maxposition];

	if (candidates[maxposition]==-1 && !params()->allowNullPrediciton)
		return -2;

	if (draw)
		return -2;
	else
		return candidates[maxposition];

}


/**
* calculates the k-NN module can make a prediction.
* @return true if the system is ready to make a prediction.
*/
bool KNNpredictor::calculate(){ //15/06/2012
	int cut=-1;
	//cout << lastCutOff << endl;
	if (lastCutOff == -1)
		cut = nearest_cutoff();
	else
		cut = extend(lastCutOff);
	//cout << lastCutOff << endl;
	lastCutOff = cut;
	//cout << lastCutOff << endl;
	if (cut<=0) return false;


	int label = -2;

	label = getLabelUsing(cut);


	while(label == -2)//no label
	{//extend;
		cut = extend(cut);
		if (cut == -1)
			return false;
		lastCutOff = cut;
		label = getLabelUsing(cut);
	}
	prediction = label;
	return true;
}



/**
* 
* using the distances of a single tree .
*
*/
void DepTreeCorpus::KNN4relabel(DepSubTree * query,ofstream &salida, int & correct, int & total,  ifstream & tableDistances){
		string targetPredicate, sourcePredicate,old_label,new_label;
	int targetLine,targetNNodes,sourceLine,sourceNNodes,samples_at_same_distance,NtreesCheked;
	double distance = 0.0;

	targetLine = targetNNodes = sourceLine = sourceNNodes = samples_at_same_distance =NtreesCheked = -11;
	//    *			*				?				?					?				    *			
	targetPredicate =  sourcePredicate = old_label = new_label = "??";
	//    *				     ?              *             *   
	targetLine = query->line_number;
	targetNNodes= query->size()+1;

	vector<TreeMeasure*> v;//create vector of Alignments. 
	v.reserve(samples.size());
	
	for(unsigned int i = 0; i<samples.size()&&i<params()->maxSamples;i++)	{


			string cad;
			string head;
			string tail;
			double distance=0;
			tableDistances >> cad;
			int position = cad.find(":");
			head = cad.substr(0,position);

			tail = cad.substr(position+1);//remove the head

			

			distance = atof(tail.c_str());
		//	cout << distance;
			int samplePos = atof(head.c_str());

			TreeMeasure * pp;
			if (params()->SimilarityMachine==Zhang)
				pp =new TreeDistance(query,samples[samplePos]);
			else 
				pp =new TreeKernel3(query,samples[samplePos]);
			if (params()->N1== false)
				pp->setDistance(distance);
			else 
				pp->setDistance(i);
			v.push_back(pp);
	}
	
//	std::sort(v.begin(), v.end(), icompare);
	
	for(int arg = 0; arg<(int)query->POApred.size();arg++){//FOR EACH ARGUMENT
	


		total++;

	
		targetPredicate=old_label=new_label="?";
		KNNpredictor preditor(params()->K_for_KNN);
		
		targetPredicate = ((depTreeNodeInfo*)query->PO_nodes[query->predicateNode]->info)->getFormString();
		 int i = 0;
 		 for(i = 0;  i< (int)v.size() && !preditor.isready();i++){

		
			 Alignment *p= NULL;
			 DepSubTree * sam;
			 v[i]->getAlignament(&p);
			 sam = (DepSubTree*)(v[i]->getCoDomainTree());
			  
			  SemanticRelation * ssample;
			 
			  bool storePredition = false;
			 if(p->match(query->predicateNode,sam->predicateNode))
				for(int samArgPosition = 0; samArgPosition<(int) sam->POApred.size();samArgPosition++){
					 ssample = sam->POApred[samArgPosition].second;
					if (p->match(query->POApred[arg].first,sam->POApred[samArgPosition].first)){//they match
						int relation = ssample->getRelation();
						double dis = v[i]->getDistance();
						
						if (params()->N1==false)
							preditor.addSample(relation,dis);
						else //N1== true
							preditor.addSample(relation,i);

						storePredition=true;
						break;
					}
				}
				if (storePredition == false)//5-oct-2011
					if (params()->N1==false)
						preditor.addSample(-1,v[i]->getDistance());
					else //N1== true
						preditor.addSample(-1,i);
		}
		 if(!preditor.isready())
			 preditor.addSample(-1,10000000);//ading null prediction at distance infinite 
		
		 new_label="?NoPrediction";
		if (preditor.isready())
		 {
			 int newLabelint = preditor.getLabel();
			 if (newLabelint == -1)
				 new_label = "?NoLabel";
			 else
				 new_label = params()->getString(preditor.getLabel());
			distance = 0;//v[i]->getDistance();
			if (preditor.distances.size()>=2)
				distance = preditor.distances[preditor.distances.size()-2];
	
		}
		
		SemanticRelation * squery;
		squery = query->POApred[arg].second;
		old_label= squery->getString();

		string isPredRoot = "0";
		if (query->rootIsPredicate())
			isPredRoot = "1";
		
		NtreesCheked = i;//-1; 16/06/2012
		salida << targetLine << '\t'
					<< targetPredicate<< '\t'
					<< old_label << '\t'
					<< targetNNodes << '\t'
					<< "??\t" //<< sourceLine<< '\t'
					<< "??\t" //<< sourcePredicate<< '\t'
					<< new_label << '\t'
					<< "??\t" //<< sourceNNodes << '\t'
					<< distance << '\t'
					<< "??\t" //<< samples_at_same_distance << '\t'
					<< NtreesCheked  << '\t' 
					<< preditor.getLastCutOff() << '\t'
					<< preditor.getAristocrats() << '\t'
					<< preditor.getSupportingVotes()<<'\t'
					<< preditor.getUsedEquivalenceClass()<< '\t'
					<< isPredRoot << endl;
					//<< preditor.samples.size() << endl;		
		
	}
}


bool allReady (vector<KNNpredictor> & v){
	for (int i = 0; i< (int) v.size() ; i++){
		if(v[i].isready()){
			v[i].turnUnReady();
		}
		else return false;
	}
	for (int i = 0; i< (int) v.size() ; i++)
		v[i].turnReady();
	return true;
	
}




/**
* 16/6/2012
* using the distances of a single tree . all arguments at once AllAtOnce
*
*/
	
void DepTreeCorpus::KNN4AllAtOnceRelabel(DepSubTree * query,ofstream &salida, int & correct, int & total,  ifstream & tableDistances){
		string targetPredicate, sourcePredicate,old_label,new_label;
	int targetLine,targetNNodes,sourceLine,sourceNNodes,samples_at_same_distance,NtreesCheked;
	double distance = 0.0;

	targetLine = targetNNodes = sourceLine = sourceNNodes = samples_at_same_distance =NtreesCheked = -11;
	//    *			*				?				?					?				    *			
	targetPredicate =  sourcePredicate = old_label = new_label = "??";
	//    *				     ?              *             *   
	targetLine = query->line_number;
	targetNNodes= query->size()+1;

	vector<TreeMeasure*> v;//create vector of Alignments. 
	v.reserve(samples.size());
	
	for(unsigned int i = 0; i<samples.size()&&i<params()->maxSamples;i++)	{


			string cad;
			string head;
			string tail;
			double distance=0;
			tableDistances >> cad;
			int position = cad.find(":");
			head = cad.substr(0,position);

			tail = cad.substr(position+1);//remove the head 
			distance = atof(tail.c_str());
		//	cout << distance;
			int samplePos = atof(head.c_str());

			TreeMeasure * pp;
			if (params()->SimilarityMachine==Zhang)
				pp =new TreeDistance(query,samples[samplePos]);
			else 
				pp =new TreeKernel3(query,samples[samplePos]);
			if (params()->N1== false)
				pp->setDistance(distance);
			else 
				pp->setDistance(i);
			v.push_back(pp);
	}
	
//	std::sort(v.begin(), v.end(), icompare);
	


	vector<KNNpredictor> vpreditor;
	for (int i = 0; i<(int)query->POApred.size();i++){
		KNNpredictor preditor (params()->K_for_KNN);
		vpreditor.push_back(preditor);
	}






	 int i = 0;
	 for(i = 0;  i< (int)v.size();i++){

			 Alignment *p= NULL;
			 DepSubTree * sam;
			 v[i]->getAlignament(&p);
			 sam = (DepSubTree*)(v[i]->getCoDomainTree());

		 	for(int arg = 0; arg<(int)query->POApred.size();arg++){//FOR EACH ARGUMENT
			
				Alignment *p= NULL;
				DepSubTree * sam;
				v[i]->getAlignament(&p);
				sam = (DepSubTree*)(v[i]->getCoDomainTree());
			  
				SemanticRelation * ssample;
			 
				bool storePredition = false;
				if(p->match(query->predicateNode,sam->predicateNode))
					for(int samArgPosition = 0; samArgPosition<(int) sam->POApred.size();samArgPosition++){
						ssample = sam->POApred[samArgPosition].second;
						if (p->match(query->POApred[arg].first,sam->POApred[samArgPosition].first)){//they match
							int relation = ssample->getRelation();
							double dis = v[i]->getDistance();
							vpreditor[arg].addSample(relation, dis);


							if (params()->N1==false)
								vpreditor[arg].addSample(relation, dis);
							else //N1== true
								vpreditor[arg].addSample(relation, i);


							storePredition=true;
							break;
					}
				}
				if (storePredition == false)//5-oct-2011
					if (params()->N1==false)
						vpreditor[arg].addSample(-1,v[i]->getDistance());
					else 
						vpreditor[arg].addSample(-1,i);

			}

			if (allReady(vpreditor)){
				break;// all are ready; 
			}
	}


	for(int arg = 0; arg<(int)query->POApred.size();arg++){//FOR EACH ARGUMENT
	


		total++;

	
		targetPredicate=old_label=new_label="?";

		 if(!vpreditor[arg].isready())
			vpreditor[arg].addSample(-1,10000000);//ading null prediction at distance infinite 
		
		 new_label="?NoPrediction";
		//if (preditor.isready())
		 {
			 int newLabelint = vpreditor[arg].getLabel();
			 if (newLabelint == -1 || newLabelint == -2)
				 new_label = "?NoLabel";
			 else
				 new_label = params()->getString(vpreditor[arg].getLabel());
			distance = 0;//v[i]->getDistance();
			if (vpreditor[arg].distances.size()>=2)
				distance = vpreditor[arg].distances[vpreditor[arg].distances.size()-2];
	
		}
		
		SemanticRelation * squery;
		squery = query->POApred[arg].second;
		old_label= squery->getString();

		string isPredRoot = "0";
		if (query->rootIsPredicate())
			isPredRoot = "1";
		
		NtreesCheked = i;//-1; 16/06/2012
		salida << targetLine << '\t'
					<< targetPredicate<< '\t'
					<< old_label << '\t'
					<< targetNNodes << '\t'
					<< "??\t" //<< sourceLine<< '\t'
					<< "??\t" //<< sourcePredicate<< '\t'
					<< new_label << '\t'
					<< "??\t" //<< sourceNNodes << '\t'
					<< distance << '\t'
					<< "??\t" //<< samples_at_same_distance << '\t'
					<< NtreesCheked  << '\t' ;
		salida		<< vpreditor[arg].getLastCutOff() << '\t';
		salida		<< vpreditor[arg].getAristocrats() << '\t';
		salida		<< vpreditor[arg].getSupportingVotes()<<'\t';
		salida		<< vpreditor[arg].getUsedEquivalenceClass()<< '\t';
		salida		<< isPredRoot << endl;
					//<< preditor.samples.size() << endl;		
		
	}
}



void DepTreeCorpus::saveSentencesKendallFormat(string FileName){ // 
	 ofstream salida(FileName.c_str());
	 vector<TreeMeasure*> v;

	 for (int i = 0; i<(int)this->DTcorpora.size();i++){
		 // una lista para cada ejemplo:
		 v.clear();
	 	for(unsigned int j = 0; j<DTcorpora.size();j++){	//rellena el vector.{
			TreeDistance * x = new TreeDistance(DTcorpora[i],DTcorpora[j]);
			x->setVectorPossition(j);
			v.push_back(x);
		}
		std::sort(v.begin(), v.end(), icompare);
		///////////////// now the vector is already calculated. 

		for(unsigned int j = 0; j<DTcorpora.size();j++){
			salida << v[j]->getVectorPossition() << " ";
		}
		salida << endl;
		cout << "progress: " << (100*(double)i)/DTcorpora.size() <<"%" << endl;
	 }
	 salida.close();
}




/**
* 17-oct-2011
* using the distances of a single relational-tree .
*
*/
/*
void DepTreeCorpus::KNN4relabelRel(DepRelTree * query,ofstream &salida, int & correct, int & total,  ifstream & tableDistances){
	string targetPredicate, sourcePredicate,old_label,new_label;
	int targetLine,targetNNodes,sourceLine,sourceNNodes,samples_at_same_distance,NtreesCheked;
	double distance = 0.0;

	targetLine = targetNNodes = sourceLine = sourceNNodes = samples_at_same_distance =NtreesCheked = -11;
	//    *			*				?				?					?				    *			
	targetPredicate =  sourcePredicate = old_label = new_label = "??";
	//    *				     ?              *             *   
	targetLine = query->line_number;
	targetNNodes= query->size()+1;

	vector<TreeMeasure*> v;//create vector of alignments. 
	v.reserve(relTrees.size());
	
	for(unsigned int i = 0; i<relTrees.size();i++)	{
			TreeDistance * pp =new TreeDistance(query,relTrees[i]); 
			

			string cad;
			double distance=0;
			tableDistances >> cad;

			cad = cad.substr(cad.find(":")+1);//remove the head 
			distance = atof(cad.c_str());

			pp->distance=distance;
			v.push_back(pp);
	}
	
	std::sort(v.begin(), v.end(), icompare);
	
	total++;
	targetPredicate=old_label=new_label="?";
	KNNpredictor preditor(params()->K_for_KNN);
		
	targetPredicate = ((depTreeNodeInfo*)query->PO_nodes[query->predicateNode]->info)->getFormString();
	int i = 0;
 	for(i = 0;  i< (int)v.size() && !preditor.isready();i++){
		 Alignment *p= NULL;
		 DepRelTree * sam;
		 v[i]->getAlignament(&p);
		 sam = (DepRelTree*)(v[i]->getCoDomainTree());
			 
		bool storePredition = false;
		if(p->match(query->predicateNode,sam->predicateNode) && p->match(query->argumentNode,sam->argumentNode)){
			
			int relation = sam->rel->getRelation();
			double dis = v[i]->getDistance();
			preditor.addSample(relation, dis);
			storePredition=true;
			break;
		}
		
		if (storePredition == false)//5-oct-2011
			preditor.addSample(-1,v[i]->getDistance());
		
	}//end for
		
	new_label="?";
	if (preditor.isready()){
		int newLabelint = preditor.getLabel();
		if (newLabelint == -1)
			new_label = "?";
		else
			new_label = params()->getString(preditor.getLabel());
		distance = 0;//v[i]->getDistance();
	}
		
	old_label = query->rel->getString();
	
	NtreesCheked = i+1;
	salida << targetLine << '\t'
				<< targetPredicate<< '\t'
				<< old_label << '\t'
				<< targetNNodes << '\t'
				<< "??\t" //<< sourceLine<< '\t'
				<< "??\t" //<< sourcePredicate<< '\t'
				<< new_label << '\t'
				<< "??\t" //<< sourceNNodes << '\t'
				<< distance << '\t'
				<< "??\t" //<< samples_at_same_distance << '\t'
				<< NtreesCheked  << endl;	
}
*/












int getSingleRootedSentences(const char * file_name){
	
 int oneroot = 0;
 int multipleroot = 0;
 int cicles = 0;

  int index;
   string form;
  string lemma;
  string plemma;
  string pos;
  string ppos;
  string feat;
  string pfeat;
  int head;
  string phead;
  string dep_rel;
  string pdep_rel;
  string fill_pr;
  string apred;
  string tpred;


  ifstream two_trees;
  int line_counter = 1;
  int old_line_counter;
  int sentenceNumber = 0;

  two_trees.open(file_name);
  if(!two_trees) {
    cout << "failed to open\n";
    throw "failed to open";
  }
  vector<string> latest_stanza;

  bool no_last = true;
  int errors = 0;


  while (no_last){


    old_line_counter = line_counter;
    no_last = get_next_stanza(two_trees,latest_stanza, line_counter);
	sentenceNumber++;


	int rootcounter = 0; 	
  for(unsigned int i=0; i < latest_stanza.size(); i++) {
    istringstream stanza_str(latest_stanza[i]);
    stanza_str >> index >> form >> lemma >> plemma  >> pos >> ppos >> feat >> pfeat >> head >> phead >> dep_rel >> pdep_rel >> fill_pr >> tpred;
	if (head==0)
		rootcounter++;
  }

  if (rootcounter>1)
	  multipleroot++;
  else if (rootcounter==1)
	  oneroot++;
  else if (rootcounter==0)
	  cicles++;
  else { 
	  cout << "rootcounter == " << rootcounter <<" !!!"<< endl;
  }
  }
	cout << "single rooted sentences = "<< oneroot << "  multiple: " << multipleroot << " cicles: " << cicles << endl; 
	return oneroot;

}







double DepTreeCorpus::getSemanticStructuresEntropy(bool withPred){
	// return the entropy of the semantic labels 

	vector<pair<double,string> > v;
	for (int i = 0; i<samples.size();i++)
	{
		DepSubTree * d = samples[i];

		string cad = "";
		if (withPred){
			cad = d->getLemaPred();
		}
		//	cad+= ((depTreeNodeInfo*)d->PO_nodes(d->predicateNode)->info)->LEMMA;
		for (int j= 0; j<d->POApred.size();j++){
			cad += ":"+d->POApred[j].second->getString();
		}
		int j =0;
		for (;j<v.size();j++){
			if (v[j].second==cad){
				v[j].first++;
				break;
			}
		}
		if (j==v.size()){
			pair<double,string> a;
			a.first= 1;
			a.second=cad;
			v.push_back(a);
		}

	}
	/////////////
	double counter=0;
	for (int i = 0; i<v.size();i++){
		counter += v[i].first;
	}
	
	for (int i = 0; i<v.size();i++){
		v[i].first= v[i].first/counter;
	}
	counter = 0;
	
	for (int i = 0; i<v.size();i++){
		counter+= -v[i].first*log(v[i].first)/log(2.0);
	}


	return counter;



}