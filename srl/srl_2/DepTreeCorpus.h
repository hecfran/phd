/**
 * @file
 * @author  Hector-Hugo Franco-Penya <francoph@scss.tcd.ie> 
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 *
 */



#ifndef DEP_TREE_CORPUS_H
#define DEP_TREE_CORPUS_H

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include "tree.h"
#include "TreeDistance.h"
#include <algorithm>

using namespace std;
#pragma once

double str2num(string cad);
void weka(string  in, string out );
int getSingleRootedSentences(const char * file_name);

class DepTreeCorpus{
public:
	string fileName; // name of the file where the corpus is loaded.
	vector<AbsTreeNode*> ProcessPOSLine(string,int);
	void ProcessDependency( vector<AbsTreeNode*> & v, string & cad);

public:
	vector<DepTree*>    DTcorpora; //!< full sentences from the training data 
	vector<DepSubTree*> samples; //!< all sub-tree extracted
	vector<DepSubTree*> samplesVerbs; //!< all sub-trees which are verbs (english only)
	vector<DepSubTree*> samplesNouns; //!<  all sub-trees which are nouns (english only)
	vector<DepSubTree*> samplesOthers; //!<  missclasified sub-trees
//	vector<DepRelTree*> relTrees;
  	DepTreeCorpus(const char * file_name);
	DepTreeCorpus(const char * file_name, int format);
	void TestEqualAlignments(DepTreeCorpus & q);

	int getSemRel(vector< pair < int, int  > > & semCounter);
	int getSintacticRel(vector< pair < int, int  > > & DepRelCounter);
	//09-05-2012
	void saveSentencesKendallFormat(string FileName);
//	void addToCache(DepSubTree*, double );
//	int cacheIni;
//	int cacheFin;
//	int cachePointer;


	void saveMatrix(string FileName);

  	void save(string FileName); //write back in a file the whole data set.
	void reLabel(DepSubTree *,ofstream &,int & correct, int & total);
	


//	void           KNNreLabel(DepSubTree *,      ofstream &,       int & correct, int & total);
//	void          KNN3relabel(DepSubTree * query,ofstream &salida, int & correct, int & total,  int      & counter);
	void          KNN4relabel(DepSubTree * query,ofstream &salida, int & correct, int & total,  ifstream & tableDistances);
	void KNN4AllAtOnceRelabel(DepSubTree * query,ofstream &salida, int & correct, int & total,  ifstream & tableDistances);

//	void reLabel(DepTreeCorpus & query);
	void reLabelUsingTable(DepTreeCorpus & query, string file);


	void LargeScaleTest();
	//void LargeScaleTestKernel();
	void largeScaleTestSubTrees();
	//void writeTableOfDistances(const char * file);
	void saveRelTrees(string FileName);
	void describeData();
	void countIntermediateArguments();
	int countAmountArguments();

	bool deltaTest();
	void preProcesFile(DepTreeCorpus & query);

private:
//	void preProcesFileRel(DepTreeCorpus & query);
//	void reLabelUsingTableRel(DepTreeCorpus & query, string file);
public:
	void saveDistances(DepSubTree * ,ofstream &);
	void showMap(DepTreeCorpus & query, int source, int target);

	double getSemanticStructuresEntropy(bool withPred);
//	void saveRelationDistances(DepRelTree * ,ofstream &);

	//void TestEqualAlignments(DepTreeCorpus & q);
	
	//(void);
//	~DepTreeCorpus(void);
};



class DepTreeCorpus2 : public DepTreeCorpus{


public:
		
//void putBestInZero(vector<TreeMeasure*> * lista);

	DepTreeCorpus2(const char * file_name);
//	void reLabel2(DepSubTree *,ofstream &,int & correct, int & total);
	void identifyAndLabelMeWhith(DepTreeCorpus & training);
	//vector<DepTree2*>    DTcorpora;

	void evaluate();//(int & TPPosition, int & FPPosition, int & FNPosition,                            int & TPLabel, int & FPLabel, int & FNLabel){
};


class KNNpredictor{
private:
	int lastCutOff; 
	int aristocrats;
	int supportingVotes;
public:
	int prediction;
	int getUsedEquivalenceClass();

	int minK;
	vector<int> samples;
	vector<double>distances;
	bool ready;
	KNNpredictor(int k);
	int getLabel();//old
	bool isready();
	void addSample(int label, double distance);
	//5-oct-2011
	int nearest_cutoff();
	int	predict(bool allowNull);
	//09-oct-2011
	bool calculate();
	int extend(int cut);
	int getLabelUsing(int cut);


	int getLastCutOff();
	int getAristocrats();
	int getSupportingVotes();

	void turnUnReady();
	void turnReady();
};



#endif //DEPT_TREE_CORPUS_H