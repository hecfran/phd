/**
 * @file
 * @author  Hector-Hugo Franco-Penya <francoph@scss.tcd.ie> 
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 *
 */

//#/define windows  true



#include "stdafx.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include "tree.h"


static symbolTableClass symbolTable;
static ClassGlovalParameters GlobalParameters;
//static vector<double> weighs;



/**
* return a vector of weighs, it was used 
*
vector<double>* getWeighs(){
	return &weighs;
}
*/
/**
* Returns a pointer to the settings of the system in current use. 
* 
*/
ClassGlovalParameters * params(){
	return &GlobalParameters;
}

/**
* Takes a number as integer and returns it as a string.
* @param num the number
* @return the string
*/
string num2straux2(int num){
	string a ="";
	if (num == 0) return "0";
	
	while (num>0){
		a = (char)('0'+num%10)+a;
		num = num/10;
	}
	return a;
}



/*

void findtests(DepTreeCorpus & c){
	DepSubTree *p, *q;
	p = q =NULL;
	string predicado = "rise.01";
	int predicadoN = symbolTable.setString(predicado);
	for (int i = 0; i<(int)c.DTcorpora.size();i++){
		for (int j = 0; j<(int)c.DTcorpora[i]->DepSubTrees.size();j++){
			int predicatePOS = c.DTcorpora[i]->DepSubTrees[j]->predicateNode;
			DepTreeNode* node = (DepTreeNode*)c.DTcorpora[i]->DepSubTrees[j]->PO_nodes[predicatePOS];
			(node->predicate);
			bool a = (c.DTcorpora[i]->line_number==8156 || c.DTcorpora[i]->line_number==10769);
			if (predicadoN==node->predicate && a){
				if (p==NULL) p =c.DTcorpora[i]->DepSubTrees[j];
				else {q = c.DTcorpora[i]->DepSubTrees[j];
					TreeDistance td(p,q);
					Alignment *a = NULL;
					cout << "distance" << td.getAlignament(&a)<< endl;
					a->dot_show();
				}
				c.DTcorpora[i]->DepSubTrees[j]->dot_show();
				c.DTcorpora[i]->dot_show();
				cout << "line : " <<c.DTcorpora[i]->line_number<< endl;
			}
		}

	}

}

*/


/*
{
	BINARY,
	TERNARY,
	HAMMING,
	PREDICATE_MATCH,
	ANCESTOR,
	COMPLEX
};
*/

/*
string ClassGlovalParameters::getSimilarityMachineType(){
	switch (this->SimilarityMachine){
	
	case Zhang:
		return "Zhang";
	case TreeKernel:
		return "TreeKernel";
	}
	return " unknow measure"; 
}
*/



/**
@return a string describing the atomic measure used on the expriment.
*/
string ClassGlovalParameters::getAntomicMeasureType(){
//AtomicMeasureType;
	switch (this->AtomicMeasure){
	
	case BINARY:
		return "BINARY";
	case TERNARY:
		return "TERNARY";
	case HAMMING:
		return "HAMMING";
	case PREDICATE_MATCH:
		return "PREDICATE MATCH";
	case ANCESTOR:
		return "ANCESTOR";
	case COMPLEX:
		return "COMPLEX";
	case SBINARY:
		return "SBINARY";
	case STERNARY:
		return "STERNARY";
	case SHAMMING:
		return "SHAMMING";
	case SPREDICATE_MATCH:
		return "SPREDICATE MATCH";
	case SANCESTOR:
		return "SANCESTOR";
	case SCOMPLEX:
		return "SCOMPLEX";
	case DELTA:
		return "DELTA";
	case DeltaHamming:
		return "DeltaHamming";
	case MartinSimilarity:
		return "MartinSimilarity";
	case ALPHATERNARY:
		return "AlphaTernary";
	case SALPHATERNARY:
		return "SAlphaTernary";

	case ALPHAHAMMING:
		return "AlphaHamming";
	case SALPHAHAMMING:
		return "SAlphaHamming";


	case HAMMING3:
		return "HAMMING3";
	case SHAMMING3:
		return "SHAMMING3";
	case HAMMING3PM:
		return "SHAMMING3";
	case SHAMMING3PM:
		return "SHAMMING3";
	}
	return " unknow measure"; 
}


/**
* @param symbol, this integer represents a string
* @return the string represented by such integer.
*
*/
string ClassGlovalParameters::getString(int symbol){
	return symbolTable.getString(symbol);
}


/**
* The constructor set all variable to its default values.
*/
ClassGlovalParameters::ClassGlovalParameters(){
	//cout << "constructor"<<endl;
	allArgumentsEqual= false;//05/07/2012
	identificationTask=false;//05/07/2012
	predMatchCost=0;
	N1 =false;
	allAtOnce = false;
	maxSentenses= -1;
	useStrings = false;

	deleteDepRel = false;		
	splitDataSet=false;
	keepVebs=false;
	keepNoums=false;

	delta = 0;
	allowNullPrediciton=false;
	K_for_KNN = 1;
	AtomicMeasure = BINARY;
	noralize= false;
	//SimilarityMachine = Zhang;
	SimilarityMachine = Zhang;
	accuracy = -1;
	insertDeleteBaseCost=1;
	codeVersion = "SRL_v7.4_(12/7/2012)1N_bug_fixed";
	time (&start);	
	 maxSamples = 1000;

	time_t rawtime;
  	//struct tm * timeinfo;
  	time ( &rawtime );
  	startingTime = localtime ( &rawtime );
	inform << "---------------------------------" << endl;

	srand ((unsigned int) time(NULL) );
	numTAG = rand();
	stringstream auxiliar;
	auxiliar << numTAG;
	stringTAG = auxiliar.str();
	cacheSize=0;
	multipleArgumentsTrees=true;


	system("mkdir results");
	system("md results");
}



/**
* It saves some data regarding the experiment in the log file.
*/
void ClassGlovalParameters::guardar(){
	time (&end);
	double hours = difftime (end,start)/(60*60);

	time_t rawtime;
  	struct tm * timeinfo;

  	time ( &rawtime );
  	timeinfo = localtime ( &rawtime );
	
	inform << "accuracy obtaind: ";
	inform << accuracy*100;
	inform << "%" << endl;
	inform << "the Atomic measure used is: "<< getAntomicMeasureType() << endl;
//	inform << "the similarity machine used is" << this->getSimilarityMachineType() << endl;
	inform << "the process run for: " << hours << " hours" << endl;
	inform << "training data: ";
	inform << traing;
	inform << endl;
	inform << "testing  data: ";
	inform << testing;
	inform << endl;
	inform << "starting local time : " << asctime(startingTime);// << endl;
	inform << "finishing local time: " << asctime (timeinfo);// << endl;
	if (cacheSize>0)
		inform << "cache size= " << cacheSize << endl;
	if (this->noralize)
		inform << "normalizing scores" << endl;
	else inform << "NOT normalize" << endl;
	inform << "insert/delete cost base:" << this->insertDeleteBaseCost << endl;
	if (K_for_KNN != 1) inform << "K (of KNN) = " << K_for_KNN << endl;
	inform << "code version: " << codeVersion << endl;
	inform << "experiment Tag: " <<stringTAG <<endl;
	

	ofstream out;
	out.open("info.txt",ios::app);
	if(!out)
	{
		cout << "not possible to open a info file" << endl;
		exit(-1);
	}
	out << inform.str();
	out.close();
}



/**
*
* @param titulo is the title of the graphs. 
* Plots a graph of itself (a tree)
*
*/
void AbsDepTree::dot_show(string titulo) {
  ofstream f;
  // makes filename /tmp/bloggs_parse.dot
  // prints dot prologue
  
  
  ostringstream auxiliar;
  auxiliar << rand();

  string personal_parse = auxiliar.str()+"graph";
  string personal_cad = personal_parse+"_dep_tree";
//  string personal_parse(getenv("LOGNAME")); // NOT WORKING OVER WINDOWS
  //personal_parse = "/tmp/" + personal_parse + "_dep_tree";
  personal_parse = "./" + personal_parse + "_dep_tree";
  string dot_file;
  //c out << personal_parse << endl;
  
  dot_file = personal_parse + ".dot";
  f.open(dot_file.c_str());
  f << "digraph "<< titulo <<" {\n";
  f << "center=true;";
  f << "ordering=out;\n";
  // f << "size=\"10,10\";\n"; // use this to try to fix size
  // prints the tree internals in dot language
  dot_show(f);
  // print dot epilogue
  f << "};";
  f.close();
  // make /tmp/bloggs_parse.ps
  string dot_command;
  dot_command = "dot -Tps " + dot_file + " -o " + personal_parse + ".ps";
  cout << dot_command << endl;
   system(dot_command.c_str());
   // show the postscript file
   string ghost_command;
   ghost_command = "gv " + personal_parse + ".ps";
   cout << ghost_command << endl;
   system(ghost_command.c_str());
   
   string cad =personal_cad+".ps"; 
   system(cad.c_str());
   
//   system("hecfran_dep_tree.ps");
   
   system("pause");
   
   
   cad ="rm "+personal_parse+".ps"; 
   system(cad.c_str());
   cad ="rm "+personal_parse+".dot"; 
   system(cad.c_str());
	cad ="del "+personal_cad+".ps"; 
   system(cad.c_str());
	cad ="del "+personal_cad+".dot"; 
   system(cad.c_str());

   //system("rm personal_parse.dot");
   //system("del personal_parse.ps");
   //system("del personal_parse.dot");


}


/**
*   It returns a line is the same format as it was read from the data set
*	@return the string line.
*/
string depTreeNodeInfo::getOriginalLine(){
	//stanza_str >> index >> form >> lemma >> plemma  >> pos >> ppos >> feat >> pfeat >> head >> phead >> dep_rel >> pdep_rel >> fill_pr >> tpred;
	string cad;

	stringstream out;
	out << HEAD;
	string head = out.str();
	stringstream out2;
	out2 << PHEAD;
	string phead = out2.str();

	cad = symbolTable.getString(FORM) + "\t"+ symbolTable.getString(LEMMA) ;
	cad += "\t"+ symbolTable.getString(PLEMMA) + "\t"+ symbolTable.getString(POS);
	cad += "\t"+ symbolTable.getString(PPOS) + "\t"+ symbolTable.getString(FEAT);
	cad += "\t"+ symbolTable.getString(PFEAT) + "\t"+ head + "\t" + phead;
	cad += "\t"+ symbolTable.getString(DEPREL) + "\t"+ symbolTable.getString(PDEPREL);

	return  cad;

}


/**
*   It returns a line is the same format as it was read from the data set
*	@return the string line.
*/
string DepTreeNode::getOriginalLine(){
	return info->getOriginalLine();
}



/**
* Create a tree node from a string (line readed from the file data set) and its position
* @param line: the string from the file 
* @param counter: the line number in the file
*/
depTreeNodeInfo::depTreeNodeInfo(string line, int counter)
{
  string form;
  string lemma;
  string plemma;
  string pos;
  string ppos;
  string feat;
  string pfeat;
  //int head;
  //int phead;
  int head;
  int phead;
  string dep_rel;
  string pdep_rel;
  string fill_pr;
  string apred;
  string tpred;
  int index;

  istringstream stanza_str(line);
  stanza_str >> index;

  //dpt= new depTreeNodeInfo(this, index);
  //all_nodes.push_back(dpt);  /// move this stuff to the constructor of depTreeNodeInfo....
  stanza_str >> form >> lemma >> plemma  >> pos >> ppos >> feat >> pfeat >> head >> phead >> dep_rel >> pdep_rel >> fill_pr;


  if (params()->deleteDepRel==true)//16-5-2012
  {
	  pdep_rel=dep_rel="HIDDEN";
  }



    FORM = symbolTable.setString(form);
    LEMMA = symbolTable.setString(lemma);
    PLEMMA = symbolTable.setString(plemma);
    POS= symbolTable.setString(pos);
    PPOS= symbolTable.setString(ppos);
    FEAT= symbolTable.setString(feat);
    PFEAT= symbolTable.setString(pfeat);
    HEAD= head;
    PHEAD= phead;
    DEPREL= symbolTable.setString(dep_rel);
    PDEPREL= symbolTable.setString(pdep_rel);
    FILLPRED= symbolTable.setString(fill_pr);
    //PRED= symbolTable.setString(pred); ??????
    //INDEX = index;
    fileSystemLine = counter;
}

/**   return the deletion cost of itself : by default 1 */
double depTreeNodeInfo::deleteCost(){return 1;}
/**   return the insertion cost of itself : by default 1  */
double depTreeNodeInfo::createCost(){return 1;}
/**   return the swapping cost of itself : by default 1  */
double depTreeNodeInfo::matchCost( AbsNodeInfo*){
	return 1;
};

/**
* @return the label Form
*
*/
string  DepTreeNode::getForm(){
	depTreeNodeInfo *q = (depTreeNodeInfo*) info;
	return q->getFormString();

}

/**
*
* @return true if the node is not a leaf arguement.
*/
bool DepTreeNode::i_am_a_not_leaf_argument(){
	if (i_am_the_root()&& this->children.size()==1){
		return false;
	}

	if (isArg() && isPred() )
		return false;
	

	if (isArg() && !(i_am_a_leaf()))
		return true;
		
	return false;
}

/**
* @param B, and extra node to be compared. it have to be a DepTreeNode
* @returns the swapping cost between them or similarity score multiplied by -1. 
*/
double DepTreeNode::map2cost(AbsTreeNode * B){

	double extra = 0;
	DepTreeNode *a, *b;
	a= this;
	b=(DepTreeNode*)B;
	if(a->isPred() != b->isPred()) extra +=params()->predMatchCost; 
	if(a->isPred() && b->isPred() && a->predicate != b->predicate) extra +=params()->predMatchCost;


	if (this->pegamento && ((DepTreeNode*)B)->pegamento)
		return extra+map2costAux(B,params()->AtomicMeasure)-1000;

	return extra+map2costAux(B,params()->AtomicMeasure);//-0.0000001;
}

/**
* @param B, and extra node to be compared. it have to be a DepTreeNode
* @param atomic especifies which is the atomic setting used. 
* @returns the swapping cost between them or similarity score multiplied by -1. 
* This function is used to help map2cost
*/
double DepTreeNode::map2costAux(AbsTreeNode * B, AtomicMeasureType atomic){
	DepTreeNode *a, *b;
	a= this;
	b=(DepTreeNode*)B;
	depTreeNodeInfo *aa,*bb;
	bb = (depTreeNodeInfo*)B->info;
	aa = (depTreeNodeInfo*)this->info;
	double sumatorio = 0;
	int contador = 0;
	string cadendaPrueba;
	switch (atomic)
	{
	case MartinSimilarity:
			if(aa->POS != bb->POS) return 0;
		if(aa->DEPREL != bb->DEPREL) return 0;
		return -1;

		///
			if(aa->POS == bb->POS) sumatorio-= 0.25;
			if(aa->DEPREL == bb->DEPREL) sumatorio -=0.25;
			if(aa->LEMMA == bb->LEMMA) sumatorio-= 0.25;
			if(aa->FORM== bb->FORM) sumatorio-= 0.25;
    	return sumatorio;

	case DeltaHamming:
//		return params()->delta-(-this->map2costAux(B,SHAMMING));
		return params()->delta-(-this->map2costAux(B,MartinSimilarity));

	case DELTA:
		return -(params()->delta - map2costAux(B,BINARY));
		
	case BINARY:
		if(aa->POS != bb->POS) return 1;
		if(aa->DEPREL != bb->DEPREL) return 1;
		return 0;
		
	case SBINARY:
		return -this->deleteCost2(BINARY) -b->createCost2(BINARY)+map2costAux(b,BINARY);


		//Martin upgrade:
				if((aa->POS == bb->POS) && (aa->DEPREL == bb->DEPREL)) 
					return -1.0;
				return -0.5;
		//

		if((aa->POS == bb->POS) && (aa->DEPREL == bb->DEPREL)) 
//		if((aa->POS == bb->POS) || (aa->DEPREL == bb->DEPREL)) 
			return -1;
		return 0;
				
	case TERNARY:
		if(aa->POS != bb->POS) sumatorio+= 0.5;
		if(aa->DEPREL != bb->DEPREL) sumatorio +=0.5;
		return sumatorio;
	case ALPHATERNARY:
		if(aa->POS != bb->POS) sumatorio+= params()->delta;
		if(aa->DEPREL != bb->DEPREL) sumatorio +=(1.0-params()->delta);
		return sumatorio;

	case SALPHATERNARY:	
		return -this->deleteCost2(ALPHATERNARY) -b->createCost2(ALPHATERNARY) + map2costAux(b,ALPHATERNARY);

	case ALPHAHAMMING: //03/07/2012
			if(aa->POS != bb->POS) sumatorio+= 0.5*params()->delta;
			if(aa->DEPREL != bb->DEPREL) sumatorio +=0.5*params()->delta;
			if(aa->LEMMA != bb->LEMMA) sumatorio+= 0.5* (1.0-params()->delta);
			if(aa->FORM!= bb->FORM) sumatorio+= 0.5* (1.0-params()->delta);
		return sumatorio;
	case SALPHAHAMMING:	
		return -this->deleteCost2(ALPHAHAMMING) -b->createCost2(ALPHAHAMMING) + map2costAux(b,ALPHAHAMMING);



	case ALPHAHAMMING3: //17/5/2012
		if(aa->POS != bb->POS) sumatorio+= 0.5* params()->delta;
		if(aa->DEPREL != bb->DEPREL) sumatorio +=0.5* params()->delta;
		if(aa->LEMMA != bb->LEMMA) sumatorio+=  1.0-params()->delta;

		if(a->isPred() != b->isPred()) sumatorio +=1;
		if(a->isPred() && b->isPred() && aa->LEMMA != bb->LEMMA) sumatorio +=1;

		return sumatorio;

	case SHAPE:
		return 1;


	case HAMMING3:
		if(aa->POS != bb->POS) sumatorio+=0.3333333;
		if(aa->DEPREL != bb->DEPREL) sumatorio +=0.3333333;
		if(aa->LEMMA != bb->LEMMA) sumatorio +=0.3333333;
		return sumatorio;
	case SHAMMING3:
		return -this->deleteCost2(HAMMING3) -b->createCost2(HAMMING3) + map2costAux(b,HAMMING3);

	case HAMMING3PM:
		if(aa->POS != bb->POS) sumatorio+=0.3333333;
		if(aa->DEPREL != bb->DEPREL) sumatorio +=0.3333333;
		if(aa->LEMMA != bb->LEMMA) sumatorio +=0.3333333;
		if(a->isPred() != b->isPred()) sumatorio +=1;
		if(a->isPred() && b->isPred() && a->predicate != b->predicate) sumatorio +=1;
		return sumatorio;
	case SHAMMING3PM:
		return -this->deleteCost2(SHAMMING3PM) -b->createCost2(SHAMMING3PM) + map2costAux(b,SHAMMING3PM);


	

	case STERNARY:	
		return -this->deleteCost2(TERNARY) -b->createCost2(TERNARY) + map2costAux(b,TERNARY);

		/*		//Martin upgrade:
				if((aa->POS == bb->POS) && (aa->DEPREL == bb->DEPREL)) return -1.0;
				if(aa->POS == bb->POS) return -0.5/2-0.5;
				if(aa->DEPREL == bb->DEPREL) return  -0.5/2-0.5;
				return 0-0.5;
				//
		if((aa->POS == bb->POS) && (aa->DEPREL == bb->DEPREL)) return -1;
		if(aa->POS == bb->POS) return -0.5;
		if(aa->DEPREL == bb->DEPREL) return  -0.5;
		return 0;
		*/	
		
		
		
	case HAMMING:
			if(aa->POS != bb->POS) sumatorio+= 0.25;
			if(aa->DEPREL != bb->DEPREL) sumatorio +=0.25;
			if(aa->LEMMA != bb->LEMMA) sumatorio+= 0.25;
			if(aa->FORM!= bb->FORM) sumatorio+= 0.25;
				
			
			//if(a->isPred() != a->isPred()) contador +=0.25;
			//if(a->isPred() && a->isPred() && a->predicate != b->predicate) contador +=0.25;
			return sumatorio;
		
			
			
	case SHAMMING:
		return -this->deleteCost2(HAMMING) -b->createCost2(HAMMING)+map2costAux(b,HAMMING);

/*
			//contador = 0;
			//cout << "contador = " << contador<< endl;
			if(aa->POS == bb->POS) sumatorio-= 0.25;
			if(aa->DEPREL == bb->DEPREL) sumatorio -=0.25;
			if(aa->LEMMA == bb->LEMMA) sumatorio-= 0.25;
			if(aa->FORM== bb->FORM) sumatorio-= 0.25;
			//if(a->isPred() != a->isPred()) contador +=0.25;
			//if(a->isPred() && a->isPred() && a->predicate != b->predicate) contador +=0.25;

			//Martin upgrade
			//if ((aa->POS == bb->POS)&& (aa->DEPREL == bb->DEPREL) && (aa->LEMMA == bb->LEMMA) &&(aa->FORM== bb->FORM))
			//	return -1;
				return (sumatorio-1)/2;
			//

			return sumatorio;
	*/		
			
			
	case PREDICATE_MATCH:
		if(aa->POS != bb->POS) sumatorio+= 0.3;
		if(aa->DEPREL != bb->DEPREL) sumatorio +=0.3;
		
		
		//if(a->isPred() != a->isPred()) sumatorio +=1; 
		if(a->isPred() != b->isPred()) sumatorio +=1; // 26/06/2012
		//if(a->isPred() && a->isPred() && a->predicate != b->predicate) sumatorio +=1;// 26/06/2012
		if(a->isPred() && b->isPred() && a->predicate != b->predicate) sumatorio +=1;

	/*	cadendaPrueba = params()->getString(a->predicate);
		cout << cadendaPrueba << endl;

		cadendaPrueba = params()->getString(b->predicate);
		cout << cadendaPrueba << endl;

		*/
		return sumatorio;

		
	case SPREDICATE_MATCH:
		return -this->deleteCost2(PREDICATE_MATCH) -b->createCost2(PREDICATE_MATCH)+map2costAux(b,PREDICATE_MATCH);


		
		
		
	case COMPLEX:
		//int contador = 0;
		contador+= (aa->DEPREL!=bb->DEPREL);
		contador+= (aa->FORM  !=bb->FORM);
		contador+= (aa->POS   !=bb->POS);

	
		if (contador==1)		sumatorio = 0.1;
		else if(contador==2)	sumatorio = 0.3;
		else if(contador==3)	sumatorio = 0.6;
	
		if (!a->isPred() && b->isPred() ||a->isPred() && !b->isPred() )
			sumatorio+=0.4;
		if(a->isPred() && b->isPred() && aa->LEMMA != bb->LEMMA)
			sumatorio+=0.4;
		if(a->isArg() && !b->isArg() || !a->isArg() && b->isArg())
			sumatorio+=2.0;
		return sumatorio;

/*
	case ANCESTOR:
	
		vector<bool> comparison(30);
		vector<bool> caso(30);

		//cout << symbolTable.getString(a->predicate) << " " <<symbolTable.getString(b->predicate)<<endl;
		//comparison.push_back( symbolTable.getString(a->predicate)=="_" && symbolTable.getString(b->predicate)=="_");
			comparison.push_back( !(a->isPred()) && !(b->isPred()));
		//	cout << "!(a->isPred())=" <<  !(a->isPred()) << " !(b->isPred())=" << !(b->isPred()) << endl;
			//////comparison.push_back(A->pred != B->pred );
		///////comparison.push_back((a->isPred()!=b->isPred()) &&symbolTable.getString(a->predicate)!=symbolTable.getString(b->predicate));
			comparison.push_back(a->predicate!=b->predicate);
		//comparison.push_back(symbolTable.getString(a->predicate)!="_" && symbolTable.getString(b->predicate)!="_");
				comparison.push_back(a->isPred() && b->isPred());


		comparison.push_back(aa->DEPREL!= bb->DEPREL);
		comparison.push_back(aa->POS != bb->POS);
		comparison.push_back(aa->LEMMA!= bb->LEMMA);
	

	caso.push_back(comparison[1]);
	for (int z = 0 ; z<3 ; z++)
		for (int x = 0; x<3; x++)
			for (int y = x; y<3; y++)
				//if (x!=y)
					caso.push_back(comparison[3+x] && comparison[3+y] && comparison[z]);


	while (weighs.size()<caso.size())
		weighs.push_back(0);

	double contador=0;

	for (int x = 0; x <(int)caso.size();x++)
		if (caso[x])
			contador += weighs[x];

	if (a->isArg() && !b->isArg() || !a->isArg() && b->isArg())
				contador += 2;

	return contador;
	

		break; ///OLD SYSTEM;
		contador+= (aa->DEPREL!=bb->DEPREL);
		contador+= (aa->FORM  !=bb->FORM);
		contador+= (aa->POS   !=bb->POS);
		if (contador==1)		sumatorio = 0.1;
		else if(contador==2)	sumatorio = 0.3;
		else if(contador==3)	sumatorio = 0.6;
		if (!a->isPred() && b->isPred() ||a->isPred() && !b->isPred() )		sumatorio+=0.4;
		if ( a->isPred() && b->isPred() && aa->LEMMA != bb->LEMMA) 			sumatorio+=0.4;
		return sumatorio;
		*/
	}

cout << "bug in atomic distance"<< endl;
exit(-1);
return -1;

}

/**
* @param t especifies the atomic measure used. 
* @return the deletion cost of deletin this node.
* This is an auxiliar function
*/
double DepTreeNode::deleteCost2(AtomicMeasureType t){
	switch (t)
	{
		case MartinSimilarity:
			return 0.5;
	case DeltaHamming:
		//return this->deleteCost2(SHAMMING)+params()->delta/2;
		return this->deleteCost2(MartinSimilarity)+params()->delta/2;

	case DELTA:
		return -(params()->delta/2 -deleteCost2(BINARY));

	case BINARY:
	case TERNARY:
	case PREDICATE_MATCH:
	case HAMMING:
	case ALPHATERNARY:
	case ALPHAHAMMING:
	case HAMMING3:
	case HAMMING3PM:
	case ALPHAHAMMING3:
	case SHAPE:

		return params()->insertDeleteBaseCost; //1;
		
	case SALPHATERNARY:
	case SALPHAHAMMING:
	case SBINARY:
	case STERNARY:
	case SHAMMING:
	case SPREDICATE_MATCH:

	case SHAMMING3:
	case SHAMMING3PM:
	

		return 0;

	case ANCESTOR:
		//if(isPred()) 
		//	return 2;
		//if(isArg()) 
		//	return 2;
	//	else 
			return 1;
		break;
	case COMPLEX:
		if(isArg())
			return 2;//&& !b->isArg() || !a->isArg() && b->isArg())
		return 1;
	//default:
	


	}
	cout << "bug in deleting/creating cost"<< endl;
	assert(false);
	return -1;

}

/**
* @return the cost of deleting this node
*
*/
double DepTreeNode::deleteCost(){
	
	return deleteCost2(params()->AtomicMeasure);
}

/**
* @return the cost of inserting this node
* this is an auxiliar function
*/
double DepTreeNode::createCost2(AtomicMeasureType t){
	return this->deleteCost2(t);
}

/**
* @return the cost of inserting this node
*
*/
double DepTreeNode::createCost(){
	return this->deleteCost();
}


/**
* Constructor of a Dependency Tree node.
*
*/
DepTreeNode::DepTreeNode(string c, int i){
	predicate=-1;
	pegamento=false;
  	ImAnArgument=false;
	ImAPredicate=false;
	POindex = -1;
	parent = NULL;
	info = new depTreeNodeInfo(c,i);
}



/**
* @param p parent node
* sets p as parent node of itself.
*/
void AbsTreeNode::setParent(AbsTreeNode * p) {
	if (p !=NULL){
	  parent = p;
	p->children.push_back(this);
	}
}


/**
* @return the amount of leaves under itself.
* if itself is a leave node returns 1.
*/
int AbsTreeNode::number_of_leafs(){
	if (children.size()==0)
		return 1;
	int counter=0;
	for (int i = 0; i<(int)children.size();i++)
		counter+= children[i]->number_of_leafs();
	return counter;
}



/**
* This is an internal recursive funtion 
* @param v this is the vector in traversial post order.
* as the function is called recursively, it will be filled with all nodes of the tree in traversial pos order.
*/
void AbsTreeNode::buildPOVector(vector<AbsTreeNode*> & v){
	for (unsigned int i = 0; i< children.size();i++){
		children[i]->buildPOVector(v);
	}
	v.push_back(this);
}

/*
int depTreeNodeInfo::getForm(){
	return FORM;
}

string depTreeNodeInfo::getFormString() {
	return symbolTable.getString(Form);
	}
depTreeNodeInfo::depTreeNodeInfo(string line, int counter){???????????????};

double depTreeNodeInfo::deleteCost() {
	  return 1;
	  }
 double depTreeNodeInfo::createCost() {return 1;}

 double depTreeNodeInfo::matchCost( AbsNodeInfo* p) {
	 depTreeNodeInfo *q = p;
	  if (POS == q->POS && DEPREL == q->DEPREL)
	  	return 0;
	  else
	  	return 1;
	  }
*/



/**
* @return the semantic relation coded as an iteger (used symbol to get is as string)/
*/
int SemanticRelation::getRelation() {return relation;}

/**
* @param A, sets A as sementic relation. when A is an integer.
*
*/
void SemanticRelation::setRelation(int A) {relation = A;}

/**
* @param A, sets A as semantic relation, when A is a string.
* 
*/
void SemanticRelation::setRelation(string A) {
 	  	relation = 	symbolTable.setString(A);
 	  }







/**
 * this function read a set of lines from a input file and write them in a vector of strings.
 * the set of lines corresponds to one sentence of the data-set.
 * @author Martin Emms
 */
void AbsTree::show_keyroots(){
	vector<int> * k = getKeyRoots();
	vector<int> * l = getMostLeft();
	cout << "key roots=";
	for(unsigned int i = 0; i<k->size();i++)
		cout << k->at(i) << " ";
	cout << endl<< "mostLeft=";
	for(unsigned int i=0; i<l->size();i++)
		cout << l->at(i) << " ";
	cout << endl;
}
	
/**
* @param f is the file in which the dependency tree should be save.
* it saves the dependency tree in CoNLL-2009 format.
*/
void DepTree::save(ofstream &f){
	vector<string> s;
	vector<string> semantic;
	vector<vector<string> >relations;
	string cad = "_";
	bool isPred;
	s.push_back("");
	semantic.push_back("");

	for (int i=1; i<(int)all_nodes.size();i++){
		isPred=false;
		s.push_back(all_nodes[i]->getOriginalLine());
		for(int j=0; j< (int)pred.size();j++)
			if (pred[j]==i)
				isPred=true;
		if(isPred)
			semantic.push_back("Y");
		else
			semantic.push_back("_");
	}

	vector<string> auxiliar;

	for(int x = 0; x<(int)pred.size()+1;x++)
		auxiliar.push_back(cad);
	for(int x = 0; x<(int)all_nodes.size();x++)
		relations.push_back(auxiliar);



	for(int j=0; j< (int)pred.size();j++){
		int predicate = pred[j];
		relations[predicate][0] = "?";// we do not store the predicate type.
		for (int k=0; k<(int)apreds[j].size(); k++){
			int argument = apreds[j][k].first;
			int aux = apreds[j][k].second->getRelation();
			string rel = symbolTable.getString(aux);
			relations[argument][j+1]= rel;
		}
	}





// now all should be written
	for (int i=1 ; i<(int)all_nodes.size();i++){
		f << i << "\t"<< s[i] << "\t"<<semantic[i];
		for (int j = 0; j<(int)relations[i].size();j++){
			f<< "\t" << relations[i][j];
		}
		f<< endl;
	}
	f << endl;
}
/*
	for(unsigned int i=1; i<all_nodes.size();i++){ //starting in 1.
	DepTreeNode * p;
	p = all_nodes[i];
	f << i << "\t";
	f << p->form  << "\t";
	f << p->lem
*/




/*
DepTreeNode::DepTreeNode(string cad,int pos){
	parent = NULL;
	info = new depTreeNodeInfo(cad,pos);
}
*/





/**
* Construct a depencency tree, 
* @param input, is a vector of strings, in which each strings describe one word of the sentence.
* @param counter, especifies from which line in the data set was read.  
*/
DepTree::DepTree(vector<string>& input, int counter) {
  line_number = counter;
//  stanza = input;
  weight=0;
  if (input.size()==0) throw "input not valid";
//  assert(input.size()>0);

//}
//bool DepTree::buildTree()
//{
  unsigned int index = counter;
  string form;
  string lemma;
  string plemma;
  string pos;
  string ppos;
  string feat;
  string pfeat;
  //int head;
  //int phead;
  int head;
  string phead;
  string dep_rel;
  string pdep_rel;
  string fill_pr;
  string apred;
  string tpred;

//  depTreeNodeInfo *dpt;
 // dpt = new depTreeNodeInfo(this, 0); // root is 0
  all_nodes.push_back(NULL);

  for(unsigned int i=0; i < input.size(); i++) {
    DepTreeNode *dpt = new DepTreeNode(input[i],counter+i);

    istringstream stanza_str(input[i]);
    stanza_str >> index;

  //  dpt= new depTreeNodeInfo(this, index);
    all_nodes.push_back(dpt);  /// move this stuff to the constructor of depTreeNodeInfo....
    stanza_str >> form >> lemma >> plemma  >> pos >> ppos >> feat >> pfeat >> head >> phead >> dep_rel >> pdep_rel >> fill_pr >>tpred;
	dpt->predicate = symbolTable.setString(tpred);

    if (fill_pr == "Y") { 
		pred.push_back(index);
	}
  }// all pred is calculated.


  apreds.resize(pred.size());

  for(unsigned int i=0; i < input.size(); i++) {
    istringstream stanza_str(input[i]);
    stanza_str >> index >> form >> lemma >> plemma  >> pos >> ppos >> feat >> pfeat >> head >> phead >> dep_rel >> pdep_rel >> fill_pr >> tpred;
    DepTreeNode *dpt = (DepTreeNode *) all_nodes[i+1];

/*   dpt->globalTable->setString(FORM, form);
   dpt->globalTable->setString(LEMMA, lemma);
   dpt->globalTable->setString(PLEMMA, plemma);
   dpt->globalTable->setString(POS, pos);
   dpt->globalTable->setString(PPOS, ppos);
   dpt->globalTable->setString(FEAT, feat);
   dpt->globalTable->setString(PFEAT, pfeat);
   dpt->globalTable->setString(DEPREL, dep_rel);
   dpt->globalTable->setString(PDEPREL, pdep_rel);
   dpt->globalTable->setString(FILLPRED, fill_pr);
   dpt->globalTable->setString(PRED, tpred);
*/

	if (params()->useStrings)
		dpt->setParent(all_nodes[index-1]);
	else 
		dpt->setParent(all_nodes[head]); // set parent should be enougth.
 //  dpt->setDtr(dpt);

	//bool allArgumentsEqual;//05/07/2012
	//bool identificationTask;//05/07/2012  "?NoPrediction";
   // builing sementic relations
    for(unsigned int j = 0; j < pred.size(); j++) {
      if ( stanza_str.eof() ) throw "not enougth arguments in stanza" ;
      stanza_str >> apred;
	  string apred2 = apred;
	  if (apred2 != "_" && params()->allArgumentsEqual)
		  apred = "argument";//05/07/2012
	  if (apred2 == "_" && params()->identificationTask)
		  apred = "?NoPrediction";//05/07/2012
	  
      SemanticRelation * sem;
      sem = new SemanticRelation();
      sem->setRelation(apred);

      if (apred != "_") {
        pair<unsigned int, SemanticRelation *> sem_dep;
        sem_dep.first = index;
        sem_dep.second = sem;
        apreds[j].push_back(sem_dep);
      }
      else delete sem;
    }
   // if ( ! stanza_str.eof() )   throw "more info than need it"; // it gives some problems :(
  }




 for (unsigned int i =1; i<this->all_nodes.size();i++) all_nodes[i]->setIndex(i);

 buildPOtree();
 if(PO_nodes.size()+1<this->all_nodes.size())
	 throw "multiple root nodes";
 for (unsigned int x = 0; x<PO_nodes.size();x++)
	 PO_nodes[x]->POindex = x;
return;// false;
}




/**
*  This is an internal to construct the dependency tree in a way that can be used by tree edit distance algorithm.
*
*/
void DepTree::buildPOtree(){
	PO_nodes.clear();
	vector<int> POindex;  //PO to word order
	vector<int> WOindex; //Word order to PO
	AbsTreeNode *p;

//	cout << "line number: " << this->line_number << endl;
//	cout <<"all_nodes.size = " << all_nodes.size() << endl;
	assert(all_nodes.size()>=2); // some sentence have only one word. 
	int counter = 0;
	p = all_nodes[1];
	while(p->parent!=NULL) {
		p = p->parent;
		counter++;
		if (counter>100)
			throw "dependency loop";
	}
	p->buildPOVector(PO_nodes);
	//if(PO_nodes.size()+1<this->all_nodes.size())
	// 	throw "multiple root nodes";

	for(unsigned int i=0; i<PO_nodes.size();i++)
		POindex.push_back(PO_nodes[i]->getIndex());
	WOindex = POindex;
	WOindex.push_back(-1);
	WOindex[0]=-1;
	int aux;
	for(unsigned int i=0; i<PO_nodes.size();i++){
		aux = POindex[i];
		WOindex[ aux ]=i;
	}

	
//	 POpred; // contains indices of the nodes which are preds
//	  vector<vector<pair<int, SemanticRelation *> > > POapreds;
//	vector<AbsTreeNode *> PO_nodes;
	//cout << "line number" << this->line_number<<endl;
	POpred = pred;

	for(unsigned int i = 0; i<pred.size();i++)
		POpred[i] = WOindex[pred[i]];

	POapreds = apreds;
	for(unsigned int i = 0; i<POapreds.size();i++)
		for (unsigned int j =0; j<POapreds[i].size();j++){
			POapreds[i][j].first = WOindex[POapreds[i][j].first];
		}
		
	setIdentityNodes();
}


/**
* It extract sub-tree per each argument or predicate.
*
*
*/
void DepTree::buildDepSubTrees(){
	//c/out << "preds";
	//int x;
	//x = POpred[0];
	//AbsTreeNode *pp = this->PO_nodes[x];
	//AbsNodeInfo * qq = pp->info;
	
	for ( int x = 0; x<(int)POpred.size();x++){

		if (params()->multipleArgumentsTrees)
			DepSubTrees.push_back(new DepSubTree(this,x));
		else{
			for (int argument = 0; argument<(int)this->apreds[x].size();argument++)
				DepSubTrees.push_back(new DepSubTree(this,x,argument));//18-oct-2011 hecfran	
		}

	}
}


/**
* builds the tree in traversial post order
* @param root indicates which node is the root node.
*/
void AbsTree::buildPO_nodes(AbsTreeNode * root){
	for (unsigned int x = 0; x<root->children.size();x++)
		buildPO_nodes(root->children[x]);
	PO_nodes.push_back(root);
}


/**
* @return true if the dependency tree node is a predicate.
*/
bool DepTreeNode::isPred(){
	return this->ImAPredicate;
}

/**
* @return true if the dependency tree node is an argument 
*/
bool DepTreeNode::isArg(){
	return this->ImAnArgument;
}


/**
* Extracts a dependency sub-tree from a sentences for a give predicate.
* @param sentence the full sentence
* @param predicateNumber indicates which for which predicate the sub-tree should be extracted first, second...
*/
DepSubTree::DepSubTree(DepTree* sentence,int predicateNumber){
	//Building a subtree;

	assert(predicateNumber < (int)sentence->POapreds.size());
	weight =0;
	int POpredicateNumber = sentence->POpred[predicateNumber];
	AbsTreeNode * predicate = sentence->PO_nodes[POpredicateNumber];
	this->line_number= predicate->info->fileSystemLine;

	//c/out <<  "number of nodes sentence: " << sentence->getNumberOfNodes() << endl;
	vector<bool> inSubTree; // what nodes will be inside.
	vector<int> newPO;
//	vector<vector<DepTreeNode *> > ancestors;
	vector<int> minNodes;

	//cout << "sentence->POpred[predicateNumber]" << sentence->POpred[predicateNumber] << endl;
	minNodes.push_back(sentence->POpred[predicateNumber]);
	for(unsigned int x = 0; x<sentence->POapreds[predicateNumber].size();x++)
		minNodes.push_back(sentence->POapreds[predicateNumber][x].first);
	for(unsigned int x = 0; x<sentence->PO_nodes.size();x++){
		inSubTree.push_back(false);
		newPO.push_back(-1);
	}

	//all minNodes are collected, now we need to join them.

	int max = minNodes[0];
	//c/out << "minNodes.size() = " << minNodes.size()<< endl;
	for (int x = 0; x<(int)minNodes.size();x++){
		///c/out << "min: " << minNodes[x] << " " << x << endl;
		
		inSubTree[minNodes[x]]=true;
		
		
		if(minNodes[x]<max){

			AbsTreeNode * aux = sentence->PO_nodes[minNodes[x]];
			minNodes[x]= aux->parent->POindex;
		}

		if(minNodes[x]>max){
			max = minNodes[x];
			x=-1;
		}
		else if (minNodes[x]!=max)
			x--;
	}

	for(int x = 0,y=0; x<(int)sentence->PO_nodes.size();x++){
		if(inSubTree[x]){
			newPO[x]=y;
			y++;
		}
	}
//	cout << "newPO= ";
//	for (unsigned int x = 0; x<newPO.size();x++) if(newPO[x]!=-1) cout << x <<":"<<  newPO[x] << " ";
	//max = root sub tree;
	//c/out << "sentence line" << this->line_number << endl;
//	cout << "root =" << max << endl;
	AbsTreeNode * root =	copySubTree(sentence,inSubTree,max);
	buildPO_nodes(root);
//	cout << "PO_nodes.size()=" << PO_nodes.size()<<endl;

	int POpredicate = sentence->POpred[predicateNumber];
	predicateNode = newPO[ POpredicate ];
	
	

	for(int x = 0; x< (int) sentence->POapreds[predicateNumber].size();x++)
	{
		pair < int, SemanticRelation * > relation;
		relation = sentence->POapreds[predicateNumber][x];
		relation.first = newPO[relation.first];
		//vector< pair < int, SemanticRelation * > > POApred;
		POApred.push_back(relation);
	}

	assert(PO_nodes.size()>0);
	//int aux;
//	aux = sentence->POpred[predicateNumber];
	//int aux =sentence->POpred(predicateNumber);
	predicateNode=newPO[POpredicateNumber];
	for(int x = 0; x<(int)PO_nodes.size();x++){
		
		PO_nodes[x]->POindex=x;
	}
	setIdentityNodes();
	((DepTreeNode*)PO_nodes[predicateNode])->predicate= ((DepTreeNode*)sentence->PO_nodes[sentence->POpred[predicateNumber]])->predicate;

	this->lemmaPredicate= ((depTreeNodeInfo*)((DepTreeNode*)sentence->PO_nodes[sentence->POpred[predicateNumber]])->info)->LEMMA;
}



/**
* This function extrats a subtree. 
* from a pre-specified list of nodes it add all uncestors up to the first common one. 
* @param sentence original sentence
* @param inSubTree contain a list of booleans specifing wich nodes are already in the sub-tree.
* @param max is an auxiliar parameter used recursivelly, this function should be called with max equal to the amount of nodes of the sentence.
* @return a pointer to the root node of the sub-tree.
*/
DepTreeNode * AbsDepTree::copySubTree(AbsDepTree * sentence,vector<bool> & inSubTree, int max){
	DepTreeNode * p;
	AbsTreeNode * child;
	DepTreeNode * baby;
	AbsTreeNode * aux = sentence->PO_nodes[max];
	DepTreeNode * aux2 = (DepTreeNode*) aux;
	p = new DepTreeNode(aux2);
	for (unsigned int x = 0; x<sentence->PO_nodes[max]->children.size(); x++)
	{
		child = sentence->PO_nodes[max]->children[x];
		if (inSubTree[child->POindex]){
			baby = copySubTree(sentence,inSubTree,child->POindex);
			baby->setParent(p);
		}
	}
	return p;
}


/**
* It creates a dependency tree node identical to the given one.
*
*/
DepTreeNode::DepTreeNode(DepTreeNode * original){
	//ONLY FOR BUILD SUBTREE
	predicate= symbolTable.underscore;//original->predicate;
  	ImAnArgument=false;
	ImAPredicate=false;
	pegamento= false;
    parent = NULL;
    this->children.clear();

    info = original->info;
    Index = -1; //it should not exist;
    POindex = -1;
}



/**
* @return the form of the node as string.
*/
string depTreeNodeInfo::getFormString(){
	return symbolTable.getString(FORM);
}

/**
* @return the Depdencenty relation to its head word as string.
*/
string depTreeNodeInfo::getDepRelation(){
	return symbolTable.getString(DEPREL);
}

/**
* @return the POS as string.
*/
string depTreeNodeInfo::getPOS(){
	return symbolTable.getString(POS);
}

/**
* @return the semantic relation as string.
*/
string SemanticRelation::getString()
{
	return symbolTable.getString(relation);
}



/**
* @return the form of the node as string.
*/
void SemanticRelation::setPrediction( SemanticRelation * prediction){
	relation = prediction->relation;
}

/*bool DepTree::is_pred(int POindex){
	for(int x = 0; x<(int)POpred.size();x++)
		if (POpred[x]==POindex)
			return true;
	return false;
}8*/

/**
* @param POindex specifies a node.
* @return true if the specified node is a predicate.
*/
bool DepSubTree::is_pred(int POindex){
	return (POindex==predicateNode);
}

/*bool DepTree::get_sem_dep(int dep_index, int WOparent_index,string & sem_rel){
	return false;
	//TODO
}*/

/**
* Displays the tree using dot.
*
*/
void AbsDepTree::dot_show(){
	stringstream out;
	out << "line_"<< this->line_number;
	dot_show(out.str());
}


/**
* Display the sub-trees which contains arguments which are not leaves of the tree. 
*
*/
void DepTree::showDangerSubTrees(){
	for (int x = 0; x<(int)this->DepSubTrees.size();x++)
		if (this->DepSubTrees[x]->intermediateArguments()>0)
		{
			this->DepSubTrees[x]->dot_show();
			this->dot_show();
		}
}

/**
* @return the cost of deleting all nodes of the tree 
* nuber of nodes mulpiplied by a constant.
*
*/
double DepSubTree::costDelete(){ //starting in 0?
	double total = 0;
	
//AtomicMeasureType oldType = params()->AtomicMeasure;
	

	for (int i=0; i<(int)this->PO_nodes.size();i++)
	//for (int i=1; i<this->PO_nodes.size();i++)
		total+= params()->insertDeleteBaseCost;//this->PO_nodes.at(i)->createCost();

//	params()->AtomicMeasure= oldType;

	return total;


}


/**
* @return the inserting of deleting all nodes of the tree 
* nuber of nodes mulpiplied by a constant.
*
*/
double DepSubTree::costInsert(){ //starting in 0?
	double total = 0;

	for (int i=0; i<(int)this->PO_nodes.size();i++)
//	for (int i=1; i<this->PO_nodes.size();i++)
		total+= params()->insertDeleteBaseCost;//this->PO_nodes.at(i)->createCost();
	return total;
}


/**
* write in a file the depenceny tree in dot format
* @param f, it the file in which the tree is going to be represented.
*
*/
void DepTree::dot_show(ofstream& f) {
	  vector<unsigned int> sem_parents;
	  for(int x = 0; x < (int)PO_nodes.size(); x++) {
		  string form = ((depTreeNodeInfo * )PO_nodes[x]->info)->getFormString();
		  if (is_pred(x))
			  f << x << " [label=\"" << PO_nodes[x]->Index <<form << "\",color=red,shape=box];"<< endl;
		  else
			  f << x << " [label=\"" << PO_nodes[x]->Index <<form << "\"];"<< endl;
	  }

	  for(int x = 0; x < (int)PO_nodes.size(); x++) {
		  string deprel = ((depTreeNodeInfo * )PO_nodes[x]->info)->getDepRelation();
		 if(PO_nodes[x]->parent!=NULL){
			f <<  PO_nodes[x]->parent->POindex << " -> " << x <<  " [label=\"" << deprel << "\",color=blue,fontsize=10];"<< endl;
		 }
	  }
	  for(int x = 0; x < (int)POpred.size(); x++) {
		  for (unsigned int y =0 ; y<POapreds[x].size();y++){
			  f <<  POpred[x] << " -> " << POapreds[x][y].first <<  " [label=\"" << POapreds[x][y].second->getString() << "\",constraint=false,color=red,style=bold,fontcolor=red,fontsize=10];"<< endl;
		  }
	  }
}


/**
* @return the amount of intermediate arguments in a dependency sub tree
* intermediate arguments are the ones not place in leaves. 
*
*/
int DepSubTree::intermediateArguments(){
		int counter = 0;
		for (int x = 0; x<(int)this->PO_nodes.size();x++){
			if (((DepTreeNode*)(PO_nodes[x]))->i_am_a_not_leaf_argument())
				counter++;
		}
		return counter;
}

/**
* Displays itself.
*/
void DepSubTree::dot_show(){
		AbsDepTree *p = (AbsDepTree *) this;
		p->dot_show();
}


/**
* Prit itself in dot format
* @param f is the file in which it should be printed.
*/
void DepSubTree::dot_show(ofstream& f) {
	  vector<unsigned int> sem_parents;

	  //for(int x = 0; x < (int)PO_nodes.size(); x++) {
	//	  cout << PO_nodes[x]->POindex << " ";
	 // }

	  for(int x = 0; x < (int)PO_nodes.size(); x++) {
		  string form = ((depTreeNodeInfo * )PO_nodes[x]->info)->getFormString();
		  if (is_pred(x))
		    f << x << " [label=\"" << PO_nodes[x]->POindex <<form << "\",color=red,shape=box];"<< endl;
		  else
		  	f << x << " [label=\"" << PO_nodes[x]->POindex <<form << "\"];"<< endl;
	  }

	  for(int x = 0; x < (int)PO_nodes.size(); x++) {
		  string deprel = ((depTreeNodeInfo * )PO_nodes[x]->info)->getDepRelation();
		 if(PO_nodes[x]->parent!=NULL){
			f <<  PO_nodes[x]->parent->POindex << " -> " << x <<  " [label=\"" << deprel << "\",color=blue,fontsize=10];"<< endl;
		 }
	  }
	  for(int x = 0; x < (int)POApred.size(); x++) {
		  //cout << "x= " << x << endl;
		  //cout << "POApred.size()= " << POApred.size() << endl;
		  //cout << "POpred.size()= " << POpred.size() << endl;
		  //cout << "POpred[x] = " << POpred[x] << endl;
		  //cout << "POApred[x].first = " << POApred[x].first << endl;
		  //cout << "POApred[x].second->getString() = " << POApred[x].second->getString() << endl;

			//  cout <<  predicateNode << " -> " << POApred[x].first <<  " [label=\"" << POApred[x].second->getString() << "\",color=red,fontcolor=red,fontsize=10];"<< endl;
			  f <<  predicateNode << " -> " << POApred[x].first <<  " [label=\"" << POApred[x].second->getString() << "\",constraint=false,color=red,style=bold,fontcolor=red,fontsize=10];"<< endl;

	  }
}


/*
void DepRelTree::dot_show(ofstream& f){
	  vector<unsigned int> sem_parents;


	  for(int x = 0; x < (int)PO_nodes.size(); x++) {
		  string form = ((depTreeNodeInfo * )PO_nodes[x]->info)->getFormString();
		  if (is_pred(x))
		    f << x << " [label=\"" << PO_nodes[x]->POindex <<form << "\",color=red,shape=box];"<< endl;
		  else
		  	f << x << " [label=\"" << PO_nodes[x]->POindex <<form << "\"];"<< endl;
	  }

	  for(int x = 0; x < (int)PO_nodes.size(); x++) {
		  string deprel = ((depTreeNodeInfo * )PO_nodes[x]->info)->getDepRelation();
		 if(PO_nodes[x]->parent!=NULL){
			f <<  PO_nodes[x]->parent->POindex << " -> " << x <<  " [label=\"" << deprel << "\",color=blue,fontsize=10];"<< endl;
		 }
	  }

  	  f <<  predicateNode << " -> " << argumentNode <<  " [label=\"" << rel->getString() << "\",constraint=false,color=red,style=bold,fontcolor=red,fontsize=10];"<< endl;

}
*/

/**
* @return the size of the tree.
*
*/
unsigned int AbsTree::size(){
	return PO_nodes.size();
}
/*
DepRelTree::DepRelTree(DepTree* sentence ,int predPosition, int argPosition){
	assert(predPosition < (int)sentence->POapreds.size());
	assert(argPosition < (int)sentence->POapreds[predPosition].size());
	int POpred = sentence->POpred[predPosition];
	int POarg  = sentence->POapreds[predPosition][argPosition].first;
	AbsTreeNode * predicate = sentence->PO_nodes[POpred];
	this->line_number= predicate->info->fileSystemLine;

	vector<bool> inSubTree; // what nodes will be inside.
	vector<int> newPO;
	vector<int> minNodes;

//	cout << "sentence->POpred[predicateNumber]" << sentence->POpred[predicateNumber] << endl;
	minNodes.push_back(POpred);
	minNodes.push_back(POarg);
	for(unsigned int x = 0; x<sentence->PO_nodes.size();x++){
		inSubTree.push_back(false);
		newPO.push_back(-1);
	}

	//all minNodes are collected, now we need to join them.

	int max = minNodes[0];
	for (int x = 0; x<(int)minNodes.size();x++){	
		inSubTree[minNodes[x]]=true;
		if(minNodes[x]<max){
			AbsTreeNode * aux = sentence->PO_nodes[minNodes[x]];
			minNodes[x]= aux->parent->POindex;
		}
		if(minNodes[x]>max){
			max = minNodes[x];
			x=-1;
		}
		else if (minNodes[x]!=max)
			x--;
	}

	for(int x = 0,y=0; x<(int)sentence->PO_nodes.size();x++){
		if(inSubTree[x]){
			newPO[x]=y;
			y++;
		}
	}
//	cout << "newPO= ";
//	for (unsigned int x = 0; x<newPO.size();x++) if(newPO[x]!=-1) cout << x <<":"<<  newPO[x] << " ";
	//max = root sub tree;
	//c/out << "sentence line" << this->line_number << endl;
	//cout << "root =" << max << endl;
	AbsTreeNode * root = copySubTree(sentence,inSubTree,max);
	buildPO_nodes(root);
	rel = sentence->POapreds[predPosition][argPosition].second;
	assert(PO_nodes.size()>0);
	predicateNode=newPO[POpred];
	argumentNode= newPO[POarg]; 
	
	setIdentityNodes();
	
	((DepTreeNode*)PO_nodes[predicateNode])->predicate= ((DepTreeNode*)sentence->PO_nodes[sentence->POpred[predPosition]])->predicate;
}

*/

/**
* @param POindex indicates a particular node in the tree in traversial post order.
* @return true if the particular nod is a predicate or not.
* It asumes the tree is an instance of dependency relation tree
*/
bool AbsDepTree::is_pred(int POindex){
	return ((DepTreeNode*)PO_nodes[POindex])->isPred();
}


/**
* @param POindex indicates a particular node in the tree in traversial post order.
* @return true if the particular nod is an argument
* It asumes the tree is an instance of dependency relation tree
*/
bool AbsDepTree::is_arg(int POindex){
	return ((DepTreeNode*)PO_nodes[POindex])->isArg();
}

/*
bool DepRelTree::is_pred(int POindex){
	return POindex == predicateNode;
}
*/


/**
* @return the branching factor of the tree.
*/
double AbsTree::no_leaf_branch_factor(){
	// arrows / no_leaves_nodes
	return ((double)PO_nodes.size()-1)/((double)PO_nodes.size()- (double)PO_nodes.back()->number_of_leafs());
}

/**
*
* @returns a vector of keyRoot positions.
*
*/
vector<int> * AbsTree::getKeyRoots(){
	assert(keyRoots.size()>0);
	return &keyRoots;
}


/**
* @returns a vector of most left possin for each node on the tree.
*
*/
vector<int> * AbsTree::getMostLeft(){
	assert(mostLeft.size()>0);
	return &mostLeft;
}

/**
* Prepares a tree to be used by the tree edit distance algorithm.
* 
*/
void AbsTree::ready2TreeDistance(){
// prepar mostLeft and key Roots;
	mostLeft.reserve(PO_nodes.size());
	assert(mostLeft.size()==0);
	mostLeft.push_back(-1);
	for(unsigned int i = 0; i<PO_nodes.size();i++){
		mostLeft.push_back(PO_nodes[i]->getMostLeftPO()+1);
	}

	for(unsigned int rootCandidate = 0; rootCandidate<PO_nodes.size();rootCandidate++){
		if (PO_nodes[rootCandidate]->parent==NULL)
			keyRoots.push_back(rootCandidate+1);
		else{
			int posParent = PO_nodes[rootCandidate]->parent->POindex;
			int l1= 	mostLeft[rootCandidate+1];
			int l2=		mostLeft[posParent+1];
			if (l1!=l2)
				keyRoots.push_back(rootCandidate+1);
		}
	}
	return;
}


/**
* @retun the traversial post order indext most left descendant node.
*
*/
int AbsTreeNode::getMostLeftPO(){
	if (children.size()>0)
		return children[0]->getMostLeftPO();
	else
		return POindex;
}


/**
* displays the depencency tree.
*
*/
void DepTree::dot_show(){
	AbsDepTree *p = (AbsDepTree *) this;
	p->dot_show();
}

/**
*	Sets the label ImAnArgument and ImAPredicate true on the correspondent nodes.
*/
void DepTree::setIdentityNodes(){
	for (unsigned int i = 0; i<(int)POapreds.size();i++)
		for(unsigned int j = 0; j<POapreds[i].size();j++)
			((DepTreeNode*)PO_nodes[POapreds[i][j].first])->ImAnArgument=true;
		
	for (unsigned int i=0; i<POpred.size();i++)
		((DepTreeNode*)PO_nodes[POpred[i]])->ImAPredicate=true;
}
/*
void DepRelTree::setIdentityNodes(){
	((DepTreeNode*)PO_nodes[predicateNode])->ImAPredicate=true;
	((DepTreeNode*)PO_nodes[argumentNode])->ImAnArgument=true;
}
*/


/**
*	Sets the label ImAnArgument and ImAPredicate true on the correspondent nodes.
*/
void DepSubTree::setIdentityNodes(){

	((DepTreeNode*)PO_nodes[predicateNode])->ImAPredicate=true;
	for(unsigned int i = 0;  i<POApred.size();i++)
		((DepTreeNode*)PO_nodes[POApred[i].first])->ImAnArgument=true;
		
}
  


/*

void DepTree::dot_show(ofstream& f) {
  vector<unsigned int> sem_parents;
  for(unsigned int dep_index = 1; dep_index < all_nodes.size(); dep_index++) {
	 string form = ((depTreeNodeInfo * )all_nodes[dep_index]->info)->getFormString();
	 cout << "form = " << form << endl;

    f << dep_index << " [label=\"" << dep_index << " " << form << "\"";
    if(is_pred(dep_index)) {
      f << ",fontcolor=red,color=red";
    }

    f << "];" << endl;
    if(dep_index == 1) {}
    else {
      // print syntax edge
      unsigned int parent_index;
      parent_index = (all_nodes[dep_index]->parent)->Index;
      f << parent_index << " -> " << dep_index ;
      AbsTreeNode * aux =  all_nodes[dep_index];
      depTreeNodeInfo * aux2 = (( depTreeNodeInfo *)aux->info );
      f << " [label=\"" << aux2->getDepRelation() << "\"];" << endl;
      // possibly print sem edge parallel to syntax edge
      string sem_rel = "";
      if(get_sem_dep(dep_index, parent_index,sem_rel)) {
      f << parent_index << " -> " << dep_index ;
      f << " [label=\"" << sem_rel << "\"";
      //      f << ",constraint=false,fontcolor=red,color=red ];" << endl;
      //      f << ",constraint=false,fontcolor=red,color=red,labelfloat=true ];" << endl;
      f << ",constraint=false,fontcolor=red,color=red,fontsize=10 ];" << endl;
      }
    }
  }

*/  // finally print semantic edges which do not coincide with syntactic edges
/*
  for(unsigned int i = 0; i < pred.size(); i++) {
    for(unsigned int  j = 0; j < apreds[i].size(); j++) {
      string syn_rel = "";
      if(get_syn_dep(apreds[i][j].first,pred[i],syn_rel) == false) {
	f << pred[i] << " -> " << apreds[i][j].first;
        f << " [label=\"" << apreds[i][j].second << "\"";
	//	f << ",constraint=false,fontcolor=red,color=red ];" << endl;
	//	f << ",constraint=false,fontcolor=red,color=red,labelfloat=true ];" << endl;
	f << ",constraint=false,fontcolor=red,color=red,fontsize=10 ];" << endl;

      }

    }
  }*//*
}*/


/*
int main( int argc, const char* argv[]) {

  if (argc != 3){
    cout << " use: TDSRL trainingSet testingSet " << endl;
    cout << " loading development " << endl;

    Corpus_class training("train.txt");//("trees");
    cout << " loading testing " << endl;
    Corpus_class testing("dev.txt");//("trees");

    training.save("copyTrain.txt");
    return 0;
  }

  Corpus_class training(argv[1]);
  Corpus_class testing(argv[2]);
  training.save("copyTrain.txt");

  return 0;
}

*/

/**
*	Display on the shell POS,DepRelation and Form of all nodes in the tree.
*/
void AbsDepTree::show_info_nodes(){
	for (int i = 0; i< (int)this->PO_nodes.size();i++){
		cout << "(Node: " << i;
		
		cout << "POS=" << ((depTreeNodeInfo*)(PO_nodes[i]->info))->getPOS();
		cout << ",deprel=" << ((depTreeNodeInfo*)(PO_nodes[i]->info))->getDepRelation();
		cout << ",form=" << ((depTreeNodeInfo*)(PO_nodes[i]->info))->getFormString();
		cout << ")"<< endl;
	}
	//	cout << ",lemmaS=" << ((depTreeNodeInfo*)(PO_nodes[i]->info))->L();


//		POS != bb->POS) sumatorio+= 0.25;
//			if(aa->DEPREL != bb->DEPREL) sumatorio +=0.25;
//			if(aa->LEMMA != bb->LEMMA) sumatorio+= 0.25;
//			if(aa->FORM!
	
}

/**
* Empty constructor.
*/
DepTree2::DepTree2(vector<string> & stanza, int line_number):DepTree(stanza,line_number){
}




/**
* Evaluates the prediction and identification of the labelling system for this particular depencency tree
* @param TPPosition is a counter of True Positives on argument identification,  
* @param FPPosition is a counter of False Positives on argument identification,
* @param FNPosition is a counter of False Negatives on argument identification,
*
* @param TPlabels is a counter of True Positive labels identified and predicted,
* @param FPlabels is a counter of False Positive labels identified or predicted,
* @param FNlabels is a counter of False Negative labels, the system didn't identified or it identified the arguments but predicted a wrong label,
*
*/
void DepTree2::evaluate(int & TPPosition, int & FPPosition, int & FNPosition, int & TPLabel, int & FPLabel, int & FNLabel){
	 // vector<vector<pair<int, SemanticRelation *> > > POapreds;
	 // 	vector<vector<pair<int, SemanticRelation *> > > NEW_POapreds;
	 //  vector<int> POpred; 

	for(int i= 0;i<(int)POpred.size();i++){
		vector<pair<int, SemanticRelation *> > * goal = &POapreds[i];
		vector<pair<int, SemanticRelation *> > * pred = &NEW_POapreds[i];

		for (int j = 0; j<(int)goal->size();j++){
			bool findJPos =false;
			bool findJLab =false;
			for( int k = 0; k < (int)pred->size();k++){

				if ((*goal)[j].first==(*pred)[k].first){
					TPPosition++;
					findJPos=true;
					//cout << (*goal)[j].second->getString() << " " << (*pred)[k].second->getString() << endl;
					
			
					if ((*goal)[j].second->getRelation() ==(*pred)[k].second->getRelation()){
						TPLabel++;
						findJLab=true;
					}

				}
			}
			if (!findJPos) FNPosition++;
			if (!findJLab) FNLabel ++;
		}

		for (int p = 0; p<(int)(*pred).size();p++){
			bool findJPos =false;
			bool findJLab =false;
			for( int g = 0; g <(int) (*goal).size();g++){

				if ((*goal)[g].first==(*pred)[p].first){
		
					findJPos=true;
					if ((*goal)[g].second->getRelation()==(*pred)[p].second->getRelation())
						findJLab=true;
				}
			}
			if (!findJPos) FPPosition++;
			if (!findJLab) FPLabel++;
		}

	}

}

/**
* convert a number (double) into string
*/
string double2str(double num){
	stringstream sstr;
    sstr << num;
    return sstr.str();
}


/**
* @param t a given atomic measure type.
* @return a string representing that atomic measure.
*/
string argType2str(AtomicMeasureType t){
	switch (t){
	case BINARY:
		return "BIN";
	case SBINARY:
		return "SBI";
	case TERNARY:
		return "TER";
	case STERNARY:
		return "STE";
	case PREDICATE_MATCH:
		return "PMA";
	case SPREDICATE_MATCH:
		return "SPM";
	case HAMMING:
		return "HAM";
	case SHAMMING:
		return "SHAM";
	case DELTA:
		return "DEL";
	case DeltaHamming:
		return "DeH";
	case MartinSimilarity:
		return "MSi";
	case ALPHATERNARY:
		return "ATER";
	case SALPHATERNARY:
		return "SATER";


	case ALPHAHAMMING:
		return "AHAM";
	case SALPHAHAMMING:
		return "SAHAM";

	case HAMMING3: 
		return "HAMT";
	case SHAMMING3: 
		return "SHAMT";
	case HAMMING3PM:
		return "HAMPM";
	case SHAMMING3PM:
		return "SHAMPM";
	case ALPHAHAMMING3:
		return "AlphaHammingThree";
	case SHAPE:
		return "SHAPE";


	default:
		return "???";
	}

}

/*
* 10 Oct 2011
* This functions process the arguments 
*
*/


/**
* For a given vector of arguments, it changes the parameters of the system.
*/
void ClassGlovalParameters::readParams(vector<string> arg){
	


	for (int pointer = 1; pointer<(int)arg.size();pointer++){
		if (arg[pointer] == "-compareDataSets")
		return;


		if (arg[pointer]=="-training"){
			pointer++;
			traing = arg[pointer];
		}
		else if(arg[pointer]=="-allArgumentsEqual"){
			allArgumentsEqual= true;

		}else if(arg[pointer]=="-identificationTask"){
			identificationTask = true;
		}else if (arg[pointer]=="-predMatchCost"){//26/06/2012
			pointer++;
			predMatchCost =  atof(arg[pointer].c_str());
		}		
		else if (arg[pointer]=="-1N"){
			N1 = true;
		}
		else if (arg[pointer]=="-allAtOnce"){
			allAtOnce = true;
		}
		else if (arg[pointer]=="-getSingleRootedSentences"){}
		else if (arg[pointer]=="-jointEntropy"){}
		else if (arg[pointer]=="-leveinstan"){
			this->useStrings=true;
			stringTAG += "lev_";
			stringTagTrees +="lev_";
		}
		else if (arg[pointer]=="-deleteDepRel"){
			stringTAG += "deleteDepRel_";
			stringTagTrees +="deleteDepRel_";
			deleteDepRel= true;
		}
		else if (arg[pointer]=="-maxSentences"){
			pointer ++;
			this->maxSentenses = (int) str2num(arg[pointer]);
		}
		else if(arg[pointer]=="-testing"){
			pointer++;
			this->testing = arg[pointer];
		}
		else if(arg[pointer]=="-nullPrediction"){
			pointer++;
			if (arg[pointer][0]=='Y' || arg[pointer][0]=='T')
				this->allowNullPrediciton=true;
			else allowNullPrediciton=false;
		}
		else if(arg[pointer]=="-atomicMeasure"){
			pointer++;
			if(	arg[pointer]=="binary" || arg[pointer]=="0")
				AtomicMeasure= BINARY;
			else if(arg[pointer]=="sbinary" || arg[pointer]=="1")
				AtomicMeasure= SBINARY;
			else if(arg[pointer]=="ternary" || arg[pointer]=="2")
				AtomicMeasure= TERNARY;
			else if(arg[pointer]=="sternary" || arg[pointer]=="3")
				AtomicMeasure= STERNARY;
			else if(arg[pointer]=="hamming" || arg[pointer]=="4")
				AtomicMeasure= HAMMING;
			else if(arg[pointer]=="shamming" || arg[pointer]=="5")
				AtomicMeasure= SHAMMING;
			else if(arg[pointer]=="predicateMatch" || arg[pointer]=="6")
				AtomicMeasure= PREDICATE_MATCH;
			else if(arg[pointer]=="spredicateMatch" || arg[pointer]=="7")
				AtomicMeasure= SPREDICATE_MATCH;
			else if(arg[pointer]=="delta" || arg[pointer]=="10")
				AtomicMeasure= DELTA;
			else if(arg[pointer]=="deltaHamming" || arg[pointer]=="11")
				AtomicMeasure= DeltaHamming;
			else if(arg[pointer]=="MartinSimilarity" || arg[pointer]=="12")
				AtomicMeasure= MartinSimilarity;
			else if(arg[pointer]=="ALPHATERNARY" || arg[pointer]=="22")
				AtomicMeasure= ALPHATERNARY;
			else if(arg[pointer]=="SALPHATERNARY" || arg[pointer]=="23")
				AtomicMeasure= SALPHATERNARY;
			else if(arg[pointer]=="ALPHAHAMMING" || arg[pointer]=="30")
				AtomicMeasure= ALPHAHAMMING;
			else if(arg[pointer]=="SALPHAHAMMING" || arg[pointer]=="31")
				AtomicMeasure= SALPHAHAMMING;

			else if(arg[pointer]=="HAMMING3" || arg[pointer]=="24")
				AtomicMeasure= HAMMING3;
			else if(arg[pointer]=="SHAMMING3" || arg[pointer]=="25")
				AtomicMeasure= SHAMMING3;
			else if(arg[pointer]=="HAMMING3PM" || arg[pointer]=="26")
				AtomicMeasure= HAMMING3PM;
			else if(arg[pointer]=="SHAMMING3PM" || arg[pointer]=="27")
				AtomicMeasure= SHAMMING3PM;
			else if(arg[pointer]=="ALPHAHAMMING3" || arg[pointer]=="28")
				AtomicMeasure = ALPHAHAMMING3;
			else if(arg[pointer]=="SHAPE" || arg[pointer]=="50")
				AtomicMeasure = SHAPE;





			else cout << "this may be a bug! ";
		}		//chache size???
		else if(arg[pointer]=="-delta"){
			pointer++;
			this->delta = atof(arg[pointer].c_str());
		}
		else if(arg[pointer]=="-insertCost"){
			pointer++;
			this->insertDeleteBaseCost = atof(arg[pointer].c_str());
		}
		else if(arg[pointer]=="-k"){
			pointer++;
			this->K_for_KNN = (int)atof(arg[pointer].c_str());
		}
		else if(arg[pointer]=="-normalize"){
			pointer++;
			if (arg[pointer][0]=='Y' || arg[pointer][0]=='T')
				this->noralize=true;
			else noralize=false;
		}
		else if(arg[pointer]=="-tag"){
			pointer++;
			stringTAG = arg[pointer];
			stringTagTrees = stringTAG;
		}
		else if(arg[pointer]=="-multipleArguments"){
			pointer++;
			if (arg[pointer][0]=='Y' || arg[pointer][0]=='T')
				this->multipleArgumentsTrees=true;
			else multipleArgumentsTrees=false;
		}
		else if(arg[pointer]=="-treeDistance"){
			SimilarityMachine = Zhang;		
		}
		else if(arg[pointer]=="-treeKernel"){
			SimilarityMachine = TreeKernel;
		}
		else if(arg[pointer]=="-generateTables"){
			//do nothing
		}
		else if(arg[pointer]=="-predict"){
			//do nothing
		}
		else if(arg[pointer]=="-stats"){
		}
		else if(arg[pointer]=="-saveTrainingSentencesKendallFormat"){
		}

		else if(arg[pointer]=="-help"){
			help();
		}
		else if(arg[pointer]=="-onlyVerbs"){
			this->splitDataSet=true;
			this->keepVebs=true;
			this->keepNoums=false;
			cout << "warning: onlyVerbs option only works in english";
		}
		else if(arg[pointer]=="-getkendallValue")
			pointer+=2;
		else if(arg[pointer]=="-onlyNoums"){
			this->splitDataSet=true;
			this->keepNoums=true;
			this->keepVebs=false;;
			cout << "warning: onlyNoums option only works in english";
		}
		else if(arg[pointer]=="-verbsAndNoums"){
			this->splitDataSet=true;
			this->keepVebs=true;
			this->keepNoums=true;
			cout << "warning: verbsAndNoums option only works in english";
		}
		else if (arg[pointer].length()>2){
			cout << "Unkown parameter '" << arg[pointer] << "'"<< endl;
			help();
		}
	}
	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	//after reading all parameters

			//bool allArgumentsEqual;//05/07/2012
		//bool identificationTask;//05/07/2012  "?NoPrediction";
	if(allArgumentsEqual){
		stringTAG += "allArgEqual_";
		stringTagTrees +="allArgEqual_";
	}

	if(identificationTask){
		stringTAG += "identify_";
		stringTagTrees +="identify_";
	}

	if (noralize==true)
	{
		stringTAG += "N_";
		stringTagTrees +="N_";
	}
	else{
		stringTAG += "B_";
		stringTagTrees +="B_";
	}

	if (multipleArgumentsTrees==false){
		stringTAG += "singleArg_";
		stringTagTrees +="singleArg_";
	}



	if (SimilarityMachine == TreeKernel){
		stringTAG += "TK_";
		stringTagTrees +="TK_";
	}

	{
	stringTAG += "del";
	stringTAG += double2str(insertDeleteBaseCost);
	stringTAG += "_";

	stringTagTrees += "del";
	stringTagTrees += double2str(insertDeleteBaseCost);//new
	stringTagTrees+= "_";
	}


	if (predMatchCost!=0){//predMatchCost
		stringTAG += "predMatchCost";
		stringTAG += double2str(predMatchCost);
		stringTAG += "_";

		stringTagTrees += "predMatchCost";
		stringTagTrees += double2str(predMatchCost);//new
		stringTagTrees+= "_";
	}


	{
		stringTAG +="k";
		stringTAG +=num2straux2(this->K_for_KNN);
		stringTAG +="_";
	}


	if (N1)
			stringTAG +="N1_";	
	if (allAtOnce)
		stringTAG += "AllAtOnce_";



	stringTAG += argType2str(AtomicMeasure);
	stringTagTrees += argType2str(AtomicMeasure);


	if (AtomicMeasure == ALPHATERNARY || AtomicMeasure == SALPHATERNARY || AtomicMeasure == ALPHAHAMMING3 || AtomicMeasure == ALPHAHAMMING || AtomicMeasure == SALPHAHAMMING)
	{
		stringTAG += double2str(params()->delta);
		stringTagTrees += double2str(params()->delta);
	}


	if (this->maxSentenses !=-1)
	{
		stringTAG +="limSent"+num2straux2(maxSentenses);		
		stringTagTrees +="limSent"+num2straux2(maxSentenses);

	}


	
	//builiding the TAG: 10-FEB-2012
	if(this->keepVebs){
		stringTAG += "verbs_";
		stringTagTrees +="verbs_";
	}
	if(this->keepNoums){
		stringTAG += "noums_";
		stringTagTrees +="noums_";
	}

}


 

/**
* return true if both vectors of characters are identical.
*/
bool compare(const char* a1, const char* a2){
string a(a1);
string b(a2);
return (a==b);
}

/*
* 14 Oct 2011
* This function run the experiment; 
* 
*/
/**
* Runs the experiment
* @param arg, list of arguments taken from the shell when the software was called.
*/
void ClassGlovalParameters::execute(vector<string> arg){
	readParams(arg);
	if (arg.size()==1) help();
	cout << "software version: "<<  params()->codeVersion << endl;
	cout << "params:" << endl;
	for (int pointer = 1; pointer<(int)arg.size();pointer++)
		cout << "'" << arg[pointer]<<"' \n";
	cout << endl;






	for (int pointer = 0; pointer<(int)arg.size();pointer++)
	if (arg[pointer]=="-compareDataSets"){
		vector<DepTreeCorpus *> v;
		for ( pointer++; pointer<(int)arg.size();pointer++)
		{
			DepTreeCorpus * c = new DepTreeCorpus(arg[pointer].c_str());			
			v.push_back(c);
		}
		this->makeTableData(v);

		return;
	}




		for (int pointer = 0; pointer<(int)arg.size();pointer++)
		if (arg[pointer]=="-jointEntropy"){
			cout << "loading training data"<<endl;	
			DepTreeCorpus training(params()->traing.c_str()); 
			cout << "loading testing data"<<endl;
			DepTreeCorpus2 test(params()->testing.c_str());
	  		cout << "displaying"<<endl;
			jointEntropy(&training,&test);
		}
			

		for (int pointer = 0; pointer<(int)arg.size();pointer++)
		if (arg[pointer]=="-visualTest"){


			cout << "loading training data"<<endl;	
			DepTreeCorpus training(params()->traing.c_str()); 
			cout << "loading testing data"<<endl;
			DepTreeCorpus2 test(params()->testing.c_str());
	  		cout << "displaying"<<endl;
		
			string auxcad = params()->stringTagTrees+".tre";//fin";//".txt.done";
			//training.reLabelUsingTable(test,auxcad);
			//test.samples[1]->dot_show();
			for (unsigned int i = 0; i<test.samples.size();i++){
	/*			if (test.samples[i]->line_number==42)
					test.samples[i]->dot_show();
				if (test.samples[i]->line_number==101)
					test.samples[i]->dot_show();
				if (test.samples[i]->line_number==136)
					test.samples[i]->dot_show();*/
				if (test.samples[i]->line_number==162)
					test.samples[i]->dot_show();
				if (test.samples[i]->line_number==191)
					test.samples[i]->dot_show();
				if (test.samples[i]->line_number==235)
					test.samples[i]->dot_show();
				if (test.samples[i]->line_number==334)
					test.samples[i]->dot_show();
				if (test.samples[i]->line_number==335)
					test.samples[i]->dot_show();
			}

			training.showMap(test,56,999);
			
			 
			return;
		}







	
	for (int pointer = 0; pointer<(int)arg.size();pointer++)
		if (arg[pointer]=="-done"){
			ifstream ficheroDone;
			string cad2=  params()->stringTagTrees + ".tre.done";
			ficheroDone.open(cad2.c_str());
			if (!ficheroDone)
			{
				cout <<"File: " << cad2 << " not found" << endl;
				exit(0);

			}
			ficheroDone.close();
			return ;
		}


	for (int pointer = 0; pointer<(int)arg.size();pointer++)
		if (arg[pointer]=="-help")
			help();

	for (int pointer = 0; pointer<(int)arg.size();pointer++)
		if (arg[pointer]=="-stats"){
			cout << endl<<"make statistics"<< endl;
			cout << "loading training data"<<endl;	
			DepTreeCorpus training(params()->traing.c_str());
			training.describeData();
			break;
		}



		

	for (int pointer = 0; pointer<(int)arg.size();pointer++)
		if (arg[pointer]=="-saveTrainingSentencesKendallFormat"){
			DepTreeCorpus2 test(params()->testing.c_str());
		  	
			string cad=  params()->stringTAG + ".kendall";
			test.saveSentencesKendallFormat(cad);
		}
	for (int pointer = 0; pointer<(int)arg.size();pointer++)
		if (arg[pointer]=="-getSingleRootedSentences"){
			getSingleRootedSentences(params()->traing.c_str());
		  	
			
		}
		

	for (int pointer = 0; pointer<(int)arg.size();pointer++)
		if (arg[pointer]=="-getkendallValue"){
			cout << getKendallTau(arg[pointer+1],arg[pointer+2]) << endl << endl;
			exit(0);
		}




	for (int pointer = 0; pointer<(int)arg.size();pointer++)
		if (arg[pointer]=="-generateTables"){

			string complet = params()->stringTagTrees + ".tre.done";
			ifstream ifile(complet.c_str());
				if (ifile) {
					cout << "  distance tables found, procedint to second phase. " << endl;
					
					ifile.close();
					break;// 
				}
				else cout << params()->stringTagTrees <<  ".tre.done was not found, aborting second phase"<< endl;

			
			cout << endl<<"GENERATE TABLES"<< endl;
			cout << "loading training data"<<endl;	
			DepTreeCorpus training(params()->traing.c_str());
			cout << "loading testing data"<<endl;
			DepTreeCorpus2 test(params()->testing.c_str());
		  	cout << "labelling"<<endl;
			training.preProcesFile(test);


			break;
		}
	

	for (int pointer = 0; pointer<(int)arg.size();pointer++)
		if (arg[pointer]=="-predict"){

			// nombre del fichero que indica que proceso ha terminado.

			string complet = 	"results/" +params()->stringTAG+".fin";//".txt.done";
			// comprobar si el fichero existe
				ifstream ifile(complet.c_str());
				if (ifile) {
					cout << " The experiment is already finish " << endl;
					break;// 
				}
				cout << complet << " file not found" << endl;


			cout << endl << "PREDICTING"<< endl;
			cout << "loading training data"<<endl;	
			DepTreeCorpus training(params()->traing.c_str()); 
			cout << "loading testing data"<<endl;
			DepTreeCorpus2 test(params()->testing.c_str());
	  		cout << "labelling"<<endl;
		
			string auxcad = params()->stringTagTrees+".tre";//fin";//".txt.done";
			training.reLabelUsingTable(test,auxcad);

			params()->guardar();


			/// create the termination file:
			cout << "file name: " << complet << endl;
				ofstream myfile;
				myfile.open (complet.c_str());
				myfile << " ";
				myfile.close();
			break;
		}
	
}


/**
* Display on the shell how to call the system.
*/
void ClassGlovalParameters::help(){
	cout << "parameters:" << endl <<
		"-training + training File" << endl <<
		"-tresting + testing File" <<  endl <<
		"[-allArgumentsEqual] chage all semantic labels to argument"<< endl<<
		"[-identificationTask] all non arguments nodes in a sentence have label `?NoPrediction' to the predicate node"<< endl <<
		"[-deleteDepRel] set all dependency relation labels to 'hidden'"<< endl <<
		"[-getSingleRootedSentences] to count how many roots have the sentences of the training data set"<< endl << 
		"[-saveTrainingSentencesKendallFormat] to save training data set in a kendall file to analize rankings to itself" << endl <<
		"[-getkendallValue file1 file2]"<< endl <<
		"[-stats to show statistics of the training data set]" << endl <<
		"[-nullPrediction Y|N default N]" << endl <<
		"[-atomicMeasure  binary|sbinary|ternary|sternary|hamming|shamming|predicateMatch|spredicateMatch|delta|deltaHamming|MartinSimilarity default binary|ALPHATERNARY|SALPHATERNARY|HAMMING3|SHAMMING3|HAMMING3PM|SHAMMING3PM|ALPHAHAMMING3|SHAPE]"<< endl <<
		"[-delta deltavalue default 1]" << endl <<
		"[-insertCost value cost default 1]" << endl << 
		"[-k value of knn default 1]" << endl <<
		"[-normalize Y|N default N]" << endl <<
		"[-multipleArguments Y|N default Y]"<< endl<<
		"[-treeKernel to make the system use tree kernel alignments, by default use tree distance]"<< endl <<
		"[-maxSentences number] limit the amount of sentence that can be loaded in a data set" << endl <<
		"[-leveinstan] will load the corpus as strings and not as trees" << endl <<
		"[-onlyVerbs | -onlyNoums | --verbsAndNoums ] for restriction the samples to verbs, noums or both, it only work in english dataset. " << endl<<
		"[-1N] al equivalences class have 1 element artificially "<< endl <<
		"[-predMatchCost + double] sets the extra cost of swapping two different predicates"<< endl<< 
		"-tag Tagforexperiment" << endl << endl<<
		"For use ALPHATERNARY OR SALPHATERNARY (ALPHAHAMMING or SALPHAHAMMING) put the value of alpha in parameter delta"<< endl <<
		"[-predict (for runing a SRL experiment)]" << endl <<
		"[-generateTables (for generating distance tables)]"<< endl<< endl <<
		"as first parameter -compareDataSets and then a list of the files to be compared"<< endl << endl;
	cout << "TREE SRL version: "<< this->codeVersion << endl;

}




 

/*
* 18-oct-2011
* Generate Sub-Tree-Relations
*
*/
/**
* For a given depencency tree, predicate number and argument number
* It creates a depenceny sub-tree containing the minimal sub-tree which contains:
* the predicate, the argument and all common uncestors until the first common one.
*/
DepSubTree::DepSubTree(DepTree* sentence,int predicateNumber, int argumentNumber){
	//Building a subtree;

	assert(predicateNumber < (int)sentence->POapreds.size());
	weight =0;
	int POpredicateNumber = sentence->POpred[predicateNumber];
	AbsTreeNode * predicate = sentence->PO_nodes[POpredicateNumber];
	this->line_number= predicate->info->fileSystemLine;
	lemmaPredicate = ((depTreeNodeInfo*)predicate->info)->LEMMA;
	


	//c/out <<  "number of nodes sentence: " << sentence->getNumberOfNodes() << endl;
	vector<bool> inSubTree; // what nodes will be inside.
	vector<int> newPO;
//	vector<vector<DepTreeNode *> > ancestors;
	vector<int> minNodes;

	//cout << "sentence->POpred[predicateNumber]" << sentence->POpred[predicateNumber] << endl;
	minNodes.push_back(sentence->POpred[predicateNumber]);
	//for(unsigned int x = 0; x<sentence->POapreds[predicateNumber].size();x++)								// modify lines 18-10-2011
	minNodes.push_back(sentence->POapreds[predicateNumber][argumentNumber].first);                     ///


	for(unsigned int x = 0; x<sentence->PO_nodes.size();x++){
		inSubTree.push_back(false);
		newPO.push_back(-1);
	}

	//all minNodes are collected, now we need to join them.

	int max = minNodes[0];
	//c/out << "minNodes.size() = " << minNodes.size()<< endl;
	for (int x = 0; x<(int)minNodes.size();x++){
		///c/out << "min: " << minNodes[x] << " " << x << endl;
		
		inSubTree[minNodes[x]]=true;
		
		
		if(minNodes[x]<max){

			AbsTreeNode * aux = sentence->PO_nodes[minNodes[x]];
			minNodes[x]= aux->parent->POindex;
		}

		if(minNodes[x]>max){
			max = minNodes[x];
			x=-1;
		}
		else if (minNodes[x]!=max)
			x--;
	}

	for(int x = 0,y=0; x<(int)sentence->PO_nodes.size();x++){
		if(inSubTree[x]){
			newPO[x]=y;
			y++;
		}
	}
//	cout << "newPO= ";
//	for (unsigned int x = 0; x<newPO.size();x++) if(newPO[x]!=-1) cout << x <<":"<<  newPO[x] << " ";
	//max = root sub tree;
	//c/out << "sentence line" << this->line_number << endl;
//	cout << "root =" << max << endl;
	AbsTreeNode * root =	copySubTree(sentence,inSubTree,max);
	buildPO_nodes(root);
//	cout << "PO_nodes.size()=" << PO_nodes.size()<<endl;

	int POpredicate = sentence->POpred[predicateNumber];
	predicateNode = newPO[ POpredicate ];
//	for(int x = 0; x< (int) sentence->POapreds[predicateNumber].size();x++)
	{
		pair < int, SemanticRelation * > relation;
		relation = sentence->POapreds[predicateNumber][argumentNumber];
		relation.first = newPO[relation.first];
		//vector< pair < int, SemanticRelation * > > POApred;
		POApred.push_back(relation);
	}

	assert(PO_nodes.size()>0);
	//int aux;
//	aux = sentence->POpred[predicateNumber];
	//int aux =sentence->POpred(predicateNumber);
	predicateNode=newPO[POpredicateNumber];
	for(int x = 0; x<(int)PO_nodes.size();x++){
		
		PO_nodes[x]->POindex=x;
	}
	setIdentityNodes();
	((DepTreeNode*)PO_nodes[predicateNode])->predicate= ((DepTreeNode*)sentence->PO_nodes[sentence->POpred[predicateNumber]])->predicate;
}



/**
*  @return true if the root of the tree is a predicte
*/
bool DepSubTree::rootIsPredicate(){
	return	((DepTreeNode*)PO_nodes.back())->isPred();
}

/**
*  @return true if the root of the tree is an argument
*/
bool DepSubTree::rootIsArgument(){
	return	((DepTreeNode*)PO_nodes.back())->isArg();
}

/**
*  @return true if one of the leaves is the predicate node.
*/
bool DepSubTree::leavePredicate(){
	return	(((DepTreeNode*) PO_nodes[predicateNode])->children.size()==0);
}
  

/**
*  This is an axuliar function to be used to short a vector of neigbours. 
*/
bool compareLabelVector(const pair < int, vector< int >  > & i, const pair < int, vector< int > > & j) 
{
		int a = 0;
	int b = 0;
	for (unsigned int p = 0; p<i.second.size();p++){
		a+= i.second[p];
		b+= j.second[p];
	}
	return (a>b);
}



/**
* It calculates the perplexity of the set of labels for multiple files a thte same time.
* pair<int,vector<int>  the first part represent a label, and the second part a counter per each file.
* @param counter specifies the total amount of labels in each file.
* @param labels, especifies for each label the amount of times that label appears on the file for each file.
* @return a vector of the prepelxitys on each file.
* 
*/
vector<double> getPerplexity(	vector<pair<int,vector<int> > > labels, vector<int> counter ){
	vector<double> res;
	for (unsigned int i = 0; i<labels[0].second.size();i++) res.push_back(0);

//	cout << "labels.size = "<< labels.size() << endl;
//	cout << "files size = "<< labels[0].second.size()<< endl;

	for (unsigned int l = 0; l<labels.size();l++)
		for (unsigned int file = 0; file<labels[0].second.size();file++){

			double prob = ((double)labels[l].second[file])/(double)counter[file];
			if (prob ==0) continue;
			//cout << "prob:"<< prob<< endl;
			double aux = prob*log(prob)/log(2.0);
			
		//	cout << "aux:"<< aux<< endl;
			res[file]+=aux;
		}
	for (unsigned int i = 0; i<res.size();i++){
		res[i] = pow(2,-res[i]);//04/07/2012
	//	cout << "res: "<< res[i]<< endl;
	}

	return res;
}



/**
* @param v is a vector of data sets (usually for the same language, training, evaluation, ood)
* make statistics for the vector of data sets.
*/
void ClassGlovalParameters::makeTableData(vector<DepTreeCorpus *> v){


	vector<int> neutral; 

	for (unsigned int i = 0; i<v.size();i++) neutral.push_back(0);

	vector<pair<int,vector<int> > > labels;
	
	vector<int> counter = neutral;


	for (unsigned int file = 0; file<v.size(); file++){
		DepTreeCorpus * cor = v[file];
	for (unsigned int sam = 0; sam<(int)cor->samples.size();sam++){
		

		DepSubTree* s = cor->samples[sam];
		for (int arg = 0; arg<(int)s->POApred.size();arg++){
			int argumento = s->POApred[arg].second->getRelation();
			//search and add
			int position;
			counter[file]+=1;

			for ( position = 0; position<(int)labels.size();position++)
				if (labels[position].first==argumento) 
					break;
			if (position==labels.size()){
				
				pair<int, vector<int> > a;
				a.first= argumento;
				a.second=neutral;
				labels.push_back(a);
			}
			
			labels[position].second[file]++;
		}

	}
	}
	std::sort(labels.begin(), labels.end(), compareLabelVector);



	cout << "\\begin{table}[h]" << endl;
	cout << "\\centering" << endl;
	cout << "\\begin{tabular}{| r | l | l | l | l |}" << endl;
//	cout << "\\hline" << endl;
//	cout << "semantic & \% of\\\\ " << endl;
//	cout << "labels   & total \\\\ " << endl;
//	cout << "\\hline" << endl;
	cout << "\\hline" << endl;
	cout.precision(4);
	vector<double> perplexity = getPerplexity(labels,counter);
	
	cout << "Amount labels: " << labels.size();
	for(unsigned int file = 0; file	<v.size();file++){
		cout <<  " &\t"	<< v[file]->fileName;
	}
	cout << " \\\\ \n \\hline \\hline"<< endl;

	
	cout << "perplexity ";
	for(unsigned int file = 0; file	<v.size();file++){
		cout <<  " &\t"	<< perplexity[file];
	}
	cout << " \\\\ \n \\hline"<< endl;
	cout << "total amount ";
	for(unsigned int file = 0; file	<v.size();file++){
		cout <<  " &\t"	<< counter[file];
	}
	cout << " \\\\ \n \\hline \\hline"<< endl;

	 
	for (int i = 0; i<(int)labels.size();i++){
		cout << params()->getString(labels[i].first);
	
		for(unsigned int file = 0; file	<v.size();file++){
		
			double x = 100.0 * labels[i].second[file]/counter[file];
			cout <<  " &\t"	<<  fixed <<scientific   << x << "\\\% "; // setprecision(7)
		}
		cout << " \\\\ \n\\hline"<< endl;
	}
		
	cout << "\\end{tabular}"<< endl;
	cout << "\\caption{}"<< endl;
	cout << "\\label{}"<< endl;
	cout << "\\end{table}"<< endl;


	///////////////////////////////////////////////////////////
	///
	//          THE SAME FOR DEPENDENCY RELATION
	//
	////////////////////////////////////////////////////////////



	labels.clear();
	
	 counter = neutral;


	for (unsigned int file = 0; file<v.size(); file++){
		DepTreeCorpus * cor = v[file];
	for (int sam = 0; sam<(int)cor->samples.size();sam++){
		

		DepSubTree* s = cor->samples[sam];
		for (int node = 0; node<(int)s->PO_nodes.size();node++){
			AbsTreeNode *n =  s->PO_nodes[node];
			depTreeNodeInfo* info = (depTreeNodeInfo*) n->info;
			counter[file]+=1;
			int label = info->DEPREL;
			
			int position;
			for ( position = 0; position<(int)labels.size();position++)
				if (labels[position].first==label) 
					break;
			if (position==labels.size()){
				
				pair<int, vector<int> > a;
				a.first= label;
				a.second=neutral;
				labels.push_back(a);
			}
			
			labels[position].second[file]++;
		}

	}
	}
	std::sort(labels.begin(), labels.end(), compareLabelVector);



	cout << "\\begin{table}[h]" << endl;
	cout << "\\centering" << endl;
	cout << "\\begin{tabular}{| r | l | l | l | l |}" << endl;
	cout << "\\hline" << endl;
	cout.precision(4);
	perplexity = getPerplexity(labels,counter);
	
	cout << "Amount DepRel labels: " << labels.size();
	for(unsigned int file = 0; file	<v.size();file++){
		cout <<  " &\t"	<< v[file]->fileName;
	}
	cout << " \\\\ \n \\hline \\hline"<< endl;

	
	cout << "perplexity ";
	for(unsigned int file = 0; file	<v.size();file++){
		cout <<  " &\t"	<< perplexity[file];
	}
	cout << " \\\\ \n \\hline"<< endl;
	cout << "total amount ";
	for(unsigned int file = 0; file	<v.size();file++){
		cout <<  " &\t"	<< counter[file];
	}
	cout << " \\\\ \n \\hline \\hline"<< endl;

	 
	for (int i = 0; i<(int)labels.size();i++){
		cout << params()->getString(labels[i].first);
	
		for(unsigned int file = 0; file	<v.size();file++){
		
			double x = 100.0 * labels[i].second[file]/counter[file];
			cout <<  " &\t"	<<  fixed << scientific   << x << "\\\% "; //setprecision(7)
		}
		cout << " \\\\ \n\\hline"<< endl;
	}
		
	cout << "\\end{tabular}"<< endl;
	cout << "\\caption{DepRelation labels on the nodes of the sub trees}"<< endl;
	cout << "\\label{}"<< endl;
	cout << "\\end{table}"<< endl;


	///////////////////////////////////////////////////////////
	///
	//          SOMETHING SIMILAR FOR LEMMA
	//
	////////////////////////////////////////////////////////////

	

	labels.clear();
	
	 counter = neutral;


	for (unsigned int file = 0; file<v.size(); file++){
		DepTreeCorpus * cor = v[file];
	for (unsigned int sam = 0; sam<(int)cor->samples.size();sam++){
		

		DepSubTree* s = cor->samples[sam];
		for (int node = 0; node<(int)s->PO_nodes.size();node++){
			AbsTreeNode *n =  s->PO_nodes[node];
			depTreeNodeInfo* info = (depTreeNodeInfo*) n->info;
			counter[file]+=1;
			int label = info->LEMMA;
			
			int position;
			for ( position = 0; position<(int)labels.size();position++)
				if (labels[position].first==label) 
					break;
			if (position==labels.size()){
				
				pair<int, vector<int> > a;
				a.first= label;
				a.second=neutral;
				labels.push_back(a);
			}
			
			labels[position].second[file]++;
		}

	}
	}
	std::sort(labels.begin(), labels.end(), compareLabelVector);
	cout << "\\begin{table}[h]" << endl;
	cout << "\\centering" << endl;
	cout << "\\begin{tabular}{| r | l | l | l | l |}" << endl;
	cout << "\\hline" << endl;
	cout.precision(4);
	perplexity = getPerplexity(labels,counter);
	cout << "Amount LEMMA labels: " << labels.size();
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " &\t"	<< v[file]->fileName;
	cout << " \\\\ \n \\hline \\hline"<< endl;
	cout << "perplexity ";
	for( unsigned int file = 0; file	<v.size();file++)
		cout <<  " &\t"	<< perplexity[file];

	cout << "\n % perplexity: ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " \t"	<< perplexity[file];
	cout << endl;

	cout << " \\\\ \n \\hline"<< endl;
	cout << "total amount ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " &\t"	<< counter[file];

	cout << "\n % amount : ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " \t"	<< counter[file];
	cout << endl;


	cout << " \\\\ \n \\hline \\hline"<< endl;	
	cout << "\\end{tabular}"<< endl;
	cout << "\\caption{LEMMA labels on the nodes of the sub trees}"<< endl;
	cout << "\\label{}"<< endl;
	cout << "\\end{table}"<< endl;




	///////////////////////////////////////////////////////////
	///
	//          same for FORM
	//
	////////////////////////////////////////////////////////////

	

	labels.clear();
	
	 counter = neutral;


	for (unsigned int file = 0; file<v.size(); file++){
		DepTreeCorpus * cor = v[file];
	for (int sam = 0; sam<(int)cor->samples.size();sam++){
		

		DepSubTree* s = cor->samples[sam];
		for (int node = 0; node<(int)s->PO_nodes.size();node++){
			AbsTreeNode *n =  s->PO_nodes[node];
			depTreeNodeInfo* info = (depTreeNodeInfo*) n->info;
			counter[file]+=1;
			int label = info->FORM;
			
			int position;
			for ( position = 0; position<(int)labels.size();position++)
				if (labels[position].first==label) 
					break;
			if (position==labels.size()){
				
				pair<int, vector<int> > a;
				a.first= label;
				a.second=neutral;
				labels.push_back(a);
			}
			
			labels[position].second[file]++;
		}

	}
	}
	std::sort(labels.begin(), labels.end(), compareLabelVector);
	cout << "\\begin{table}[h]" << endl;
	cout << "\\centering" << endl;
	cout << "\\begin{tabular}{| r | l | l | l | l |}" << endl;
	cout << "\\hline" << endl;
	cout.precision(4);
	perplexity = getPerplexity(labels,counter);
	cout << "Amount words labels: " << labels.size();
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " &\t"	<< v[file]->fileName;
	cout << " \\\\ \n \\hline \\hline"<< endl;
	cout << "perplexity ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " &\t"	<< perplexity[file];
	cout << " \\\\ \n \\hline"<< endl;
	
		cout << "\n % perplexity: ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " \t"	<< perplexity[file];
	cout << endl;
	
	cout << "total amount ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " &\t"	<< counter[file];

	
	cout << "\n % amount : ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " \t"	<< counter[file];
	cout << endl;

	cout << " \\\\ \n \\hline \\hline"<< endl;	
	cout << "\\end{tabular}"<< endl;
	cout << "\\caption{FORM labels on the nodes of the sub trees}"<< endl;
	cout << "\\label{}"<< endl;
	cout << "\\end{table}"<< endl;


	///////////////////////////////////////////////////////////
	///
	//          how about the Frames of the predicates? a->isPred()
	//
	////////////////////////////////////////////////////////////

	
	labels.clear();
	
	 counter = neutral;


	for (unsigned int file = 0; file<v.size(); file++){
		DepTreeCorpus * cor = v[file];
	for (int sam = 0; sam<(int)cor->samples.size();sam++){
		

		DepSubTree* s = cor->samples[sam];
		for (int node = 0; node<(int)s->PO_nodes.size();node++){
			AbsTreeNode *n =  s->PO_nodes[node];
			DepTreeNode *nn = (DepTreeNode *)n;
			if (!nn->isPred()) continue;

			depTreeNodeInfo* info = (depTreeNodeInfo*) n->info;
			counter[file]+=1;
			int label =  nn->predicate;
			
			int position;
			for ( position = 0; position<(int)labels.size();position++)
				if (labels[position].first==label) 
					break;
			if (position==labels.size()){
				
				pair<int, vector<int> > a;
				a.first= label;
				a.second=neutral;
				labels.push_back(a);
			}
			
			labels[position].second[file]++;
		}

	}
	}
	std::sort(labels.begin(), labels.end(), compareLabelVector);
	cout << "\\begin{table}[h]" << endl;
	cout << "\\centering" << endl;
	cout << "\\begin{tabular}{| r | l | l | l | l |}" << endl;
	cout << "\\hline" << endl;
	cout.precision(4);
	perplexity = getPerplexity(labels,counter);
	cout << "Amount frame labels: " << labels.size();
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " &\t"	<< v[file]->fileName;
	cout << " \\\\ \n \\hline \\hline"<< endl;
	cout << "frame perplexity ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " &\t"	<< perplexity[file];
	cout << " \\\\ \n \\hline"<< endl;
	
		cout << "\n % frame perplexity: ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " \t"	<< perplexity[file];
	cout << endl;
	
	cout << "total frame amount ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " &\t"	<< counter[file];

	
	cout << "\n % frame amount : ";
	for(unsigned int file = 0; file	<v.size();file++)
		cout <<  " \t"	<< counter[file];
	cout << endl;

	cout << " \\\\ \n \\hline \\hline"<< endl;	
	cout << "\\end{tabular}"<< endl;
	cout << "\\caption{frame labels on the nodes of the sub trees}"<< endl;
	cout << "\\label{}"<< endl;
	cout << "\\end{table}"<< endl;


}

/**
* abs function
*/
int intabs(int a){
	if (a<0)
		return -a;
	return a;
}

/**
* For two given distance files it calculates the kendallTau score of both rankings.
*/
double ClassGlovalParameters::getKendallTau(string file1, string file2){


  ifstream f1;
  ifstream f2;

  f1.open(file1.c_str());
  f2.open(file2.c_str());

  vector<int> v1 , vv1;
  vector<int> v2 , vv2;

  string cad1,cad2;
  
  while(getline(f1,cad1)){
	  getline(f2,cad2);
	 

     istringstream s1(cad1);
	 istringstream s2(cad2);
	 int i1, i2;
	 v1.clear();
	 v2 = vv2 = vv1 = v1;
	 while (s1>> i1){
		 s2 >> i2;
		 v1.push_back(i1);
		 v2.push_back(i2);
	 }
	 vv1 = v1; //set size
	 vv2 = v1; //set size
	 for (unsigned int i = 0; i<v1.size();i++){
		 vv1[v1[i]]=i;
		 vv2[v2[i]]=i;
	 }

	 int equalPairs = 0;
	 int NoEqualPairs = 0;

	 for (unsigned int i = 0; i<v1.size();i++){
		 NoEqualPairs += intabs(vv1[i]-vv2[i]);
		 equalPairs += v1.size()-intabs(vv1[i]-vv2[i])-1;
	 }
	 double n = v1.size();

	 double tau = ((double)(equalPairs-NoEqualPairs));
	 tau = (tau/((n/2)*(n-1)))/2;
	 return tau;

  }
  return -1;//this is an error. 
}





/**
* @return the lemma of the predicate as a string
*/
string DepSubTree::getLemaPred(){
	return params()->getString(lemmaPredicate);
}


/**
*  For two given dependency tree corpus it calcualtes the joint entropy and perplexity of the sequences of semantic labels of their predicates.
*/
void ClassGlovalParameters::jointEntropy(DepTreeCorpus* a,DepTreeCorpus* b){
	{
		vector<pair<double,string> > v;
		bool withPred = true;
	for (int i = 0; i<(int)a->samples.size();i++)
	{
		DepSubTree * d = a->samples[i];

		string cad = "";
		if (withPred){
			cad = d->getLemaPred();
		}
		//	cad+= ((depTreeNodeInfo*)d->PO_nodes(d->predicateNode)->info)->LEMMA;
		for (int j= 0; j<(int)d->POApred.size();j++){
			cad += ":"+d->POApred[j].second->getString();
		}
		int j =0;
		for (;j< (int) v.size();j++){
			if (v[j].second==cad){
				v[j].first++;
				break;
			}
		}
		if (j==v.size()){
			pair<double,string> a;
			a.first= 1;
			a.second=cad;
			v.push_back(a);
		}
	}
	///second data;
	double constant = a->samples.size()/(double)b->samples.size();
	constant = 1;
	for (int i = 0; i<(int)b->samples.size();i++)
	{
		DepSubTree * d = b->samples[i];

		string cad = "";
		if (withPred){
			cad = d->getLemaPred();
		}
		//	cad+= ((depTreeNodeInfo*)d->PO_nodes(d->predicateNode)->info)->LEMMA;
		for (int j= 0; j<(int)d->POApred.size();j++){
			cad += ":"+d->POApred[j].second->getString();
		}
		int j =0;
		for (;j<(int)v.size();j++){
			if (v[j].second==cad){
				v[j].first+= constant;
				break;
			}
		}
		if (j==v.size()){
			pair<double,string> a;
			a.first= constant;
			a.second=cad;
			v.push_back(a);
		}
	}
	/////////////
	double counter=0;
	for (int i = 0; i<(int)v.size();i++){
		counter += v[i].first;
	}
	
	for (int i = 0; i<(int)v.size();i++){
		v[i].first= v[i].first/counter;
	}
	counter = 0;
	
	for (int i = 0; i<(int)v.size();i++){
		counter+= -v[i].first*log(v[i].first)/log(2.0);
	}
	cout << "joint entropy with predicate lema = "<< counter << endl; 
	cout << "joint perplexity with predicate lema = "<< pow(2,counter) << endl; 
	}
	/////////2 part
	{
		vector<pair<double,string> > v;
		bool withPred = false;
	for (int i = 0; i<(int)a->samples.size();i++)
	{
		DepSubTree * d = a->samples[i];

		string cad = "";
		if (withPred){
			cad = d->getLemaPred();
		}
		//	cad+= ((depTreeNodeInfo*)d->PO_nodes(d->predicateNode)->info)->LEMMA;
		for (int j= 0; j<(int)d->POApred.size();j++){
			cad += ":"+d->POApred[j].second->getString();
		}
		int j =0;
		for (;j<(int)v.size();j++){
			if (v[j].second==cad){
				v[j].first++;
				break;
			}
		}
		if (j==v.size()){
			pair<double,string> a;
			a.first= 1;
			a.second=cad;
			v.push_back(a);
		}
	}
	///second data;
	double constant = a->samples.size()/(double)b->samples.size();
	constant = 1;
	for (int i = 0; i<(int)b->samples.size();i++)
	{
		DepSubTree * d = b->samples[i];

		string cad = "";
		if (withPred){
			cad = d->getLemaPred();
		}
		//	cad+= ((depTreeNodeInfo*)d->PO_nodes(d->predicateNode)->info)->LEMMA;
		for (int j= 0; j<(int)d->POApred.size();j++){
			cad += ":"+d->POApred[j].second->getString();
		}
		int j =0;
		for (;j<(int)v.size();j++){
			if (v[j].second==cad){
				v[j].first+= constant;
				break;
			}
		}
		if (j==v.size()){
			pair<double,string> a;
			a.first= constant;
			a.second=cad;
			v.push_back(a);
		}
	}
	/////////////
	double counter=0;
	for (int i = 0; i<(int)v.size();i++){
		counter += v[i].first;
	}
	
	for (int i = 0; i<(int)v.size();i++){
		v[i].first= v[i].first/counter;
	}
	counter = 0;
	
	for (int i = 0; i<(int)v.size();i++){
		counter+= -v[i].first*log(v[i].first)/log(2.0);
	}
	cout << "joint entropy without predicate lema = "<< counter << endl; 
	cout << "joint perplexity without predicate lema = "<< pow(2,counter) << endl; 
	}
	


}
