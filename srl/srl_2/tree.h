/**
 * @file
 * @author  Hector-Hugo Franco-Penya <francoph@scss.tcd.ie> 
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 *
 */


#pragma once

#ifndef TREE_H_

#define TREE_H_

#ifndef NDEBUG
	#define NDEBUG
#endif


//#//define NDEBUG
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <math.h>

using namespace std;

class DepTreeCorpus;
class DepTree;
class AbsTree;
class DepSubTree;
class ClassGlovalParameters;

ClassGlovalParameters * params();
vector<double>* getWeighs();

/**
* Specifies if the system is using the tree edit distance or the tree kernel.
*/
enum SimilarityMachineType{
	Zhang,
	TreeKernel
};

/**
* Specifies which of the attomic costs the system is ussing.
*/
enum AtomicMeasureType
{
	BINARY,
	TERNARY,
	HAMMING,
	PREDICATE_MATCH,
	ANCESTOR,
	COMPLEX,
	
	SBINARY,
	STERNARY,
	SHAMMING,
	SPREDICATE_MATCH,
	SANCESTOR,
	SCOMPLEX,

	ALPHAHAMMING3,
	SHAPE,

	ALPHATERNARY,
	SALPHATERNARY,	
	ALPHAHAMMING,
	SALPHAHAMMING,
	

	HAMMING3,
	SHAMMING3,
	HAMMING3PM,
	SHAMMING3PM,


	DELTA,
	DeltaHamming,
	MartinSimilarity
};


/**
* Contains the set of parameters the system is ussing.
*/
class ClassGlovalParameters{
private:
	time_t start,end;
	struct tm * startingTime;

	void makeTableData(vector<DepTreeCorpus *>);
public:
	string getString(int symbol);//30-sep-2011
	//static ClassGlovalParameters * me;
	//static ClassGlovalParameters *getMe();
	string traing;
	string testing;
public:
	int maxSentencesInDataSet;
	bool allowNullPrediciton;
	double delta;
	int numTAG;

	bool allArgumentsEqual;//05/07/2012
	bool identificationTask;//05/07/2012

	string stringTAG;
	string stringTagTrees;
	string codeVersion;

	stringstream inform;
	string getAntomicMeasureType();
	//string getSimilarityMachineType();
	ClassGlovalParameters();
	void guardar();
	AtomicMeasureType AtomicMeasure;
	SimilarityMachineType SimilarityMachine;
	double accuracy; //!<  accuracy of the system. 
	bool deleteDepRel;
	int cacheSize;
	bool noralize; //!< normalize tree edit distance?
	bool multipleArgumentsTrees; //!< using multiples arguments per sub-tree.
	double insertDeleteBaseCost; //!< deletion/insertion cost.
	int K_for_KNN; //!< k value for the k-NN system.
	void readParams(vector<string> );
	void execute(vector<string>);
	void help();

	int maxSamples; //!< max number of sub-trees in a corpora.
	int maxSentenses; //!<  max number of sentences loaded in a corpora
	bool useStrings; 

	bool splitDataSet;
	bool keepVebs; 
	bool keepNoums;
	bool N1; //!< re-define the distances in order to have 1 sample per equivalence class.
	bool allAtOnce; //!< predict all arguments at once. 

	double getKendallTau(string file1, string file2);
	void jointEntropy(DepTreeCorpus*,DepTreeCorpus*);

	double predMatchCost;

};
 

/**
 * this function read a set of lines from a input file and write them in a vector of strings.
 * the set of lines corresponds to one sentence of the dataset.
 * @author Martin Emms
 */
bool get_next_stanza(ifstream  &f,vector<string> &latest_stanza, int & counter);

////enum {FORM=1, LEMMA, PLEMMA, POS, PPOS, FEAT, PFEAT, HEAD, PHEAD, DEPREL, PDEPREL, FILLPRED, PRED};

class SemanticRelation { //it will be extended
protected:
  int relation; // the label is in the table.

public:
  int getRelation();
  void setRelation(int A) ;
  void setRelation(string A);
  string getString();
  void setPrediction( SemanticRelation * prediction);
};

//class Corpus_class {
//};

/**
* This class is used to code strings into integers
*/
class symbolTableClass {
private:
  //map<int, string> symbols;
  vector<string> symbols;
  map<string, int> numbers;
  //int counter; is vector size;
public:
	const int underscore;
	symbolTableClass():underscore(setString("_")){
	//underscore = ;

  }
  bool exist(const int index){
	/*map<int, string>::iterator itr;
    if ( (itr = symbols.find(index)) != symbols.end())
      return true;//(*itr).second;
    else
	    return false;
*/
	  return (index >= 0 && index <(int)symbols.size());
  }
  bool exist(const string & cad){
	map< string, int>::iterator itr;
	itr = numbers.find(cad);
    if ( itr != numbers.end())
      return true;
    else
	    return false;
  }

//  symbolTable() {
//	  counter = 0;
//	  }



  string getString(int val) {

/*    map<int, string>::iterator itr;
    if ( (itr = symbols.find(val)) != symbols.end())
      return (*itr).second;
    else {
	    return ""; //????
      // throw an exception here ??
    }
    */
	  if (!( val >= 0 && (val <(int)symbols.size()) ))
		  throw "that index do not exist";
	  return symbols[val];
  }


  int setString (string str) {


	map< string, int>::iterator itr;
    if ( (itr = numbers.find(str)) != numbers.end())
      return (*itr).second;
    else {
    	numbers[str]=symbols.size();
    	symbols.push_back(str);
//	    symbols[counter]= str;
	    //counter++;
	    return symbols.size()-1;
    }



  }


};



class AbsNodeInfo {

public:
	  int fileSystemLine; //!< in which line of the data set file was read, for debuggin purposes. 
virtual string	getOriginalLine() =0;
  virtual double deleteCost() = 0;
  virtual double createCost() = 0;
  virtual double matchCost( AbsNodeInfo*) = 0;
};



/**
* Contains the information of a depencency tree node.
*/
class depTreeNodeInfo : public AbsNodeInfo {
friend class DepTreeNode;
private:
public:
//public:
// GLOVAL SYMBOL TABLE.
 // symbolTable* globalTable; //

  ////DepTree* cntr;
////  vector<depTreeNodeInfo*> dtrs; // word order this node's dependents    -> a ordered list of children.
////  depTreeNodeInfo *parent;

int FORM , LEMMA, PLEMMA, POS, PPOS, FEAT, PFEAT,  DEPREL, PDEPREL, FILLPRED, PRED; // they are pointers to the symbol table.
int HEAD, PHEAD;
//int INDEX;


public:
	//int getForm();
	string getFormString();
	string getDepRelation();
	string getPOS();

//  depTreeNodeInfo() {}
  depTreeNodeInfo(string line, int counter);
  string getOriginalLine();
//  depTreeNodeInfo(DepTree* dt, unsigned int i);


////  void setDtr(depTreeNodeInfo *child) { parent->dtrs.push_back(child); }
  // Function to be defined later
  double deleteCost();
  double createCost();
  double matchCost( AbsNodeInfo* p) ;
};

class AbsTreeNode {
  //protected:
public:
    AbsTreeNode * parent ; //!< points to the parent node
    vector<AbsTreeNode * > children; //!< list of children 
    AbsNodeInfo * info; //!< furter information of this node
    int Index; //!< word order index
    int POindex; //!< traversial post order index

  public:
  int  number_of_leafs();

  int getMostLeftPO();
  void buildPOVector(vector<AbsTreeNode*> & v);
  void setParent(AbsTreeNode * p);
  virtual  string getOriginalLine() = 0;
  void setIndex (int x){Index =x;};
  int getIndex(){return Index;};
  bool i_am_a_leaf(){return children.size()==0;}; //10/5/2010
  bool i_am_the_root(){return parent==NULL;};

  virtual double deleteCost()=0;//{return 1;};
  virtual double createCost()=0;//{return 1;};
  virtual double map2cost(AbsTreeNode * B)=0;//{return 0.1;}; // to make virtual
};


/**
*   Contains the information of a dependency tree node.
*/
class DepTreeNode : public AbsTreeNode {
  public:
    int predicate;

  	bool ImAnArgument; //!< true is the node is an argument
	bool ImAPredicate; //!< true is the node is a predicate

    double deleteCost();
    double createCost();
	double deleteCost2(AtomicMeasureType t);
	double createCost2(AtomicMeasureType t);

    double map2cost(AbsTreeNode * B);
	double map2costAux(AbsTreeNode *B,AtomicMeasureType atomic);
	DepTreeNode(string,int);
	DepTreeNode(DepTreeNode * original);
	string getOriginalLine();
	string getForm();
	bool isPred();
	bool isArg();

	bool i_am_a_not_leaf_argument();

	//3-april-2011
	bool pegamento; //!< if both nodes have this variable set to true, they are forece to match each other. 
//	string getLemma();
  //protected:
  //  bool AmIaPredicate;
};
/*
class DepSubTreeNode : public AbsTreeNode {
  protected:
    bool AmIaArgument;
};
////endif
*/

/**
* represents the minial information to be used by tree edit distance algorithm.
*/
class AbsTree {
protected:
public:
	void buildPO_nodes(AbsTreeNode * root);
	vector<AbsTreeNode *> PO_nodes; //!< List of nodes in traversial post order
	vector<int> keyRoots; //!<  list of position of keyRoots nodes in traversial post order
	vector<int> mostLeft; //!<  For each node specifies which is the most left node in traversial post order

    int line_number; // indicates from witch line from the source file it was builded.
   // int rootWordOrder; // indicates were is the root node in the wordOrderNodes vector.

//    friend void DepTree::save(ofstream &f);
public:
	double weight; //!< not in used
	virtual ~AbsTree(){};//!< it allows dynamic conversion
	void show_keyroots(); 
	void ready2TreeDistance();
	vector<int> * getKeyRoots();
	vector<int> * getMostLeft();
	AbsTreeNode * getNode(int PO);
	unsigned int size();
//  vector<int>* getKeyRoots() {} // post order IT IS REQUIRED FOR RUN TREE DISTANCE ALGORITHM.
//  vector<int>* getMostLeft() {} // post order IT IS REQUIRED FOR RUN TREE DISTANCE ALGORITHM.
	vector<string >  getStranza();
  int getNumberOfNodes(){return PO_nodes.size();};
         double no_leaf_branch_factor();
protected:

//	AbsTreeNode * copySubTree(AbsTree * sentence,vector<bool> & inSubTree, int max);
};


class AbsDepTree: public AbsTree{
public:
		void dot_show(string);
		void dot_show();
		void show_info_nodes();

        double no_leaf_branch_factor();

//protected:
		virtual void dot_show(ofstream& f)=0;
		bool is_pred(int POindex);
		bool is_arg(int POindex);
		virtual void setIdentityNodes()=0;
	DepTreeNode * copySubTree(AbsDepTree * sentence,vector<bool> & inSubTree, int max);


};


class DepTree : public AbsDepTree { // is a natural language sentence
private:


	void buildPOtree();

	bool get_sem_dep(int dep_index, int WOparent_index,string & sem_rel);
//	bool is_pred(int POindex);
public:
	
	void dot_show();
	void setIdentityNodes();
	vector<AbsTreeNode *> all_nodes;  //!< List os nodes in word order.
  //DepTree() {}
  //DepTree(vector<string>& input, int counter);
	void dot_show(ofstream& f);
  DepSubTree* getSubTree(int);
  void save(ofstream &f);
  DepTree (vector<string> & stanza, int line_number);
  void export_tree(ofstream &f); //Function to export a tree to a given file
  //vector<depTreeNodeInfo *> wordOrderNodes; // It is only usefull for read and write from a file.
  //vector<depTreeNodeInfo *> postOrderNodes; // Tree distance only use this vector.
                                            // Both vectors contain the same nodes, but in different order. the root
                                            // node is always the last one in the traversal post order list.
private:

  vector<int> pred; //!< contains indices of the nodes which are preds
  vector<vector<pair<int, SemanticRelation *> > > apreds; //!< list of arguments per each predicate

public:
 void showDangerSubTrees();

  vector<int> POpred; //!< contains indices of the nodes which are preds
  vector<vector<pair<int, SemanticRelation *> > > POapreds; //!< For each predicate, there is a list of nodes that are arguments,  int indicates the postOrderNode position of the argument, Semantic Relation indicates the type of relation.

  void buildDepSubTrees();
  vector<DepSubTree *> DepSubTrees; //!< list of sub-trees extracted from this sentence

  friend class DepSubTree;
 // friend class DepRelTree;
};


/**
* This class is used to experiment argument identification. 
*
*/
class DepTree2 : public DepTree { // is a natural language sentence to be relabel
public:
	DepTree2(vector<string> & stanza, int line_number);
	vector<vector<pair<int, SemanticRelation *> > > NEW_POapreds; //!< new list of arguments per each predicate predicted by the system. 
	vector<vector<double > > strength;  //!< if it is a prediction how much confidence has the system to made such prediction

	void evaluate (int & TPPosition, int & FPPosition, int & FNPosition, int & TPLabel, int & FPLabel, int & FNLabel); 

};



/*
class DepRelTree : public AbsDepTree {
public:
	void dot_show(ofstream& f);
	DepRelTree(DepTree*,int, int);
	bool is_pred(int POindex);
	void saveWEKA(ofstream &f);
	void setIdentityNodes();
protected:
public:
	//DepTreeNode * copySubTree(DepTree * sentence,vector<bool> & inSubTree, int max);                                          // Both vectors contain the same nodes, but in different order.
  int predicateNode;                        // where is the predicate in the vector (only can be one).
  int argumentNode; // there is only one.
  SemanticRelation * rel; // there is only one;

};
*/

/**
* Contains a single predicate node.
*/
class DepSubTree : public AbsDepTree { //it stores sub-trees.
public:
	void dot_show();
	void dot_show(ofstream& f);
	  //vector<int> POpred; // contains indices of the nodes which are preds
	  DepSubTree(DepTree*,int);
	  DepSubTree(DepTree* sentence,int predicateNumber, int argumentNumber);//for sub-trees;
	
	bool is_pred(int POindex);
private:
	  int lemmaPredicate; //!< indicates which node is the predicate
public:
	string getLemaPred();

	double costDelete();
	double costInsert();

	void setIdentityNodes();
	int intermediateArguments();

  ///vector<depTreeNodeInfo *> postOrderNodes; // Tree distance only use this vector.
	                                          // Both vectors contain the same nodes, but in different order.
  int predicateNode;                        //!<  where is the predicate in the vector (only can be one).


  vector< pair < int, SemanticRelation * > > POApred; // In this case there is only one predicate, so is not a matrix,
                                                 // just a vector the semanticRelation is a pointer, because if a subtree
                                                 // is modificated the depTree were it comes from have to be modificate also.



  //4-Nov-2011
  bool rootIsPredicate();
  bool rootIsArgument();
  bool leavePredicate();
  


};

/**
* Contains the whole set of trees of a corpus. 
*/
class DepTreeCorpus;
#include "DepTreeCorpus.h"

void findtests(DepTreeCorpus & c);
#endif
