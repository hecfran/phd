/**
 * @file
 * @author  Hector-Hugo Franco-Penya <francoph@scss.tcd.ie> 
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 *
 */

#ifndef TREE_DISTANCE_H
#define TREE_DISTANCE_H

#pragma once
#include "tree.h"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <stdlib.h>
#include <assert.h>
//#include <algorithm>

//class TreeDistance;
//class TreeKernel;








class Alignment;
class Alignment{
	friend class TreeDistance;
	friend class TreeKernel2;
	friend class TreeKernel3;
protected:
//public:
	int * Image; //!<  F[domain]= codomain; 
	int * CoImage; //!< F[codomain] = domain;
	void set( int X, int Y);
	bool secureSet (int, int);

	public:

	Alignment(AbsTree * A, AbsTree * B);	
	AbsTree * DomainTree; //!< domain tree 
	AbsTree * CoDomainTree; //!< co-domain tree

	AbsTree * getDomainTree(); 
	AbsTree * getCoDomainTree(); //Y
	bool match(int X, int Y);
	void shell_show();
	int function(int PO);
	int inverseF(int PO);
	~Alignment();
	void dot_show();
	void dot_show(ofstream& f);
	void dot_show_new(string style, AtomicMeasureType cost_setting);//martin 26/06/2012
	void dot_show_new(string style, AtomicMeasureType cost_setting, ofstream& f); //martin 26/06/2012

	bool isEqual(Alignment*);

	double recalculateDistance();
};

class SimpleTableNode{
public:
	SimpleTableNode();
	void copy(SimpleTableNode &);//escribir
	double value;
	SimpleTableNode * forestFrom;
	SimpleTableNode * treeFrom;
	int matchA;
	int matchB;
};


class TreeMeasure {
//protected:
//	AbsTree * DomainTree; //X
//	AbsTree * CoDomainTree; //Y
public:
	virtual void setVectorPossition(int i)=0;
	virtual int getVectorPossition()=0;
	virtual AbsTree * getDomainTree()=0; //X
	virtual AbsTree * getCoDomainTree()=0; //Y
	
	 virtual double getDistance()=0;
	 virtual double getAlignament(Alignment * *)=0;
	 virtual void setDistance(double d)=0;
	
   	virtual ~TreeMeasure() {}
};




class TreeKernel2: public TreeMeasure
{
	protected:
		AbsTree * DomainTree; //X
		AbsTree * CoDomainTree; //Y
		double distance;
		Alignment * firstAlignment;

		void buildTables();
		double getCvalue(int i, int j, vector< vector< double > > & C_values);
		bool increaseCounter(vector<int> & counter, int max);
		double multiplySubChild(AbsTreeNode * nodeA, AbsTreeNode *nodeB, vector< vector< double > > & C_values,	vector<vector< int > > & positionsA, vector<vector < int > > & positionsB);
		Alignment * makeAlignment(vector < vector< double > > & C_values);

		int positionVector;
	public:
		AbsTree * getDomainTree(); //X
		AbsTree * getCoDomainTree(); //Y
		TreeKernel2(AbsTree *domine, AbsTree *codomine);
		~TreeKernel2(void);
		double getDistance();//negative kernel value.
		double getAlignament(Alignment * *);
		void setDistance(double d);

		
		void setVectorPossition(int i);
		int getVectorPossition();

};



class TreeKernel3: public TreeMeasure
{
	protected:
		AbsTree * DomainTree; 
		AbsTree * CoDomainTree; 
		double distance;
		Alignment * firstAlignment;

		void buildTables();
		double getCvalue(int i, int j, vector< vector< double > > & C_values);
		//bool increaseCounter(vector<int> & counter, int max);
		//double multiplySubChild(AbsTreeNode * nodeA, AbsTreeNode *nodeB, vector< vector< double > > & C_values,	vector<vector< int > > & positionsA, vector<vector < int > > & positionsB);
		Alignment * makeAlignment(vector < vector< double > > & C_values);

		int positionVector;
	public:
		AbsTree * getDomainTree(); //X
		AbsTree * getCoDomainTree(); //Y
		TreeKernel3(AbsTree *domine, AbsTree *codomine);
		~TreeKernel3(void);
		double getDistance();//negative kernel value.
		double getAlignament(Alignment * *);
		void setDistance(double d);
		void setVectorPossition(int i);
		int getVectorPossition();
};


class TreeDistance: public TreeMeasure
{
protected:
	AbsTree * DomainTree; //X
	AbsTree * CoDomainTree; //Y
	
	Alignment * firstAlignment;
	void buildSimpleTables();
	void build1AlginamentTables();
	void buildSimpleTablesTreeDist(int keyA, int KeyB, int counter,   vector< int > * mostLeftA, vector<int> * mostLeftB ,vector<vector<vector<double> > > *, vector< vector< double > > * treeTable);
	void build1AlignamentTablesTreeDist(int keyA, int KeyB, int counter,   vector< int > * mostLeftA, vector<int> * mostLeftB ,vector<vector<vector<SimpleTableNode> > > *,vector< vector < SimpleTableNode > > * treeTable);
//////	void build1aligment(SimpleTableNode*);
	void build1aligment(SimpleTableNode* node);
	

	int positionVector;
public:
	double distance;
	AbsTree * getDomainTree(); //X
	AbsTree * getCoDomainTree(); //Y
	TreeDistance(AbsTree *domine, AbsTree *codomine);
	~TreeDistance(void);
	double getDistance();
	double getAlignament(Alignment * *);
	//Alignment * getAlignment(double & distance);
	void setDistance(double d);
	void setVectorPossition(int i);
	int getVectorPossition();
};






#endif //TREE_DISTANCE_H