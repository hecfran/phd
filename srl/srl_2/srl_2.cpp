/**
 * @file
 * @author  Hector-Hugo Franco-Penya <francoph@scss.tcd.ie> 
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 *
 */


// srl_2.cpp : Defines the entry point for the console application.
#include "stdafx.h"
#include <typeinfo>

#include "tree.h"
#include "DepTreeCorpus.h"
#include "TreeDistance.h"


#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <stdlib.h>
#include <assert.h>
#include <iostream>
#include <iostream>
#include <time.h>
//#include "tree.h"
using namespace std;


void Searcher(const char*);
void AlignmentTest(const char * file);
void TranslateFormat(const char * inputFile, char * outputFile);
int inspector( int argc, const char* argv[] );



/**
* This function is a sanity test to verify if the Issert example is reproducible
* Isert, C. (1999). The Editing Distance Between Trees.
* 
*/
void IssertTest(){

	DepTreeCorpus corpus("trees");
	Alignment * p;
	TreeDistance *d = new TreeDistance(corpus.DTcorpora[0],corpus.DTcorpora[1]);
	cout << "distance " << d->getDistance() << endl;
	cout << "distance : " << d->getAlignament(&p) << endl;
	//cout << typeid(TreeDistance);
	cout << "type  " << (int) (typeid(*d)==typeid(TreeDistance))<< endl;
	cout << "type  " << (int) (typeid(*p)==typeid(TreeDistance))<< endl;

	p->dot_show();

}


/**
* Converts a number into a string
*
* @param num given number
* @return string corresponding to that number.
*/
string num2straux(int num){
	string a ="";
	if (num == 0) return "0";
	
	while (num>0){
		a = (char)('0'+num%10)+a;
		num = num/10;
	}
	return a;
}







/**
* This function is the starting poing of the system.
*/
int main( int argc, const char* argv[] ) {

//	return inspector (argc, argv);
//	params()->AtomicMeasure=HAMMING;
    vector<string> argumentos;
	
	for (int i= 0 ; i<argc; i++){
		string cad(argv[i]);
		argumentos.push_back(cad);
	}
	params()->execute(argumentos);
	exit(0);
};


/**
* For a fix set of arguments, the system displays the alignment of two given trees.
* The parameters have to be fixed by changing the source code. 
*/
int inspector( int argc, const char* argv[] ) {
	params()->AtomicMeasure=HAMMING;
	vector<string> arg;
	
	arg.push_back("-testing");
	arg.push_back("D://data//UPCoNLL2009-ST-evaluation-German.txt");
	arg.push_back("-training");
	arg.push_back("D://data//UPCoNLL2009-ST-German-train.txt");
	arg.push_back("-atomicMeasure");
	arg.push_back("hamming");
	arg.push_back("-normalize");
	arg.push_back("N");
	arg.push_back("-tag");
	arg.push_back("GER");
	arg.push_back("-insertCost");
	arg.push_back("1");
	arg.push_back("-multipleArguments");
	arg.push_back("Y");

	params()->readParams(arg);


        // process some additional arguments
        // -trnTree Num1  -tstTree Num2
        int trnNum, tstNum;
        for (int pointer = 0; pointer< arg.size();pointer++){
          if (arg[pointer]=="-trnTree"){
	    pointer++;
	    trnNum = str2num(arg[pointer]);
	  }
          if (arg[pointer]=="-tstTree"){
	    pointer++;
	    tstNum = str2num(arg[pointer]);
	  }

	}


	cout << "loading training data"<<endl;
	DepTreeCorpus training("D://data//UPCoNLL2009-ST-Japanese-train.txt");
	cout << "loading testing data"<<endl;
	DepTreeCorpus2 test("D://data//UPCoNLL2009-ST-evaluation-Japanese.txt");

        string wish;

     //   while((cin >> wish) && (wish != "quit")) {
      //    if(wish == "align") {
	  //  cout << "training and testing tree indices\n";
      //      cin >>  trnNum >> tstNum;
			trnNum = 23976;//45748;
			tstNum = 1;

	    DepSubTree *s = training.samples[trnNum];
//	    s->dot_show();
	    cout << "training line num:" << s->line_number << endl;

	    DepSubTree *t =  test.samples[tstNum];
//	    t->dot_show();
	    cout << "testing  line num:" << t->line_number << endl;

	    /* extracted from  DepTreeCorpus::showMap */

	    TreeDistance *td = new TreeDistance(s,t);
	    TreeDistance *td2 = new TreeDistance(t,s);


	    cout << "showing info nodes of source " << endl;

	    s->show_info_nodes();


	    cout << "showing info nodes of target " << endl;

	    t->show_info_nodes();

	    cout << "showing the alignment"<< endl;
	    Alignment *a;
	    double distance = td->getAlignament(&a);
	    Alignment *a2;
	    double distance2 = td2->getAlignament(&a2);
	    cout << "distance1 " << distance << endl;
	    //a->dot_show_new("", TERNARY);
	    a->shell_show();
		cout << "distance2 " << distance2 << endl;
	    //a->dot_show_new("", TERNARY);
	    a2->shell_show();


		a->dot_show();
	    cout << "distance " << distance << endl;
//	  }
//	}

//	system("PAUSE");
	exit(0);
};



/**
* Compares if teh alignments using Distance Ternary and Similarity Ternary are equivalent of trees of one corpus to themselves.
*/
void AlignmentTest(const char * file){
	DepTreeCorpus x(file);
	DepTreeCorpus y(file);

	x.TestEqualAlignments(y);

}


/**
* This function displays sentences as user request them.
*/
void Searcher(const char* file){

	cout << endl <<"loading data"<<endl;
	DepTreeCorpus x(file);
	x.describeData();

	string cad;
	int num;
	while(true){
		cout << endl << endl << "Menu: type:" << endl;
		cout << "'.' to close the program" << endl;
		cout << "a number to display the sentence and sub-trees on that line of the imput file" << endl;
		cout << "a word to display a sub-tree which predicate is that word"<< endl;
		cin >> cad;
		if (cad[0]=='.')
			break;
		if (cad[0]>='0' && cad[0]<='9')//it is a number.
		{
			istringstream myStream(cad);
			myStream >> num;
		
			for (int count=1;count<(int)x.DTcorpora.size();count++){
				if (x.DTcorpora[count]->line_number>num){
					count--;
					x.DTcorpora[count]->dot_show();
					for (int i=0;i<(int)x.DTcorpora[count]->DepSubTrees.size();i++){
						cout <<"line"<< x.DTcorpora[count]->DepSubTrees[i]->line_number << endl;
						x.DTcorpora[count]->DepSubTrees[i]->dot_show();						
					}
					break;
				}
			}
		}
		else // it is a form
		{
			for (int count=1;count<(int)x.DTcorpora.size();count++){
				bool found = false;
				for (int i=0;i<(int)x.DTcorpora[count]->DepSubTrees.size();i++){
					DepSubTree * p;
					p = x.DTcorpora[count]->DepSubTrees[i];
					int predicateNodePosition = p->predicateNode;
					if (cad==((DepTreeNode*)p->PO_nodes[predicateNodePosition])->getForm())
					{
						//then it is found.
						found = true;
						cout <<"line"<< p->line_number << endl;
						p->dot_show();
					
					}

				}
				if (found) x.DTcorpora[count]->dot_show();
			}//end for (int count=1;count<x.DTcorpora.size();count++
			

		}
	}
}


/**
* Counts the amount of predicates of one sentence
* @param stranza is the sentence represented in a vector of strings (one string per word) in CoNLL2009 format
* @return it returns the amount of predicates.
*/
int countPred(vector<string> stanza){
	int counter=0;
	for (int i = 0; i<(int)stanza.size();i++){
		string cad;
		istringstream ins; // Declare an input string stream.
		ins.str(stanza[i]);
		for (int j=0;j<13;j++)	ins >> cad;
		if(cad=="Y")counter++;
	}
	return counter;
}

/**
* Adds and extra root node to each sentence.
*/
void increse1stanza(vector<string> & stanza){
	string cad;
	string newcad;
	for(int i = 0; i<(int)stanza.size(); i++){
		istringstream ins; // Declare an input string stream.
		ins.str(stanza[i]);
		ins >> cad;
		newcad = num2straux(i+2);

		for (int j=0;j<7;j++){
			ins >> cad;
			newcad = newcad+"\t"+cad;
		}
		
		ins>>cad;
		newcad = newcad+"\t"+  num2straux((int)str2num(cad)+1);
		
		ins>>cad;
		newcad = newcad+"\t"+ num2straux((int)str2num(cad)+1);

		while ( ins >> cad){
			newcad = newcad+"\t"+cad;
		}
		stanza[i]=newcad;
	}
}
